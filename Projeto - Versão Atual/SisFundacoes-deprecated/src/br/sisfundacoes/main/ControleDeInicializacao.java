package br.sisfundacoes.main;



import br.sisfundacoes.capacidadeDeCarga.servicos.ServicoDeLog;
import br.sisfundacoes.visao.FrameInicial;
import org.apache.log4j.BasicConfigurator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gabriel B Moro
 */
public class ControleDeInicializacao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ServicoDeLog.getMyInstance().registrarLogDeInformacao(ControleDeInicializacao.class,
                "Inicializando a aplicação...");
        
        new FrameInicial();
    }
    
}
