package br.sisfundacoes.capacidadeDeCarga;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.sisfundacoes.main.Dispachante;
import br.sisfundacoes.capacidadeDeCarga.visao.JFramePrincipalCarga;

/**
 *
 * @author Gabriel B Moro
 */
public class DispachanteDeModuloCapacidadeDeCarga implements Dispachante{
    
    @Override
    public void chamarInterfaceGraficaDoModulo(){
        new JFramePrincipalCarga();
    }
}
