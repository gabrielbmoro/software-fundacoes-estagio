/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

import br.sisfundacoes.capacidadeDeCarga.servicos.ServicoDeLog;

/**
 *
 * @author Gabriel B Moro
 */
public class EstrategiaDeMetodoDecourtQuaresma implements IEstrategiaCalculoDeCarga {

    private double nsptMedio;
    private double areaDaBase;
    private double nsptPor3;
    private double perimetro;
    private int profundidade;

    @Override
    public int calcularResistenciaDePonta(Estaca estaca, Solo solo) {
        double result = solo.getAlfa_usadoPorDecourtQuaresma() * (double) solo.getC_usadoPorDecourtQuaresma() * nsptMedio * areaDaBase;

        ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                "======>>Qp Decourt, ---- " + result + "(resultado) = "
                + solo.getAlfa_usadoPorDecourtQuaresma() + "(alfa) * "
                + solo.getK() + "(k) * "
                + nsptMedio + "(nsptMedio) * "
                + areaDaBase + "(ab) ");
        return (int) result;
    }

    @Override
    public int calcularResistenciaLateral(Estaca estaca, Solo solo) {
        nsptPor3 += 1;

        double result = solo.getBeta_usadoPorDecourtQuaresma() * nsptPor3 * perimetro * 1 * profundidade;

        ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                "======>>Ql Decourt, ---- " + result + " (resultado) = "
                + solo.getBeta_usadoPorDecourtQuaresma() + "(beta) * "
                + nsptPor3 + "(nsptPor3) * "
                + perimetro + "(perimetro) * "
                + profundidade + "(profundidade) ");

        return (int) result;
    }

    @Override
    public int calcularResistenciaTotal(int resistenciaDePonta, int resistenciaLateral) {
        return (resistenciaDePonta + resistenciaLateral);
    }

    public double getNsptMedio() {
        return nsptMedio;
    }

    public void setNsptMedio(double nsptMedio) {
        this.nsptMedio = nsptMedio;
    }

    public double getAreaDaBase() {
        return areaDaBase;
    }

    public void setAreaDaBase(double areaDaBase) {
        this.areaDaBase = areaDaBase;
    }

    public double getNsptPor3() {
        return nsptPor3;
    }

    public void setNsptPor3(double nsptPor3) {
        this.nsptPor3 = nsptPor3;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public int getProfundidade() {
        return profundidade;
    }

    public void setProfundidade(int profundidade) {
        this.profundidade = profundidade;
    }

}
