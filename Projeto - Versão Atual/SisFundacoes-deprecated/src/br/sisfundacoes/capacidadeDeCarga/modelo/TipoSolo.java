/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

/**
 *
 * @author Gabriel B Moro
 */
public enum TipoSolo {
    AREIA, AREIA_SITOSA, AREIA_SILTO_ARGILOSA,
    AREIA_ARGILOSA, AREIA_ARGILO_SILTOSA, SILTE,
    SILTE_ARENOSO, SILTE_ARENOSO_ARGILOSO, SILTE_ARGILOSO,
    SILTE_ARGILOSO_ARENOSO, ARGILA, ARGILA_ARENOSA,
    ARGILA_ARENO_SILTOSA, ARGILA_SILTOSA, ARGILA_SILO_ARENOSA;
}
