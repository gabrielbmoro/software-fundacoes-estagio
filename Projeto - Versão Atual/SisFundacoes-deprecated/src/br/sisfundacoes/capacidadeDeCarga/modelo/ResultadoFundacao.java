/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

/**
 * <b>Propósito:</b>
 * <br>
 * Reunir resultados para a geração de relatórios.
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link Projeto} para definir os resultados válidos e
 * pela classe {@link GeraPdf} para a geração dos relatórios.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class ResultadoFundacao {

    private ItemDeEnsaio itemDeEnsaio;
    private Solo solo;
    private Estaca estaca;
    private double qp_decourt;
    private double qp_decourt_reajustado;
    private double qp_aoki;
    private double qp_aoki_reajustado;
    private double qt_aoki;
    private double qt_aoki_FS;
    private double ql_decourt;
    private double ql_decourt_reajustado;
    private double ql_aoki;
    private double ql_aoki_reajustado;
    private double qt_decourt;
    private double qt_decourt_FS;
    private double areaDaBase;
    private double nsptMedio;
    private double nsptPor3;
    private double perimetro;
    private int deltaL;
    private double f1;
    private double f2;

    public Estaca getEstaca() {
        return estaca;
    }

    public void setEstaca(Estaca estaca) {
        this.estaca = estaca;
    }

    public ItemDeEnsaio getItemDeEnsaio() {
        return itemDeEnsaio;
    }

    public void setItemDeEnsaio(ItemDeEnsaio itemDeEnsaio) {
        this.itemDeEnsaio = itemDeEnsaio;
    }

    public Solo getSolo() {
        return solo;
    }

    public void setSolo(Solo solo) {
        this.solo = solo;
    }

    public double getQp_decourt() {
        return qp_decourt;
    }

    public void setQp_decourt(double qp_decourt) {
        this.qp_decourt = qp_decourt;
    }

    public double getQp_decourt_reajustado() {
        return qp_decourt_reajustado;
    }

    public void setQp_decourt_reajustado(double qp_decourt_reajustado) {
        this.qp_decourt_reajustado = qp_decourt_reajustado;
    }

    public double getQp_aoki() {
        return qp_aoki;
    }

    public void setQp_aoki(double qp_aoki) {
        this.qp_aoki = qp_aoki;
    }

    public double getQp_aoki_reajustado() {
        return qp_aoki_reajustado;
    }

    public void setQp_aoki_reajustado(double qp_aoki_reajustado) {
        this.qp_aoki_reajustado = qp_aoki_reajustado;
    }

    public double getQt_aoki() {
        return qt_aoki;
    }

    public void setQt_aoki(double qt_aoki) {
        this.qt_aoki = qt_aoki;
    }

    public double getQt_aoki_FS() {
        return qt_aoki_FS;
    }

    public void setQt_aoki_FS(double qt_aoki_FS) {
        this.qt_aoki_FS = qt_aoki_FS;
    }

    public double getQl_decourt() {
        return ql_decourt;
    }

    public void setQl_decourt(double ql_decourt) {
        this.ql_decourt = ql_decourt;
    }

    public double getQl_decourt_reajustado() {
        return ql_decourt_reajustado;
    }

    public void setQl_decourt_reajustado(double ql_decourt_reajustado) {
        this.ql_decourt_reajustado = ql_decourt_reajustado;
    }

    public double getQl_aoki() {
        return ql_aoki;
    }

    public void setQl_aoki(double ql_aoki) {
        this.ql_aoki = ql_aoki;
    }

    public double getQl_aoki_reajustado() {
        return ql_aoki_reajustado;
    }

    public void setQl_aoki_reajustado(double ql_aoki_reajustado) {
        this.ql_aoki_reajustado = ql_aoki_reajustado;
    }

    public double getQt_decourt() {
        return qt_decourt;
    }

    public void setQt_decourt(double qt_decourt) {
        this.qt_decourt = qt_decourt;
    }

    public double getQt_decourt_FS() {
        return qt_decourt_FS;
    }

    public void setQt_decourt_FS(double qt_decourt_FS) {
        this.qt_decourt_FS = qt_decourt_FS;
    }

    public double getAreaDaBase() {
        return areaDaBase;
    }

    public void setAreaDaBase(double areaDaBase) {
        this.areaDaBase = areaDaBase;
    }

    public double getNsptMedio() {
        return nsptMedio;
    }

    public void setNsptMedio(double nsptMedio) {
        this.nsptMedio = nsptMedio;
    }

    public double getNsptPor3() {
        return nsptPor3;
    }

    public void setNsptPor3(double nsptPor3) {
        this.nsptPor3 = nsptPor3;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public int getDeltaL() {
        return deltaL;
    }

    public void setDeltaL(int deltaL) {
        this.deltaL = deltaL;
    }

    public double getF1() {
        return f1;
    }

    public void setF1(double f1) {
        this.f1 = f1;
    }

    public double getF2() {
        return f2;
    }

    public void setF2(double f2) {
        this.f2 = f2;
    }

    
   
    
}
