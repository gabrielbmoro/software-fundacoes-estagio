/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

/**
 *
 * @author Gabriel B Moro
 */
public class Estaca {

    private int dimensao;
    private boolean lama;
    private int altura;
    private TipoEstaca tipo;

    public Estaca() {

    }
    
    public Estaca(int dimensao, TipoEstaca tipo)
    {
        this.dimensao = dimensao;
        this.tipo = tipo;
    }
   
    /**
     * @return the dimensao
     */
    public int getDimensao() {
        return dimensao;
    }

    /**
     * @param dimensao the dimensao to set
     */
    public void setDimensao(int dimensao) {
        this.dimensao = dimensao;
    }

    /**
     * @return the lama
     */
    public boolean isLama() {
        return lama;
    }

    /**
     * @param lama the lama to set
     */
    public void setLama(boolean lama) {
        this.lama = lama;
    }

    /**
     * @return the altura
     */
    public int getAltura() {
        return altura;
    }

    /**
     * @param altura the altura to set
     */
    public void setAltura(int altura) {
        this.altura = altura;
    }

    /**
     * @return the tipo
     */
    public TipoEstaca getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoEstaca tipo) {
        this.tipo = tipo;
    }

}
