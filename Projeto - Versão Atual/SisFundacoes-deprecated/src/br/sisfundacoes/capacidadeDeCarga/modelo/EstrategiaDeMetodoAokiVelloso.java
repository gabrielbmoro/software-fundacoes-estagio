/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

import br.sisfundacoes.capacidadeDeCarga.servicos.ServicoDeLog;

/**
 *
 * @author Gabriel B Moro
 */
public class EstrategiaDeMetodoAokiVelloso implements IEstrategiaCalculoDeCarga {

    public static double F1 = 3.0;
    public static double F2 = 6.0;
    public static int DELTA_L = 1;
    private double nsptMedio;
    private double perimetro;
    private double nsptDaCamada;
    private double areaDaBase;

    @Override
    public int calcularResistenciaDePonta(Estaca estaca, Solo solo) {
        if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
            F1 = 2.0;
        } else {
            F1 = 3.0;
        }

        double result = solo.getK() * nsptDaCamada * (areaDaBase / F1);
        ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                "======>>Qp Aoki, ----" + result + " (resultado) = ["
                + solo.getK() + "(k) * "
                + nsptDaCamada + "(nsptCamada) * "
                + areaDaBase + "(ab)] div "
                + F1 + "(Constante F1 da Estaca)");
        return (int) result;
    }

    @Override
    public int calcularResistenciaLateral(Estaca estaca, Solo solo) {
        if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
            F2 = 4.0;
        } else {
            F2 = 6.0;
        }
        int nsptMedioInt = (int) Math.ceil(nsptMedio);

        double result = (solo.getAlfa_usadoPorAokiVelloso() / 100.0) * solo.getK() * nsptMedio * perimetro * (DELTA_L / F2);
        ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                "======>>Ql Aoki, ----" + result
                + "= [" + solo.getAlfa_usadoPorAokiVelloso() + "(alfa) * "
                + solo.getK() + "(k) * "
                + nsptMedioInt + "(nsptMedio) * "
                + perimetro + "(perimetro) * "
                + DELTA_L + "(deltaL)] div "
                + F2 + "F2 da Estaca");

        return (int) result;
    }

    @Override
    public int calcularResistenciaTotal(int resistenciaDePonta, int resistenciaLateral) {
        return (resistenciaDePonta + resistenciaLateral);
    }

    public double getNsptMedio() {
        return nsptMedio;
    }

    public void setNsptMedio(double nsptMedio) {
        this.nsptMedio = nsptMedio;
    }

    public double getPerimetro() {
        return perimetro;
    }

    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    public double getNsptDaCamada() {
        return nsptDaCamada;
    }

    public void setNsptDaCamada(double nsptDaCamada) {
        this.nsptDaCamada = nsptDaCamada;
    }

    public double getAreaDaBase() {
        return areaDaBase;
    }

    public void setAreaDaBase(double areaDaBase) {
        this.areaDaBase = areaDaBase;
    }

}
