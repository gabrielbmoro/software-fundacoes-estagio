/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

/**
 *
 * @author Gabriel B Moro
 */
public enum TipoEstaca {
    FRANKI, HELICE_CONTINUA, PRE_MOLDADA_CIRCULAR, 
    PRE_MOLDADA_QUADRADA, RAIZ, ROTATIVA, STRAUS, 
    TRILHO;
}
