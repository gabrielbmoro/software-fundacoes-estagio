/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

import br.sisfundacoes.capacidadeDeCarga.servicos.ServicoDeLog;
import br.sisfundacoes.capacidadeDeCarga.servicos.UtilitariaDeCalculo;
import java.util.ArrayList;
import java.util.Date;
import org.apache.log4j.Logger;

/**
 *
 * @author Gabriel B Moro
 */
public class Projeto {

    private int[] faixaDeCalculo;
    private int passo;
    private int alternativa;
    private Cliente cliente;
    private Fundacao fundacao;
    private static Projeto instancia;
    private String referencia;
    private String perfil;
    private Date data;
    private ArrayList<ResultadoFundacao> linkedResultados = new ArrayList<>();

    private Projeto() {

    }

    public static Projeto obterInstancia() {
        if (instancia == null) {
            instancia = new Projeto();
        }
        return instancia;
    }

    public IEstrategiaCalculoDeCarga obterEstrategiaDeMetodoAokiVelloso() {
        return new EstrategiaDeMetodoAokiVelloso();
    }

    public IEstrategiaCalculoDeCarga obterEstrategiaDeMetodoDecourtQuaresma() {
        return new EstrategiaDeMetodoDecourtQuaresma();
    }

    public ArrayList<ResultadoFundacao> definirResultadosDeCapacidadeDeCarga() {
        int passo = 1;
        Projeto projeto = Projeto.obterInstancia();
        Fundacao fundacao = projeto.getFundacao();
        ArrayList<Solo> solos = fundacao.getSolos();
        ArrayList<Estaca> estacas = fundacao.getEstacas();

        ArrayList<ItemDeEnsaio> ensaios = fundacao.getEnsaios_profPorNSPT();

        ArrayList<ItemDeEnsaio> ensaiosUtilizados = ensaiosUtilizados(
                faixaDeCalculo[0], faixaDeCalculo[1], ensaios);

        int nsptPonta = 0;
        Solo soloValido = null;
        double ab = 0.0;
        int resultadoQp_aoki = 0, resultadoQp_decourt = 0, resultadoQl_aoki = 0,
                resultadoQl_decourt = 0, resultadoQt_aoki = 0, resultadoQt_decourt = 0;

        for (ItemDeEnsaio itemDeEnsaio : ensaiosUtilizados) {
            
            ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                    "=============Profundidade: " + itemDeEnsaio.getProfundidade() +
                             " Nspt: " + itemDeEnsaio.getNSPT() + "======");
            
            for (Estaca estacaTemp : estacas) {
                ResultadoFundacao resultadoFundacaoProfunda = new ResultadoFundacao();
                resultadoFundacaoProfunda.setItemDeEnsaio(new ItemDeEnsaio(itemDeEnsaio.getProfundidade(), itemDeEnsaio.getNSPT()));
               
                ServicoDeLog.getMyInstance().registrarLogDeInformacao(getClass(),
                        "-- Estaca de Dimensão: " + estacaTemp.getDimensao() + 
                                " Tipo de Estaca: " + String.valueOf(estacaTemp.getTipo()));
                
                soloValido = fundacao.recuperaSoloValido(
                        itemDeEnsaio.getProfundidade());
                ab = UtilitariaDeCalculo.calcularAreaDaBase(estacaTemp.getDimensao(), estacaTemp.getTipo());
                nsptPonta = itemDeEnsaio.getNSPT();

                double raio = UtilitariaDeCalculo.calcularRaio(estacaTemp.getDimensao());
                double perimetro = UtilitariaDeCalculo.calcularPerimetro(raio);
                resultadoFundacaoProfunda.setPerimetro(perimetro);
                double nsptMedio = fundacao.calcularNsptMedio(ensaios, itemDeEnsaio);

                soloValido.configurarValoresPorEstaca(estacaTemp);

                /*CALCULANDO MÉTODO DE AOKI*/
                EstrategiaDeMetodoAokiVelloso estrategiaPorAokiVelloso = (EstrategiaDeMetodoAokiVelloso) obterEstrategiaDeMetodoAokiVelloso();
                estrategiaPorAokiVelloso.setNsptMedio(nsptMedio);
                estrategiaPorAokiVelloso.setPerimetro(perimetro);
                estrategiaPorAokiVelloso.setAreaDaBase(ab);
                estrategiaPorAokiVelloso.setNsptDaCamada(itemDeEnsaio.getNSPT());
                
                resultadoQp_aoki = estrategiaPorAokiVelloso.calcularResistenciaDePonta(estacaTemp, soloValido);
                resultadoQl_aoki = resultadoQl_aoki + estrategiaPorAokiVelloso.calcularResistenciaLateral(estacaTemp, soloValido);
                resultadoQt_aoki = estrategiaPorAokiVelloso.calcularResistenciaTotal(resultadoQp_aoki,resultadoQl_aoki);

                double nsptPor3 = nsptPonta / 3.0;

                /*CALCULANDO MÉTODO DE DECOURT*/
                EstrategiaDeMetodoDecourtQuaresma estrategiaDeMetodoDecourtQuaresma = (EstrategiaDeMetodoDecourtQuaresma) obterEstrategiaDeMetodoDecourtQuaresma();
                estrategiaDeMetodoDecourtQuaresma.setAreaDaBase(ab);
                estrategiaDeMetodoDecourtQuaresma.setPerimetro(perimetro);
                estrategiaDeMetodoDecourtQuaresma.setNsptMedio(nsptMedio);
                estrategiaDeMetodoDecourtQuaresma.setNsptPor3(nsptPor3);
                estrategiaDeMetodoDecourtQuaresma.setProfundidade(itemDeEnsaio.getProfundidade());
                
                resultadoQp_decourt = estrategiaDeMetodoDecourtQuaresma.calcularResistenciaDePonta(estacaTemp, soloValido);
                resultadoQl_decourt = resultadoQl_decourt + estrategiaDeMetodoDecourtQuaresma.calcularResistenciaLateral(estacaTemp, soloValido);
                resultadoQt_decourt = estrategiaDeMetodoDecourtQuaresma.calcularResistenciaTotal(resultadoQp_decourt, resultadoQl_decourt);

                /*Definindo fatores globais*/
                double qp_aoki_fs = resultadoQp_aoki / 4;
                double ql_aoki_fs = (resultadoQl_aoki / 2);
                double qt_aoki_fs = qp_aoki_fs + ql_aoki_fs;
                double qp_decourt_fs = (resultadoQp_decourt / 4);
                double ql_decourt_fs = (resultadoQl_decourt / 2);
                double qt_decourt_fs = qp_decourt_fs + ql_decourt_fs;

                /*Adicionando resultados em uma estrutura de dados a fim de mapeamento*/
                resultadoFundacaoProfunda.setDeltaL(EstrategiaDeMetodoAokiVelloso.DELTA_L);
                resultadoFundacaoProfunda.setF1(EstrategiaDeMetodoAokiVelloso.F1);
                resultadoFundacaoProfunda.setF2(EstrategiaDeMetodoAokiVelloso.F2);
                resultadoFundacaoProfunda.setNsptPor3(nsptPor3);
                resultadoFundacaoProfunda.setAreaDaBase(ab);
                resultadoFundacaoProfunda.setNsptMedio(nsptMedio);
                resultadoFundacaoProfunda.setQp_aoki_reajustado(qp_aoki_fs);
                resultadoFundacaoProfunda.setQl_aoki(resultadoQl_aoki);
                resultadoFundacaoProfunda.setQl_aoki_reajustado(ql_aoki_fs);
                resultadoFundacaoProfunda.setQt_aoki(resultadoQt_aoki);
                resultadoFundacaoProfunda.setQt_aoki_FS(qt_aoki_fs);
                resultadoFundacaoProfunda.setQp_decourt(resultadoQp_decourt);
                resultadoFundacaoProfunda.setQl_decourt(resultadoQl_decourt);
                resultadoFundacaoProfunda.setQp_decourt_reajustado(qp_decourt_fs);
                resultadoFundacaoProfunda.setQl_decourt_reajustado(ql_decourt_fs);
                resultadoFundacaoProfunda.setQt_decourt(resultadoQt_decourt);
                resultadoFundacaoProfunda.setQt_decourt_FS(qt_decourt_fs);
                resultadoFundacaoProfunda.setSolo(soloValido);
                resultadoFundacaoProfunda.setQp_aoki(resultadoQp_aoki);
                resultadoFundacaoProfunda.setItemDeEnsaio(itemDeEnsaio);
                resultadoFundacaoProfunda.setEstaca(estacaTemp);

                linkedResultados.add(resultadoFundacaoProfunda);

            }
        }
        return linkedResultados;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por recuperar os ensaios utilizados de acordo com a
     * profundidade desejada.
     *
     * @param de de tipo {@link Double} que é a profundidade inicial
     * @param ate de tipo {@link Double} que é a profundidade final
     * @param ensaios de tipo {@link ArrayList} que é todos os ensaios
     * utilizados.
     * @return
     */
    public ArrayList<ItemDeEnsaio> ensaiosUtilizados(int de, int ate, ArrayList<ItemDeEnsaio> ensaios) {
        int countPasso = 0;
        ArrayList<ItemDeEnsaio> ensaiosUtilizados = new ArrayList<ItemDeEnsaio>();
        if (ate > ensaios.get(ensaios.size() - 1).getProfundidade()) {
            ate = ensaios.get(ensaios.size() - 1).getProfundidade();
        }
        for (int count = 0; count < ensaios.size(); count++) {
            if (ensaios.get(count).getProfundidade() >= de && ensaios.get(count).getProfundidade() <= ate) {
                ensaiosUtilizados.add(new ItemDeEnsaio(ensaios.get(count).getProfundidade(),
                        ensaios.get(count).getNSPT()));
            } else {
                continue;
            }
        }
        return ensaiosUtilizados;
    }

    public static void destruirEstancia()
    {
        instancia= null;
    }
    /**
     * @return the faixaDeCalculo
     */
    public int[] getFaixaDeCalculo() {
        return faixaDeCalculo;
    }

    /**
     * @param faixaDeCalculo the faixaDeCalculo to set
     */
    public void setFaixaDeCalculo(int[] faixaDeCalculo) {
        this.faixaDeCalculo = faixaDeCalculo;
    }

    /**
     * @return the passo
     */
    public int getPasso() {
        return passo;
    }

    /**
     * @param passo the passo to set
     */
    public void setPasso(int passo) {
        this.passo = passo;
    }

    /**
     * @return the alternativa
     */
    public int getAlternativa() {
        return alternativa;
    }

    /**
     * @param alternativa the alternativa to set
     */
    public void setAlternativa(int alternativa) {
        this.alternativa = alternativa;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the fundacao
     */
    public Fundacao getFundacao() {
        return fundacao;
    }

    /**
     * @param fundacao the fundacao to set
     */
    public void setFundacao(Fundacao fundacao) {
        this.fundacao = fundacao;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public ArrayList<ResultadoFundacao> getLinkedResultados() {
        return linkedResultados;
    }

    public void setLinkedResultados(ArrayList<ResultadoFundacao> linkedResultados) {
        this.linkedResultados = linkedResultados;
    }

    
    
}
