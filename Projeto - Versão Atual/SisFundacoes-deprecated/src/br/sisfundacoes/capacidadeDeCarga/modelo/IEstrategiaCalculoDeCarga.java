/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.modelo;

/**
 *
 * @author Gabriel B Moro
 */
public interface IEstrategiaCalculoDeCarga {
    
    public static double FS_GLOBAL = 2.5;
    public static double FS_LOCAL_PONTA = 4.0;
    public static double FS_LOCAL_ATRITO = 2.0;
    
    public int calcularResistenciaDePonta(Estaca estaca, Solo solo);
    
    public int calcularResistenciaLateral(Estaca estaca, Solo solo);
        
    public int calcularResistenciaTotal(int resistenciaDePonta, int resistenciaLateral);
    
}
