/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.servicos;

import java.io.File;
import java.util.Locale;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author Gabriel B Moro
 */
public class GeraTemplate {

    public boolean gerarTemplate(String diretorio) {
        if (!diretorio.equalsIgnoreCase("<caminho do arquivo escolhido>")) {
            WorkbookSettings configuracoesDoExcel = new WorkbookSettings();
            configuracoesDoExcel.setLocale(new Locale("en", "EN"));
            WritableFont manipuladorDeFonteTitulo = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
            WritableFont manipuladorDeFonteCabecalhoSimples = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
            WritableCellFormat manipuladorFormCelulaTitulo = new WritableCellFormat(manipuladorDeFonteTitulo);
            WritableCellFormat manipuladorFormatoDeCelula = new WritableCellFormat(manipuladorDeFonteCabecalhoSimples);
            try {
                diretorio = diretorio + File.separator + "TemplateParaImportacao.xls";
                WritableWorkbook canalDeEscrita = Workbook.createWorkbook(new File(diretorio), configuracoesDoExcel);
                WritableSheet abaDadosDeProjeto = canalDeEscrita.createSheet(GeraExcel.NOME_DA_ABA1, 0);

                Label labelTitulo = new Label(0, 0, "SisFundacoes", manipuladorFormCelulaTitulo);
                abaDadosDeProjeto.addCell(labelTitulo);
                abaDadosDeProjeto.mergeCells(0, 0, 4, 0);

                Label perfil = new Label(0, 3, "Perfil: ", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(perfil);
                Label alternativa = new Label(0, 4, "Alternativa: ", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(alternativa);
                Label referencia = new Label(0, 5, "Referencia: ", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(referencia);
                Label tipoDeEstaca = new Label(0, 6, "Tipo de Estaca: ", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(tipoDeEstaca);

                Label cabecalhoProf = new Label(0, 9, "Profundidade (m)", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(cabecalhoProf);
                Label cabecalhoNspt = new Label(1, 9, "Nspt", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(cabecalhoNspt);

                Label exemploProf = new Label(0, 10, "1");
                abaDadosDeProjeto.addCell(exemploProf);

                Label cabecalhoSolo = new Label(0, 14, "Solo", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(cabecalhoSolo);
                Label exemploDeProf = new Label(1, 15, "0");
                abaDadosDeProjeto.addCell(exemploDeProf);

                Label cabecalhoSoloDe = new Label(1, 14, "De (m)", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(cabecalhoSoloDe);
                Label cabecalhoSoloAte = new Label(2, 14, "Ate (m)", manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(cabecalhoSoloAte);

                canalDeEscrita.write();
                canalDeEscrita.close();
                return true;
            } catch (Exception erro) {
                ServicoDeLog.getMyInstance().registrarLogDeErro(getClass(), erro.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }
}
