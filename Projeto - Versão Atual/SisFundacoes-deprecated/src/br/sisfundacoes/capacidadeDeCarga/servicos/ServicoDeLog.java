/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.servicos;

import java.util.Calendar;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 *
 * @author gabriel-moro
 */
public class ServicoDeLog {
    
    private static ServicoDeLog myInstance;
    
    private ServicoDeLog(){
        
    }
    
    public static ServicoDeLog getMyInstance() {
        if(myInstance==null) {
            BasicConfigurator.configure();
            myInstance = new ServicoDeLog();
        }
        return myInstance;
    }
    
    public void registrarLogDeInformacao(Class classe, String mensagem) {
        Logger log4j = Logger.getLogger(classe);
        Calendar c = Calendar.getInstance();
        
        log4j.info("["+ c.getTime().toString() +"] : " + mensagem);
    }
    
    public void registrarLogDeErro(Class classe, String mensagem) {
        Logger log4j = Logger.getLogger(classe);
        Calendar c = Calendar.getInstance();
        
        log4j.error("["+ c.getTime().toString() +"] : " + mensagem);
    }
    
    public void registrarLogDeWarning(Class classe, String mensagem) {
        Logger log4j = Logger.getLogger(classe);
        Calendar c = Calendar.getInstance();
        
        log4j.warn("["+ c.getTime().toString() +"] : " + mensagem);
    }
    
}
