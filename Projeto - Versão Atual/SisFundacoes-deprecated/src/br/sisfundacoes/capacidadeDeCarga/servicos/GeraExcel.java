/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.servicos;

import br.sisfundacoes.capacidadeDeCarga.modelo.Estaca;
import br.sisfundacoes.capacidadeDeCarga.modelo.ItemDeEnsaio;
import br.sisfundacoes.capacidadeDeCarga.modelo.ResultadoFundacao;
import br.sisfundacoes.capacidadeDeCarga.modelo.Solo;
import br.sisfundacoes.visao.GeradorDeMensagem;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Gabriel B Moro
 */
public class GeraExcel extends GeradorDeRelatorio {

    private WorkbookSettings configuracoesDoExcel;
    public static final String NOME_DA_ABA1 = "Dados do Projeto";
    public static final String NOME_DA_ABA2 = "Resultados Adquiridos";
    private WritableWorkbook canalDeEscrita;
    private WritableSheet abaDadosDeProjeto;
    private WritableSheet abaResultadosDeProjeto;
    private WritableFont manipuladorDeFonteCabecalhoSimples;
    private WritableFont manipuladorDeFonteTitulo;
    private WritableCellFormat manipuladorFormatoDeCelula;
    private WritableCellFormat manipuladorFormCelulaTitulo;

    public GeraExcel() {
        configurarObjetosNecessarios();
    }

    private void configurarObjetosNecessarios() {
        this.configuracoesDoExcel = new WorkbookSettings();
        this.configuracoesDoExcel.setLocale(new Locale("en", "EN"));
        this.manipuladorDeFonteTitulo = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
        this.manipuladorDeFonteCabecalhoSimples = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        this.manipuladorFormatoDeCelula = new WritableCellFormat(this.manipuladorDeFonteCabecalhoSimples);
        this.manipuladorFormCelulaTitulo = new WritableCellFormat(this.manipuladorDeFonteTitulo);
    }

    public void executaExcel(HashMap<CHAVE_UTILIZADA, Object> parametros, String diretorio, String titulo) {
        try {
            File file = new File(diretorio);
            if (!file.isDirectory()) {
                diretorio = System.getProperty("user.home") + titulo;
                GeradorDeMensagem.exibirMensagemDeInformacao("O relatório pode ser encontrado no seguinte endereço padrão: \n"
                        + "<sua pasta de usuário>//", titulo);
            } else {
                diretorio = diretorio + File.separator + titulo;
            }

            canalDeEscrita = Workbook.createWorkbook(new File(diretorio), configuracoesDoExcel);
            abaDadosDeProjeto = canalDeEscrita.createSheet(NOME_DA_ABA1, 0);

            Label labelTitulo = new Label(0, 0, "SisFundacoes", this.manipuladorFormCelulaTitulo);
            abaDadosDeProjeto.addCell(labelTitulo);
            abaDadosDeProjeto.mergeCells(0, 0, 4, 0);

            Label perfil = new Label(0, 3, "Perfil: ", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(perfil);
            Label perfilValue = new Label(1, 3, parametros.get(CHAVE_UTILIZADA.PERFIL).toString());
            abaDadosDeProjeto.addCell(perfilValue);
            Label alternativa = new Label(0, 4, "Alternativa: ", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(alternativa);
            Label alternativaValue = new Label(1, 4, parametros.get(CHAVE_UTILIZADA.ALTERNATIVA).toString());
            abaDadosDeProjeto.addCell(alternativaValue);
            Label referencia = new Label(0, 5, "Referencia: ", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(referencia);
            Label referenciaValue = new Label(1, 5, parametros.get(CHAVE_UTILIZADA.REFERENCIA).toString());
            abaDadosDeProjeto.addCell(referenciaValue);
            Label tipoDeEstaca = new Label(0, 6, "Tipo de Estaca: ", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(tipoDeEstaca);
            Label tipoDeEstacaValue = new Label(1, 6, recuperaParametroTextoEstaca((Estaca) parametros.get(CHAVE_UTILIZADA.TIPO_DE_ESTACA)));
            abaDadosDeProjeto.addCell(tipoDeEstacaValue);

            Label cabecalhoProf = new Label(0, 9, "Profundidade (m)", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(cabecalhoProf);
            Label cabecalhoNspt = new Label(1, 9, "Nspt", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(cabecalhoNspt);

            ArrayList<ItemDeEnsaio> ensaiosRealizados = (ArrayList<ItemDeEnsaio>) parametros.get(CHAVE_UTILIZADA.PROFUNDIDADE_NSPT);
            int linha = 10;
            for (ItemDeEnsaio itemTemp : ensaiosRealizados) {
                Label profundTemp = new Label(0, linha, String.valueOf(itemTemp.getProfundidade()));
                abaDadosDeProjeto.addCell(profundTemp);
                Label nsptTemp = new Label(1, linha, String.valueOf(itemTemp.getNSPT()));
                abaDadosDeProjeto.addCell(nsptTemp);
                linha += 1;
            }
            linha += 1;

            Label cabecalhoSolo = new Label(0, linha, "Solo", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(cabecalhoSolo);
            Label cabecalhoSoloDe = new Label(1, linha, "De (m)", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(cabecalhoSoloDe);
            Label cabecalhoSoloAte = new Label(2, linha, "Ate (m)", this.manipuladorFormatoDeCelula);
            abaDadosDeProjeto.addCell(cabecalhoSoloAte);

            linha += 1;
            ArrayList<Solo> solos = (ArrayList<Solo>) parametros.get(CHAVE_UTILIZADA.SOLOS);
            for (Solo soloTemp : solos) {
                Label itmSolo = new Label(0, linha, recuperaParametroTextoTipoDeSolo(soloTemp.getTipo()));
                abaDadosDeProjeto.addCell(itmSolo);
                Label itmDe = new Label(1, linha, String.valueOf(soloTemp.getDe()));
                abaDadosDeProjeto.addCell(itmDe);
                Label itmAte = new Label(2, linha, String.valueOf(soloTemp.getAte()));
                abaDadosDeProjeto.addCell(itmAte);
                linha += 1;
            }
            linha++;
            if (parametros.get(CHAVE_UTILIZADA.DATA) != null) {
                Label data = new Label(0, linha, "Data: ", this.manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(data);
                Label dataValue = new Label(1, linha, String.valueOf(parametros.get(CHAVE_UTILIZADA.DATA)), this.manipuladorFormatoDeCelula);
                abaDadosDeProjeto.addCell(dataValue);
                linha++;
            }
            escreverTabelaResultados(parametros);

            this.canalDeEscrita.write();
            this.canalDeEscrita.close();

            GeradorDeMensagem.exibirMensagemDeInformacao("O relatório Excel foi gerado com sucesso!", "Alerta ao Usuário");

        } catch (Exception erro1) {
            GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um erro na geração do relatório, favor realize novamente a operação!");
            ServicoDeLog.getMyInstance().registrarLogDeErro(getClass(), erro1.getMessage());
        }
    }

    private void escreverTabelaResultados(HashMap parametros) {
        abaResultadosDeProjeto = canalDeEscrita.createSheet(NOME_DA_ABA2, 1);
        try {
            ArrayList<ResultadoFundacao> resultadosFundacaoProfunda = (ArrayList<ResultadoFundacao>) parametros.get(CHAVE_UTILIZADA.RESULTADOS_GERADOS);

            Label labelTitulo = new Label(0, 1, "Resultados Adquiridos - Capacidade de Carga", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(labelTitulo);
            abaResultadosDeProjeto.mergeCells(0, 1, 6, 1);

            Label estacaCabecalho = new Label(0, 3, "Estaca (cm)", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(estacaCabecalho);

            Label metodoDecourtCabecalho = new Label(1, 3, "Método Decóurt-Quaresma", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(metodoDecourtCabecalho);
            abaResultadosDeProjeto.mergeCells(1, 3, 6, 3);

            Label metodoAokiCabecalho = new Label(8, 3, "Método Aoki-Velloso", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(metodoAokiCabecalho);
            abaResultadosDeProjeto.mergeCells(8, 3, 13, 3);

            abaResultadosDeProjeto.mergeCells(0, 3, 0, 4);

            Label qpDecourtCabecalho = new Label(1, 4, "Qp", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qpDecourtCabecalho);
            Label qlDecourtCabecalho = new Label(2, 4, "Ql", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qlDecourtCabecalho);
            Label qtDecourtCabecalho = new Label(3, 4, "Qt", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qtDecourtCabecalho);
            Label qpDecourtCabecalhos = new Label(4, 4, "[Qp/FS", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qpDecourtCabecalhos);
            Label qlDecourtCabecalhos1 = new Label(5, 4, "Ql/FS", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qlDecourtCabecalhos1);
            Label qtDecourtCabecalhos2 = new Label(6, 4, "Qt]", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qtDecourtCabecalhos2);

            Label qpAokiCabecalho = new Label(8, 4, "Qp", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qpAokiCabecalho);
            Label qlAokiCabecalho = new Label(9, 4, "Ql", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qlAokiCabecalho);
            Label qtAokiCabecalho = new Label(10, 4, "Qt", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qtAokiCabecalho);
            Label qpDecourtCabecalhosd = new Label(11, 4, "[Qp/FS", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qpDecourtCabecalhosd);
            Label qlDecourtCabecalhosd1 = new Label(12, 4, "Ql/FS", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qlDecourtCabecalhosd1);
            Label qtDecourtCabecalhosd2 = new Label(13, 4, "Qt]", this.manipuladorFormatoDeCelula);
            abaResultadosDeProjeto.addCell(qtDecourtCabecalhosd2);

            int linha = 4;
            linha++;

            int profundidade = 0;
            for (ResultadoFundacao resultadoTemp : resultadosFundacaoProfunda) {
                if (profundidade == 0 || resultadoTemp.getItemDeEnsaio().getProfundidade() != profundidade) {
                    Label profundidadeTemp = new Label(0, linha, "L (m) = " + resultadoTemp.getItemDeEnsaio().getProfundidade());
                    abaResultadosDeProjeto.addCell(profundidadeTemp);
                    abaResultadosDeProjeto.mergeCells(0, linha, 14, linha);
                    linha++;
                    profundidade = resultadoTemp.getItemDeEnsaio().getProfundidade();
                }
                Label dimensaoEstacaTemp = new Label(0, linha, String.valueOf(resultadoTemp.getEstaca().getDimensao()));
                abaResultadosDeProjeto.addCell(dimensaoEstacaTemp);
                Label qpDec = new Label(1, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQp_decourt()));
                abaResultadosDeProjeto.addCell(qpDec);
                Label qlDec = new Label(2, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQl_decourt()));
                abaResultadosDeProjeto.addCell(qlDec);
                Label qtDec = new Label(3, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQt_decourt()));
                abaResultadosDeProjeto.addCell(qtDec);

                Label qpDecFS = new Label(4, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQp_decourt_reajustado()));
                abaResultadosDeProjeto.addCell(qpDecFS);
                Label qlDecFS = new Label(5, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQl_decourt_reajustado()));
                abaResultadosDeProjeto.addCell(qlDecFS);
                Label qtDecFS = new Label(6, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQt_decourt_FS()));
                abaResultadosDeProjeto.addCell(qtDecFS);

                Label qpAok = new Label(8, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQp_aoki()));
                abaResultadosDeProjeto.addCell(qpAok);
                Label qlAok = new Label(9, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQl_aoki()));
                abaResultadosDeProjeto.addCell(qlAok);
                Label qtAok = new Label(10, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQt_aoki()));
                abaResultadosDeProjeto.addCell(qtAok);

                Label qpAokiFS = new Label(11, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQp_aoki_reajustado()));
                abaResultadosDeProjeto.addCell(qpAokiFS);
                Label qlAokiFS = new Label(12, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQl_aoki_reajustado()));
                abaResultadosDeProjeto.addCell(qlAokiFS);
                Label qtAokiFS = new Label(13, linha, UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultadoTemp.getQt_aoki_FS()));
                abaResultadosDeProjeto.addCell(qtAokiFS);

                linha++;
            }
            linha += 5;
            Label rodape = new Label(0, linha, "Todos os direitos reservados - SisFundacoes");
            abaResultadosDeProjeto.addCell(rodape);
            abaResultadosDeProjeto.mergeCells(0, linha, 9, linha);
        } catch (WriteException erro1) {
            ServicoDeLog.getMyInstance().registrarLogDeErro(getClass(), erro1.getMessage());
        }

    }
}
