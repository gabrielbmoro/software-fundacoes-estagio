/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.capacidadeDeCarga.servicos;

import br.sisfundacoes.capacidadeDeCarga.modelo.Estaca;
import br.sisfundacoes.capacidadeDeCarga.modelo.TipoEstaca;
import br.sisfundacoes.capacidadeDeCarga.modelo.TipoSolo;

/**
 *
 * @author Gabriel B Moro
 */
public class GeradorDeRelatorio {
    
    protected String recuperaParametroTextoEstaca(Estaca temp) {
        String tipoDaEstaca = "";
        if (temp.getTipo()==TipoEstaca.FRANKI) {
            tipoDaEstaca = "Franki (FK)";
        } else if (temp.getTipo()==TipoEstaca.HELICE_CONTINUA) {
            tipoDaEstaca = "Hélice Contínua (HC)";
        } else if (temp.getTipo()==TipoEstaca.PRE_MOLDADA_CIRCULAR) {
            tipoDaEstaca = "Pré-moldada Circular (PM)";
        } else if(temp.getTipo()==TipoEstaca.PRE_MOLDADA_QUADRADA){
            tipoDaEstaca = "Pré-moldada Quadrada (PQ)";
        }else if (temp.getTipo()==TipoEstaca.ROTATIVA) {
            tipoDaEstaca = "Rotativa (RT)";
        } else if (temp.getTipo()==TipoEstaca.RAIZ) {
            tipoDaEstaca = "Raíz ou Injetadas de Pequeno Diâmetro (RZ)";
        } else if (temp.getTipo()==TipoEstaca.STRAUS) {
            tipoDaEstaca = "Strauss (ST)";
        } else if (temp.getTipo()==TipoEstaca.TRILHO) {
            tipoDaEstaca = "Trilho (TR)";
        } else {
            tipoDaEstaca = "Estaca não definida";
        }
        return tipoDaEstaca;
    }
    protected String recuperaParametroTextoTipoDeSolo(TipoSolo tipoDeSolo) {
        switch (tipoDeSolo) {
            case AREIA:
                return "Areia";
            case AREIA_SITOSA:
                return "Areia Sitosa";
            case AREIA_SILTO_ARGILOSA:
                return "Areia Silto Argilosa";
            case AREIA_ARGILOSA:
                return "Areia Argiloso";
            case AREIA_ARGILO_SILTOSA:
                return "Areia Argilo Siltosa";
            case SILTE:
                return "Silte";
            case SILTE_ARENOSO:
                return "Silte Arenoso";
            case SILTE_ARENOSO_ARGILOSO:
                return "Silte Arenoso Argiloso";
            case SILTE_ARGILOSO:
                return "Silte Argiloso";
            case SILTE_ARGILOSO_ARENOSO:
                return "Silte Argiloso Arenoso";
            case ARGILA:
                return "Argila";
            case ARGILA_ARENOSA:
                return "Argila Arenosa";
            case ARGILA_ARENO_SILTOSA:
                return "Argila Areno Siltosa";
            case ARGILA_SILTOSA:
                return "Argila Siltosa";
            case ARGILA_SILO_ARENOSA:
                return "Argila Silto Arenosa";
            default:
                return "Solo não especificado";
        }
    }
}
