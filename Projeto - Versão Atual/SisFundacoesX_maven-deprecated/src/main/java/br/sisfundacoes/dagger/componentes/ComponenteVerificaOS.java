/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.componentes;

import br.sisfundacoes.dagger.modulos.ModuloVerificaOS;
import br.sisfundacoes.visao.capacidade_de_carga.JFramePrincipalCarga;
import dagger.Component;

/**
 *
 * @author Gabriel B Moro
 */
@Component(modules = {ModuloVerificaOS.class})
public interface ComponenteVerificaOS {
    
    ModuloVerificaOS forneceInstanciaDeVerificaOS();
    
    void inject(JFramePrincipalCarga jframe);
    
}
