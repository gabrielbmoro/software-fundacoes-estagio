/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.modulos;

import br.sisfundacoes.modelo.TipoEstaca;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Gabriel B Moro
 */
public class Utilitaria {

    
    public static String retornarTextoDeResultadoDouble(double valor) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        return nf.format(valor);
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para calcular a área da base da estaca de acordo com o
     * seu diâmetro.
     *
     * @param diametro de tipo {@link Double} varia de estaca para estaca
     * @return de tipo {@link Double}
     */
    public static double calcularAreaDaBase(double diametro, TipoEstaca tipoEstaca) {
        diametro = diametro / 100.0;
        if (tipoEstaca != TipoEstaca.PRE_MOLDADA_QUADRADA) {
            return (Math.PI * (diametro * diametro) / 4.0);
        } else {
            return diametro * diametro;
        }
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método utilizado para calcular o raio do diâmetro da estaca
     *
     * @param diametro de tipo {@link Double} que varia de acordo com a dimensão
     * da estaca.
     * @return de tipo {@link Double}
     */
    public static double calcularRaio(double diametro) {
        return diametro / 2.0;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por calcular o perímetro de uma estaca.
     *
     * @param raio de tipo {@link Double} que varia de acordo com a dimensão da
     * estaca utilizada.
     * @return de tipo {@link Double}
     */
    public static double calcularPerimetro(double raio) {
        return (2 * Math.PI * raio) / 100.0;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por calcular a média de acordo com os valores
     * informados por parâmetros.
     *
     * @param valores de tipo {@link Integer}
     * @return de tipo {@link Double}
     */
    public static double calcularMedia(List<Integer> valores) {
        double total = 0.0;
        for (int count = 0; count < valores.size(); count++) {
            total = total + valores.get(count);
        }
        return total / (double) valores.size();
    }

    public static String retornaDataAtual(){
        Date data = new Date();
        SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
        return formatador.format(data);
    }
    
     public static Date retornaData(){
        Date data = new Date();
        return data;
    }
     
    public static String recuperaDiretorioDeUsuario() {
        return System.getProperty("user.dir");
    }
    
}
