/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.componentes;

import br.sisfundacoes.dagger.modulos.ModuloEstrategiaCalculoDeCarga;
import br.sisfundacoes.modelo.Projeto;
import dagger.Component;

/**
 *
 * @author Gabriel B Moro
 */
@Component(modules = {ModuloEstrategiaCalculoDeCarga.class})

public interface ComponenteDeCapacidadeDeCarga {
    
    ModuloEstrategiaCalculoDeCarga forneceEstrategiaDeCalculo();
        
    void inject(Projeto projeto);
    
}
