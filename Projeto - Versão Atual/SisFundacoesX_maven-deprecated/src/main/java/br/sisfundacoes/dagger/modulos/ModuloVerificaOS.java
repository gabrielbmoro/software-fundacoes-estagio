/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.modulos;

import br.sisfundacoes.main.ServicoDeLog;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 *
 * @author gabrielbmoro
 */
@Module
@Singleton
public class ModuloVerificaOS {

    @Provides
    ModuloVerificaOS forneceInstancia() {
        return new ModuloVerificaOS();
    }

    public boolean isLinux() {
        String texto = System.getProperty("os.name");
        if (texto.equalsIgnoreCase("Linux")) {
            ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(), "Sistema operacional Linux!");
            return true;
        } else {
            ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(), "Sistema operacional Windows!");
            return false;
        }
    }
}
