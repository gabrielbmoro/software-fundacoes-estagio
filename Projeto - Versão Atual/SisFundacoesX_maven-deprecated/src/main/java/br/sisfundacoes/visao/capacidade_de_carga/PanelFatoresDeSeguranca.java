/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.visao.capacidade_de_carga;

import br.sisfundacoes.main.ServicoDeLog;
import br.sisfundacoes.modelo.Projeto;
import br.sisfundacoes.modelo.pojo.ResultadoFundacao;
import br.sisfundacoes.dagger.modulos.Utilitaria;
import br.sisfundacoes.main.FabricaDeComponentes;
import br.sisfundacoes.modelo.TipoEstaca;
import br.sisfundacoes.visao.GeradorDeMensagem;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

/**
 * @author GabrielBMoro
 * @version 0.4
 * @since 24/01/2014
 */
public class PanelFatoresDeSeguranca extends javax.swing.JPanel {

    private DefaultTableModel modeloTabelaDecourt;
    private DefaultTableModel modeloTabelaAoki;
    private JFramePrincipalCarga framePrincipalCarga;

    /**
     * Creates new form PanelFatoresDeSeguranca
     */
    public PanelFatoresDeSeguranca(JFramePrincipalCarga framePrincipalCarga) {
        initComponents();

        this.modeloTabelaDecourt = (DefaultTableModel) jTableDecourt.getModel();
        this.modeloTabelaAoki = (DefaultTableModel) jTableAoki.getModel();

        this.jFormattedTextPasso.setEnabled(false);
        this.jFsResistenciaPonta.setEnabled(true);
        this.jFsGlobal.setEnabled(true);
        this.jFsResistenciaPonta.setEnabled(true);
        this.jFsAtritoLateral.setEnabled(true);

        this.jPanelDemonstrativo.setVisible(false);
        this.jScrollPaneDecourt.setVisible(false);
        this.jScrollPaneAoki.setVisible(false);
        this.btnLiberaNovoCalculo.setEnabled(false);

        this.framePrincipalCarga = framePrincipalCarga;
        
        revalidate();
        repaint();
    }

    
    public void configurarValorDeTensao(TipoEstaca tipo){
        
        Double tensaoDeEstaca = 0.0;
        
        switch(tipo) {
            case HELICE_CONTINUA:
                tensaoDeEstaca = 60.0;
                break;
            case ROTATIVA:
                tensaoDeEstaca = 40.0;
                break;
            case RAIZ:
                tensaoDeEstaca = 150.0;
                break;
            case PRE_MOLDADA_CIRCULAR:
                tensaoDeEstaca = 70.0;
                break;
            case PRE_MOLDADA_QUADRADA:
                tensaoDeEstaca = 70.0;
                break;
            case STRAUS:
                tensaoDeEstaca = 25.0;
                break;
            case TRILHO:
                tensaoDeEstaca = 500.0;
                break;
            default:
                tensaoDeEstaca = 60.0;
                break;
        }
        
        this.jFormattedTensaoDoCalculo.setText(String.valueOf(tensaoDeEstaca));
    }
    
    private void configurarValoresEmTabela(ArrayList<ResultadoFundacao> resultados) {

        txtEstacaUtilizada.setText(String.valueOf(Projeto.obterInstancia().getFundacao().
                getEstacas().get(0).getTipo()));

        this.modeloTabelaDecourt.setNumRows(0);
        this.modeloTabelaAoki.setNumRows(0);

        for (ResultadoFundacao resultadoTemp : resultados) {
            this.modeloTabelaDecourt.addRow(new Object[]{
                resultadoTemp.getItemDeEnsaio().getProfundidade(),
                resultadoTemp.getEstaca().getDimensao(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getAreaDaBase()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getPerimetro()),
                resultadoTemp.getItemDeEnsaio().getNSPT(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getNsptMedio()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getNsptPor3()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getSolo().getAlfa_usadoPorDecourtQuaresma()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getSolo().getBeta_usadoPorDecourtQuaresma()),
                resultadoTemp.getSolo().getK(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQp_decourt()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQl_decourt()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQt_decourt()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQp_decourt_reajustado()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQl_decourt_reajustado()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQt_decourt_FS()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getResistenciaConcreto())
            });
            this.modeloTabelaAoki.addRow(new Object[]{
                resultadoTemp.getItemDeEnsaio().getProfundidade(),
                resultadoTemp.getEstaca().getDimensao(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getAreaDaBase()),
                resultadoTemp.getItemDeEnsaio().getNSPT(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getNsptMedio()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getPerimetro()),
                resultadoTemp.getDeltaL(),
                resultadoTemp.getF1(),
                resultadoTemp.getF2(),
                resultadoTemp.getSolo().getK(),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQp_aoki()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQl_aoki()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQt_aoki()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQp_aoki_reajustado()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQl_aoki_reajustado()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getQt_aoki_FS()),
                Utilitaria.retornarTextoDeResultadoDouble(resultadoTemp.getResistenciaConcreto())
            });
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jFormattedDeProf = new javax.swing.JFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        jFormattedAteProfu = new javax.swing.JFormattedTextField();
        jLabel13 = new javax.swing.JLabel();
        jFormattedTextPasso = new javax.swing.JFormattedTextField();
        btnOk4 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelDemonstrativo = new javax.swing.JPanel();
        btnDecourt = new javax.swing.JButton();
        btnAoki = new javax.swing.JButton();
        jScrollPaneDecourt = new javax.swing.JScrollPane();
        jTableDecourt = new javax.swing.JTable();
        jScrollPaneAoki = new javax.swing.JScrollPane();
        jTableAoki = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txtEstacaUtilizada = new javax.swing.JTextField();
        jLabelMetodoEscolhido = new javax.swing.JLabel();
        jPanelFatoresDeSeguranca = new javax.swing.JPanel();
        jFormattedTensaoDoCalculo = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        jFsResistenciaPonta = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        jFsGlobal = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jFsAtritoLateral = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jF1 = new javax.swing.JFormattedTextField();
        fF2 = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        btnLiberaNovoCalculo = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Fatores de Segurança"));

        jLabel10.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(1, 1, 1));
        jLabel10.setText("FAIXA DE CÁLCULO");

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setText("De (m):");

        jFormattedDeProf.setText("1");
        jFormattedDeProf.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel12.setText("Até (m):");

        jFormattedAteProfu.setText("23");
        jFormattedAteProfu.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jFormattedAteProfu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedAteProfuActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel13.setText("Passo (m) :");

        jFormattedTextPasso.setText("1");
        jFormattedTextPasso.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jFormattedTextPasso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTextPassoActionPerformed(evt);
            }
        });

        btnOk4.setBackground(new java.awt.Color(255, 255, 0));
        btnOk4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnOk4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/calcular.png"))); // NOI18N
        btnOk4.setText("Realizar Cálculo");
        btnOk4.setToolTipText("Calcular");
        btnOk4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOk4ActionPerformed(evt);
            }
        });

        btnDecourt.setText("Decóurt-Quaresma");
        btnDecourt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecourtActionPerformed(evt);
            }
        });

        btnAoki.setText("Aoki-Velloso");
        btnAoki.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAokiActionPerformed(evt);
            }
        });

        jTableDecourt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Prof", "Dia", "Ab", "Per", "Nspt Cam", "Nspt Med", "Nspt/3", "Alfa", "Beta", "K", "Qp", "Ql", "Qt", "[Qp/Fs", "Ql/Fs", "Qt]", "Qt_c"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableDecourt.getTableHeader().setReorderingAllowed(false);
        jScrollPaneDecourt.setViewportView(jTableDecourt);

        jTableAoki.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Prof", "Dia", "Ab", "Nspt", "Nspt Med", "Per", "DeltaL", "F1", "F2", "K", "Qp", "Ql", "Qt", "[Qp/FS", "Ql/FS", "Qt]", "Qt_c"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableAoki.getTableHeader().setReorderingAllowed(false);
        jScrollPaneAoki.setViewportView(jTableAoki);

        jLabel1.setText("Estaca Utilizada:");

        txtEstacaUtilizada.setEditable(false);

        jLabelMetodoEscolhido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabelMetodoEscolhido.setForeground(new java.awt.Color(51, 51, 255));
        jLabelMetodoEscolhido.setText("Método Escolhido:");

        javax.swing.GroupLayout jPanelDemonstrativoLayout = new javax.swing.GroupLayout(jPanelDemonstrativo);
        jPanelDemonstrativo.setLayout(jPanelDemonstrativoLayout);
        jPanelDemonstrativoLayout.setHorizontalGroup(
            jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDemonstrativoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPaneDecourt, javax.swing.GroupLayout.DEFAULT_SIZE, 922, Short.MAX_VALUE)
                    .addComponent(jScrollPaneAoki)
                    .addGroup(jPanelDemonstrativoLayout.createSequentialGroup()
                        .addGroup(jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelDemonstrativoLayout.createSequentialGroup()
                                .addComponent(btnDecourt)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnAoki))
                            .addGroup(jPanelDemonstrativoLayout.createSequentialGroup()
                                .addComponent(jLabelMetodoEscolhido)
                                .addGap(86, 86, 86)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtEstacaUtilizada, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelDemonstrativoLayout.setVerticalGroup(
            jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDemonstrativoLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDecourt)
                    .addComponent(btnAoki))
                .addGap(18, 18, 18)
                .addGroup(jPanelDemonstrativoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtEstacaUtilizada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelMetodoEscolhido))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPaneDecourt, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPaneAoki, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jFormattedTensaoDoCalculo.setText("60.0");
        jFormattedTensaoDoCalculo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jFormattedTensaoDoCalculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFormattedTensaoDoCalculoActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel8.setText("Tensão do Cálculo de Concreto (kg/cm²): ");

        jFsResistenciaPonta.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        jFsResistenciaPonta.setText("4,00");
        jFsResistenciaPonta.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel4.setText("FS resistência da ponta: ");

        jFsGlobal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        jFsGlobal.setText("2,5");
        jFsGlobal.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jFsGlobal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFsGlobalActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setText("FS global: ");

        jFsAtritoLateral.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        jFsAtritoLateral.setText("2,0");
        jFsAtritoLateral.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jFsAtritoLateral.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFsAtritoLateralActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("FS atrito lateral: ");

        jLabel6.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel6.setText("F1: ");

        jF1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        jF1.setText("3,0");
        jF1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jF1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jF1ActionPerformed(evt);
            }
        });

        fF2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        fF2.setText("6,0");
        fF2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        fF2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fF2ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setText("F2: ");

        javax.swing.GroupLayout jPanelFatoresDeSegurancaLayout = new javax.swing.GroupLayout(jPanelFatoresDeSeguranca);
        jPanelFatoresDeSeguranca.setLayout(jPanelFatoresDeSegurancaLayout);
        jPanelFatoresDeSegurancaLayout.setHorizontalGroup(
            jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFsAtritoLateral, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFsGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jFsResistenciaPonta, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jFormattedTensaoDoCalculo, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(145, 145, 145))
            .addGroup(jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jF1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(fF2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelFatoresDeSegurancaLayout.setVerticalGroup(
            jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFatoresDeSegurancaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(fF2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(jF1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jFsAtritoLateral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jFsGlobal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jFsResistenciaPonta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelFatoresDeSegurancaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jFormattedTensaoDoCalculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnLiberaNovoCalculo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/verificarArquivo.png"))); // NOI18N
        btnLiberaNovoCalculo.setText("Liberar Novo Cálculo");
        btnLiberaNovoCalculo.setToolTipText("Liberar Novo Cálculo");
        btnLiberaNovoCalculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLiberaNovoCalculoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelDemonstrativo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 932, Short.MAX_VALUE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jLabel9)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jFormattedDeProf, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel12)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jFormattedAteProfu, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel13)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jFormattedTextPasso, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanelFatoresDeSeguranca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(btnOk4)
                .addGap(29, 29, 29)
                .addComponent(btnLiberaNovoCalculo)
                .addGap(0, 578, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelFatoresDeSeguranca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel10)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jFormattedDeProf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jFormattedAteProfu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jFormattedTextPasso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk4)
                    .addComponent(btnLiberaNovoCalculo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelDemonstrativo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jFsGlobalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFsGlobalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFsGlobalActionPerformed

    private void jFormattedAteProfuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedAteProfuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedAteProfuActionPerformed

    private void jFsAtritoLateralActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFsAtritoLateralActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFsAtritoLateralActionPerformed

    private void jFormattedTensaoDoCalculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTensaoDoCalculoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedTensaoDoCalculoActionPerformed

    private void jFormattedTextPassoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFormattedTextPassoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jFormattedTextPassoActionPerformed

    private void btnOk4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOk4ActionPerformed
        try {
            int de = Integer.parseInt(jFormattedDeProf.getText());
            int ate = Integer.parseInt(jFormattedAteProfu.getText());
            int passo = Integer.parseInt(jFormattedTextPasso.getText());

            Projeto projeto = Projeto.obterInstancia();

            projeto.setFaixaDeCalculo(new int[]{de, ate});
            projeto.setPasso(passo);

            double fsglobal = 0.0, fsatrito = 0.0, fsponta = 0.0,
                    f1double = 0.0, f2double = 0.0, tensaoDoConcreto = 0.0;

            fsglobal = Double.parseDouble(jFsGlobal.getText().replaceAll(",", "."));
            fsatrito = Double.parseDouble(jFsAtritoLateral.getText().replaceAll(",", "."));
            fsponta = Double.parseDouble(jFsResistenciaPonta.getText().replaceAll(",", "."));
            f1double = Double.parseDouble(jF1.getText().replaceAll(",","."));
            f2double = Double.parseDouble(fF2.getText().replaceAll(",","."));
            tensaoDoConcreto = Double.parseDouble(jFormattedTensaoDoCalculo.getText().replaceAll(",", "."));
            
            projeto.setTensaoDeConcreto(tensaoDoConcreto);
            
            if (fsglobal > 0.0) {
                FabricaDeComponentes.forneceInstanciaDeFabrica()
                        .getComponenteDeCapacidadeDeCarga()
                        .forneceEstrategiaDeCalculo().FS_GLOBAL = fsglobal;
            }

            if (fsatrito > 0.0) {
                FabricaDeComponentes.forneceInstanciaDeFabrica()
                        .getComponenteDeCapacidadeDeCarga()
                        .forneceEstrategiaDeCalculo().FS_LOCAL_ATRITO = fsatrito;
            }

            if (fsponta > 0.0) {
                FabricaDeComponentes.forneceInstanciaDeFabrica()
                        .getComponenteDeCapacidadeDeCarga()
                        .forneceEstrategiaDeCalculo().FS_LOCAL_PONTA = fsponta;
            }
            
            if(f1double > 0.0) {
                FabricaDeComponentes.forneceInstanciaDeFabrica()
                        .getComponenteDeCapacidadeDeCarga()
                        .forneceEstrategiaDeCalculo().F1 = f1double;
            }
            
            if(f2double > 0.0) {
                FabricaDeComponentes.forneceInstanciaDeFabrica()
                        .getComponenteDeCapacidadeDeCarga()
                        .forneceEstrategiaDeCalculo().F2 = f2double;
            }

            if (projeto.getFaixaDeCalculo()[0] < projeto.getFaixaDeCalculo()[1] && projeto.getFaixaDeCalculo()[0] != 0
                    && projeto.getPasso() != 0) {

                ArrayList<ResultadoFundacao> retorno = projeto.definirResultadosDeCapacidadeDeCarga();

                if (retorno == null || retorno.isEmpty()) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Ocorreu algum problema ao realizar o cálculo, verifique as etapas anteriores!", "Alerta ao Usuário");
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("O cálculo foi realizado com sucesso, você já pode gerar o relatório!", "Alerta ao Usuário");
                    configurarValoresEmTabela(retorno);
                    jPanelFatoresDeSeguranca.setVisible(false);
                    jPanelDemonstrativo.setVisible(true);

                    this.jFormattedDeProf.setEnabled(false);
                    this.jFormattedAteProfu.setEnabled(false);
                    this.jFormattedTextPasso.setEnabled(false);
                    this.btnOk4.setEnabled(false);
                    this.btnLiberaNovoCalculo.setEnabled(true);

                    this.framePrincipalCarga.notificarBotoesDeAvancar(true);
                    revalidate();
                }
            } else {
                GeradorDeMensagem.exibirMensagemDeInformacao("Realize a operação novamente:\n* no campo De os valores devem ser maiores que 0\n* o campo Ate deve ser maior que o De\n* quanto o campo passo deve ser menor que o Profundidade\n* o Profundidade deve ser maior que 0", "Alerta ao Usuário");
            }
        } catch (Exception erro) {
            GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um problema, por favor verifique os campos preenchidos ou realize a operação novamente!");
            ServicoDeLog.fornecerInstancia().registrarLogDeErro(getClass(), erro.getMessage());
            erro.printStackTrace();
        }
    }//GEN-LAST:event_btnOk4ActionPerformed

    private void btnDecourtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecourtActionPerformed
        this.jScrollPaneDecourt.setVisible(true);
        this.jScrollPaneAoki.setVisible(false);
        this.jPanelDemonstrativo.revalidate();
        revalidate();
        this.btnDecourt.setEnabled(false);
        this.btnAoki.setEnabled(true);
        this.jLabelMetodoEscolhido.setText("Método Escolhido: Decóurt-Quaresma");
    }//GEN-LAST:event_btnDecourtActionPerformed

    private void btnAokiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAokiActionPerformed
        this.jScrollPaneDecourt.setVisible(false);
        this.jScrollPaneAoki.setVisible(true);
        this.jPanelDemonstrativo.revalidate();
        revalidate();
        this.btnDecourt.setEnabled(true);
        this.btnAoki.setEnabled(false);
        this.jLabelMetodoEscolhido.setText("Método Escolhido: Aoki-Velloso");
    }//GEN-LAST:event_btnAokiActionPerformed

    private void btnLiberaNovoCalculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLiberaNovoCalculoActionPerformed
        this.jFormattedDeProf.setEnabled(true);
        this.jFormattedAteProfu.setEnabled(true);
        this.jFormattedTextPasso.setEnabled(true);
        this.btnOk4.setEnabled(true);
        this.btnLiberaNovoCalculo.setEnabled(false);
        this.jPanelDemonstrativo.setVisible(false);
    }//GEN-LAST:event_btnLiberaNovoCalculoActionPerformed

    private void jF1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jF1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jF1ActionPerformed

    private void fF2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fF2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fF2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAoki;
    private javax.swing.JButton btnDecourt;
    private javax.swing.JButton btnLiberaNovoCalculo;
    private javax.swing.JButton btnOk4;
    private javax.swing.JFormattedTextField fF2;
    private javax.swing.JFormattedTextField jF1;
    private javax.swing.JFormattedTextField jFormattedAteProfu;
    private javax.swing.JFormattedTextField jFormattedDeProf;
    private javax.swing.JFormattedTextField jFormattedTensaoDoCalculo;
    private javax.swing.JFormattedTextField jFormattedTextPasso;
    private javax.swing.JFormattedTextField jFsAtritoLateral;
    private javax.swing.JFormattedTextField jFsGlobal;
    private javax.swing.JFormattedTextField jFsResistenciaPonta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelMetodoEscolhido;
    private javax.swing.JPanel jPanelDemonstrativo;
    private javax.swing.JPanel jPanelFatoresDeSeguranca;
    private javax.swing.JScrollPane jScrollPaneAoki;
    private javax.swing.JScrollPane jScrollPaneDecourt;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTableAoki;
    private javax.swing.JTable jTableDecourt;
    private javax.swing.JTextField txtEstacaUtilizada;
    // End of variables declaration//GEN-END:variables

    
    public JFormattedTextField getjFormattedAteProfu() {
        return jFormattedAteProfu;
    }

    public void setjFormattedAteProfu(JFormattedTextField jFormattedAteProfu) {
        this.jFormattedAteProfu = jFormattedAteProfu;
    }

    public JFormattedTextField getjFormattedDeProf() {
        return jFormattedDeProf;
    }

    public void setjFormattedDeProf(JFormattedTextField jFormattedDeProf) {
        this.jFormattedDeProf = jFormattedDeProf;
    }

    public JFormattedTextField getjFormattedTensaoDoCalculo() {
        return jFormattedTensaoDoCalculo;
    }

    public void setjFormattedTensaoDoCalculo(JFormattedTextField jFormattedTensaoDoCalculo) {
        this.jFormattedTensaoDoCalculo = jFormattedTensaoDoCalculo;
    }

    public JFormattedTextField getjFormattedTextPasso() {
        return jFormattedTextPasso;
    }

    public void setjFormattedTextPasso(JFormattedTextField jFormattedTextPasso) {
        this.jFormattedTextPasso = jFormattedTextPasso;
    }

    public JFormattedTextField getjFsAtritoLateral() {
        return jFsAtritoLateral;
    }

    public void setjFsAtritoLateral(JFormattedTextField jFsAtritoLateral) {
        this.jFsAtritoLateral = jFsAtritoLateral;
    }

    public JFormattedTextField getjFsGlobal() {
        return jFsGlobal;
    }

    public void setjFsGlobal(JFormattedTextField jFsGlobal) {
        this.jFsGlobal = jFsGlobal;
    }

    public JFormattedTextField getjFsResistenciaPonta() {
        return jFsResistenciaPonta;
    }

    public void setjFsResistenciaPonta(JFormattedTextField jFsResistenciaPonta) {
        this.jFsResistenciaPonta = jFsResistenciaPonta;
    }

    public JLabel getjLabel10() {
        return jLabel10;
    }

    public void setjLabel10(JLabel jLabel10) {
        this.jLabel10 = jLabel10;
    }


    public JLabel getjLabel12() {
        return jLabel12;
    }

    public void setjLabel12(JLabel jLabel12) {
        this.jLabel12 = jLabel12;
    }

    public JLabel getjLabel13() {
        return jLabel13;
    }

    public void setjLabel13(JLabel jLabel13) {
        this.jLabel13 = jLabel13;
    }

    public JLabel getjLabel3() {
        return jLabel3;
    }

    public void setjLabel3(JLabel jLabel3) {
        this.jLabel3 = jLabel3;
    }

    public JLabel getjLabel4() {
        return jLabel4;
    }

    public void setjLabel4(JLabel jLabel4) {
        this.jLabel4 = jLabel4;
    }

    public JLabel getjLabel5() {
        return jLabel5;
    }

    public void setjLabel5(JLabel jLabel5) {
        this.jLabel5 = jLabel5;
    }

    public JLabel getjLabel8() {
        return jLabel8;
    }

    public void setjLabel8(JLabel jLabel8) {
        this.jLabel8 = jLabel8;
    }

    public JLabel getjLabel9() {
        return jLabel9;
    }

    public void setjLabel9(JLabel jLabel9) {
        this.jLabel9 = jLabel9;
    }

    public JButton getBtnOk4() {
        return btnOk4;
    }

}
