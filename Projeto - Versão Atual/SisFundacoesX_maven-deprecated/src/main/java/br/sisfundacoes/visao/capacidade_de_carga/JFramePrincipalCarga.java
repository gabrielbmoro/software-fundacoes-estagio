/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.visao.capacidade_de_carga;

import br.sisfundacoes.dagger.modulos.ModuloVerificaOS;
import br.sisfundacoes.modelo.CHAVE_UTILIZADA;
import br.sisfundacoes.dagger.modulos.IncubacaoDeDadosImportados;
import br.sisfundacoes.main.FabricaDeComponentes;
import br.sisfundacoes.main.ServicoDeLog;
import br.sisfundacoes.modelo.Fundacao;
import br.sisfundacoes.modelo.pojo.ItemDeEnsaio;
import br.sisfundacoes.modelo.Solo;
import br.sisfundacoes.modelo.Projeto;
import br.sisfundacoes.visao.ConfiguracaoFrame;
import br.sisfundacoes.visao.FrameInicial;
import br.sisfundacoes.visao.GeradorDeMensagem;
import br.sisfundacoes.visao.TrataWindowListener;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.inject.Inject;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 *
 * @author Gabriel B Moro
 */
public class JFramePrincipalCarga extends javax.swing.JFrame {

    private int de, ate;
    private PanelIdentificacao panelIdentificacao;
    private PanelProfundMax panelProfundidadeMax;
    private PanelTipoDeSolo panelTipoDeSolo;
    private PanelEscolhaEstaca panelEscolhaDeEstaca;
    private PanelFatoresDeSeguranca panelFatoresDeSeguranca;
    private PanelGerarDocumento panelGerarDocumento;
    public static int ITERADOR = -1;
    private ArrayList<JPanel> vectorJPanels;
    @Inject
    ModuloVerificaOS verificaOS;

    /**
     * Creates new form JFramePrincipalCarga
     */
    public JFramePrincipalCarga() {
        initComponents();

        FabricaDeComponentes.forneceInstanciaDeFabrica()
                .getComponenteVerificaOS()
                .inject(this);

        /*Inicializar JPanels*/
        panelIdentificacao = new PanelIdentificacao(this);
        panelProfundidadeMax = new PanelProfundMax(this);
        panelTipoDeSolo = new PanelTipoDeSolo(this);
        panelEscolhaDeEstaca = new PanelEscolhaEstaca(this);
        panelFatoresDeSeguranca = new PanelFatoresDeSeguranca(this);
        panelGerarDocumento = new PanelGerarDocumento();
        this.vectorJPanels = new ArrayList<>();

        ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(),
                "Inicializando o JPanels do Módulo de Carga...");

        /*adicionar panels*/
        jPanelInternoDinamico.setLayout(new FlowLayout(FlowLayout.CENTER));
        jPanelInternoDinamico.add(panelIdentificacao);
        jPanelInternoDinamico.add(panelProfundidadeMax);
        jPanelInternoDinamico.add(panelTipoDeSolo);
        jPanelInternoDinamico.add(panelEscolhaDeEstaca);
        jPanelInternoDinamico.add(panelFatoresDeSeguranca);
        jPanelInternoDinamico.add(panelGerarDocumento);

        /*configuração de estado inicial*/
        panelBoasVindas.setVisible(true);
        panelEscolhaDeEstaca.setVisible(false);
        panelFatoresDeSeguranca.setVisible(false);
        panelProfundidadeMax.setVisible(false);
        panelTipoDeSolo.setVisible(false);
        panelIdentificacao.setVisible(false);
        panelGerarDocumento.setVisible(false);
        jPanelInternoDinamico.revalidate();

        this.notificarBotoesDeAvancar(true);
        this.notificarBotoesDeVoltar(false);

        this.jMenuBar1.setVisible(true);

        this.reinterarPane();
        this.preencherVector();
        registrarListeners();
        ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 981, 512);
        addWindowListener(new TrataWindowListener());
    }

    private void reinterarPane() {
        if (verificaOS.isLinux()) {
            this.jPanelBtnSistema.setVisible(true);
        } else {
            this.jPanelBtnSistema.setVisible(false);
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por configurar os listeners respectivos da interface
     * gráfica.
     */
    public void registrarListeners() {
        jMenuItemInicio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Projeto.destruirEstancia();
                dispose();
                new FrameInicial();
            }
        });
        btnInicioBarraSistema.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Projeto.destruirEstancia();
                dispose();
                new FrameInicial();
            }
        });
        jMenuItemDesligar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean resposta = GeradorDeMensagem.exibirMensagemDeConfirmacao("Você realmente quer sair da aplicação?", "Alerta ao Usuário");
                if (resposta) {
                    System.exit(0);
                }
            }
        });
        btnAvancar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
            }
        });
        jMenuItemAvancar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
            }
        });
        jMenuItemVoltar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
            }
        });
        btnVoltar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
            }
        });
        jMenuItemImportarDados.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                criarImporteDeDados();
            }
        });
        btnImportarPLanilhaSistema.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                criarImporteDeDados();
            }
        });
        jMenuItemBaixarTemplate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new JFrameBaixarTemplate();
            }
        });
        btnBaixarTemplateDeImportacaoSistema.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new JFrameBaixarTemplate();
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelBtnSistema = new javax.swing.JPanel();
        btnInicioBarraSistema = new javax.swing.JButton();
        btnBaixarTemplateDeImportacaoSistema = new javax.swing.JButton();
        btnImportarPLanilhaSistema = new javax.swing.JButton();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnVoltar = new javax.swing.JButton();
        btnAvancar = new javax.swing.JButton();
        jPanelInternoDinamico = new javax.swing.JPanel();
        panelBoasVindas = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemInicio = new javax.swing.JMenuItem();
        jMenuItemBaixarTemplate = new javax.swing.JMenuItem();
        jMenuItemImportarDados = new javax.swing.JMenuItem();
        jMenuItemDesligar = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItemAvancar = new javax.swing.JMenuItem();
        jMenuItemVoltar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sisfundacoes - Capacidade de Carga");

        jPanelBtnSistema.setBorder(javax.swing.BorderFactory.createTitledBorder("Sistema"));

        btnInicioBarraSistema.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/casa.png"))); // NOI18N
        btnInicioBarraSistema.setText("Página Inicial");
        btnInicioBarraSistema.setToolTipText("Início");

        btnBaixarTemplateDeImportacaoSistema.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/baixarTemplate.png"))); // NOI18N
        btnBaixarTemplateDeImportacaoSistema.setText("Baixar Template");
        btnBaixarTemplateDeImportacaoSistema.setToolTipText("Baixar Template de Importação");

        btnImportarPLanilhaSistema.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/importar.png"))); // NOI18N
        btnImportarPLanilhaSistema.setText("Importar");
        btnImportarPLanilhaSistema.setToolTipText("Importar Planilha de Projeto");

        javax.swing.GroupLayout jPanelBtnSistemaLayout = new javax.swing.GroupLayout(jPanelBtnSistema);
        jPanelBtnSistema.setLayout(jPanelBtnSistemaLayout);
        jPanelBtnSistemaLayout.setHorizontalGroup(
            jPanelBtnSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBtnSistemaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnInicioBarraSistema, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBaixarTemplateDeImportacaoSistema, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImportarPLanilhaSistema, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanelBtnSistemaLayout.setVerticalGroup(
            jPanelBtnSistemaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnInicioBarraSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnBaixarTemplateDeImportacaoSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnImportarPLanilhaSistema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        btnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/esquerda.png"))); // NOI18N
        btnVoltar.setText("Recuar");
        btnVoltar.setToolTipText("Voltar para Painel anterior");
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });

        btnAvancar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/direita.png"))); // NOI18N
        btnAvancar.setText("Avançar");
        btnAvancar.setToolTipText("Avançar para Painel posterior");
        btnAvancar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvancarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("sansserif", 0, 24)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/logoSistemaPrograma.png"))); // NOI18N
        jLabel1.setText("Módulo Capacidade de Carga");

        javax.swing.GroupLayout panelBoasVindasLayout = new javax.swing.GroupLayout(panelBoasVindas);
        panelBoasVindas.setLayout(panelBoasVindasLayout);
        panelBoasVindasLayout.setHorizontalGroup(
            panelBoasVindasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBoasVindasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(23, Short.MAX_VALUE))
        );
        panelBoasVindasLayout.setVerticalGroup(
            panelBoasVindasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBoasVindasLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelInternoDinamicoLayout = new javax.swing.GroupLayout(jPanelInternoDinamico);
        jPanelInternoDinamico.setLayout(jPanelInternoDinamicoLayout);
        jPanelInternoDinamicoLayout.setHorizontalGroup(
            jPanelInternoDinamicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInternoDinamicoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBoasVindas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelInternoDinamicoLayout.setVerticalGroup(
            jPanelInternoDinamicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInternoDinamicoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelBoasVindas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jMenuBar1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        jMenu1.setText("Sistema");

        jMenuItemInicio.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        jMenuItemInicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/casa.png"))); // NOI18N
        jMenuItemInicio.setText("Início");
        jMenu1.add(jMenuItemInicio);

        jMenuItemBaixarTemplate.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemBaixarTemplate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/baixarTemplate.png"))); // NOI18N
        jMenuItemBaixarTemplate.setText("Baixar Template para Importação");
        jMenuItemBaixarTemplate.setToolTipText("Baixar Template para Importação");
        jMenuItemBaixarTemplate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemBaixarTemplateActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemBaixarTemplate);

        jMenuItemImportarDados.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemImportarDados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/importar.png"))); // NOI18N
        jMenuItemImportarDados.setText("Importar Dados de Projeto");
        jMenu1.add(jMenuItemImportarDados);

        jMenuItemDesligar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItemDesligar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/desligar.png"))); // NOI18N
        jMenuItemDesligar.setText("Desligar");
        jMenu1.add(jMenuItemDesligar);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ações");

        jMenuItemAvancar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_RIGHT, 0));
        jMenuItemAvancar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/direita.png"))); // NOI18N
        jMenuItemAvancar.setText("Avançar");
        jMenu2.add(jMenuItemAvancar);

        jMenuItemVoltar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_LEFT, 0));
        jMenuItemVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/sisfundacoes/visao/icones/esquerda.png"))); // NOI18N
        jMenuItemVoltar.setText("Voltar");
        jMenuItemVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemVoltarActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItemVoltar);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelBtnSistema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAvancar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 508, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelInternoDinamico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelBtnSistema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelInternoDinamico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAvancar)
                    .addComponent(btnVoltar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemBaixarTemplateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemBaixarTemplateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItemBaixarTemplateActionPerformed

    private void jMenuItemVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemVoltarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItemVoltarActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnVoltarActionPerformed

    private void btnAvancarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvancarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAvancarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAvancar;
    private javax.swing.JButton btnBaixarTemplateDeImportacaoSistema;
    private javax.swing.JButton btnImportarPLanilhaSistema;
    private javax.swing.JButton btnInicioBarraSistema;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItemAvancar;
    private javax.swing.JMenuItem jMenuItemBaixarTemplate;
    private javax.swing.JMenuItem jMenuItemDesligar;
    private javax.swing.JMenuItem jMenuItemImportarDados;
    private javax.swing.JMenuItem jMenuItemInicio;
    private javax.swing.JMenuItem jMenuItemVoltar;
    private javax.swing.JPanel jPanelBtnSistema;
    private javax.swing.JPanel jPanelInternoDinamico;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JPanel panelBoasVindas;
    // End of variables declaration//GEN-END:variables

    private void criarImporteDeDados() {
        new JFrameImportarDados(this);
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por configurar os listeners para um determinado
     * JPanel, diferentemente do método anterior essa sobrecarga foi utilizada
     * para aplicar a mesma funcionalidade para outra situação.
     *
     * @param jPanel de tipo {@link JPanel}
     */
    public void registrarPossiveisParametros(JPanel jPanel) {
        if (jPanel instanceof PanelTipoDeSolo) {
            Projeto projeto = Projeto.obterInstancia();
            PanelTipoDeSolo panel = (PanelTipoDeSolo) jPanel;
            int deT
                    = projeto.getFundacao().getEnsaios_profPorNSPT().get(0).getProfundidade();
            int ateT
                    = projeto.getFundacao().getEnsaios_profPorNSPT().get(
                            projeto.getFundacao().getEnsaios_profPorNSPT().size() - 1).getProfundidade();
            this.de = deT;
            this.ate = ateT;
        }

    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por alimentar o vetor utilizado para armazenar os
     * JPanels que o JFrame principal conterá, por usar navegabilidade, tal
     * técnica é necessária.
     */
    private void preencherVector() {
        this.vectorJPanels.add(panelIdentificacao);
        this.vectorJPanels.add(panelProfundidadeMax);
        this.vectorJPanels.add(panelTipoDeSolo);
        this.vectorJPanels.add(panelEscolhaDeEstaca);
        this.vectorJPanels.add(panelFatoresDeSeguranca);
        this.vectorJPanels.add(panelGerarDocumento);
    }

    public void exibirJPanelSelecionado(Object objeto) {
        panelBoasVindas.setVisible(false);
        if (objeto instanceof PanelIdentificacao) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 581, 400);
            panelEscolhaDeEstaca.setVisible(false);
            panelFatoresDeSeguranca.setVisible(false);
            panelProfundidadeMax.setVisible(false);
            panelTipoDeSolo.setVisible(false);
            panelIdentificacao.setVisible(true);
            panelGerarDocumento.setVisible(false);
            btnImportarPLanilhaSistema.setEnabled(false);
        } else if (objeto instanceof PanelProfundMax) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 581, 500);
            panelEscolhaDeEstaca.setVisible(false);
            panelFatoresDeSeguranca.setVisible(false);
            panelProfundidadeMax.setVisible(true);
            panelProfundidadeMax.getjTableProfNspt().revalidate();
            panelTipoDeSolo.setVisible(false);
            panelIdentificacao.setVisible(false);
            panelGerarDocumento.setVisible(false);
        } else if (objeto instanceof PanelTipoDeSolo) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 781, 650);
            panelEscolhaDeEstaca.setVisible(false);
            panelFatoresDeSeguranca.setVisible(false);
            panelProfundidadeMax.setVisible(false);
            panelTipoDeSolo.setVisible(true);
            panelTipoDeSolo.getjTableSolosPorProfundidade().revalidate();
            panelIdentificacao.setVisible(false);
            panelGerarDocumento.setVisible(false);
        } else if (objeto instanceof PanelEscolhaEstaca) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 781, 550);
            panelEscolhaDeEstaca.setVisible(true);
            panelFatoresDeSeguranca.setVisible(false);
            panelProfundidadeMax.setVisible(false);
            panelTipoDeSolo.setVisible(false);
            panelIdentificacao.setVisible(false);
            panelGerarDocumento.setVisible(false);
        } else if (objeto instanceof PanelFatoresDeSeguranca) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 970, 700);
            panelEscolhaDeEstaca.setVisible(false);
            panelFatoresDeSeguranca.setVisible(true);
            panelProfundidadeMax.setVisible(false);
            panelTipoDeSolo.setVisible(false);
            panelIdentificacao.setVisible(false);
            panelGerarDocumento.setVisible(false);
            
            panelFatoresDeSeguranca.configurarValorDeTensao(
            Projeto.obterInstancia()
                    .getFundacao()
                    .getEstacas()
                    .get(0).getTipo()
            );
            
        } else if (objeto instanceof PanelGerarDocumento) {
            ConfiguracaoFrame.configFrameComTamanhoPersonalizado(this, 581, 400);
            panelEscolhaDeEstaca.setVisible(false);
            panelFatoresDeSeguranca.setVisible(false);
            panelProfundidadeMax.setVisible(false);
            panelTipoDeSolo.setVisible(false);
            panelIdentificacao.setVisible(false);
            panelGerarDocumento.setVisible(true);
            this.btnVoltar.setEnabled(false);
            this.btnAvancar.setEnabled(false);
            this.jMenuItemAvancar.setEnabled(false);
            this.jMenuItemVoltar.setEnabled(false);
        }
        jPanelInternoDinamico.repaint();
        jPanelInternoDinamico.revalidate();
        revalidate();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção voltar.
     */
    private void registrarListenerBotaoVoltar() {
        if (ITERADOR > 0) {
            ITERADOR -= 1;
            registrarPossiveisParametros(vectorJPanels.get(ITERADOR));
            notificarBotoesDeVoltar(true);
            exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(), "Voltado: " + ITERADOR);
            jProgressBar1.setValue(jProgressBar1.getValue() - 14);
        } else {
            notificarBotoesDeVoltar(false);
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção avançar.
     */
    private void registrarListenerBotaoAvancar() {
        jMenuItemImportarDados.setEnabled(false);
        if (ITERADOR <= vectorJPanels.size()) {
            ITERADOR += 1;
            registrarPossiveisParametros(vectorJPanels.get(ITERADOR));
            notificarBotoesDeAvancar(true);
            exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            System.err.println("Avançado: " + ITERADOR);
            jProgressBar1.setValue(jProgressBar1.getValue() + 14);
            btnVoltar.setEnabled(true);
        }

        btnAvancar.setEnabled(false);
        notificarBotoesDeAvancar(false);
    }

    public void distribuirParametrosImportados() {
        /*PainelIdentificacao*/
        HashMap<CHAVE_UTILIZADA, Object> mapaDeParametros = IncubacaoDeDadosImportados.obterInstancia().getParametros();
        if (mapaDeParametros != null && !mapaDeParametros.isEmpty()) {
            String perfil = (String) mapaDeParametros.get(CHAVE_UTILIZADA.PERFIL);
            Projeto.obterInstancia().setPerfil(perfil);
            Date data = (Date) mapaDeParametros.get(CHAVE_UTILIZADA.DATA);
            Projeto.obterInstancia().setData(data);
            String ref = (String) mapaDeParametros.get(CHAVE_UTILIZADA.REFERENCIA);
            Projeto.obterInstancia().setReferencia(ref);
            panelIdentificacao.restaurarDadosDeInterface();

            /*Painel Profundidade*/
            ArrayList<ItemDeEnsaio> itensDeEnsaio = (ArrayList<ItemDeEnsaio>) mapaDeParametros.get(CHAVE_UTILIZADA.PROFUNDIDADE_NSPT);
            Projeto.obterInstancia().setFundacao(new Fundacao());
            Projeto.obterInstancia().getFundacao().setEnsaios_profPorNSPT(itensDeEnsaio);
            panelProfundidadeMax.restaurarDadosDeInterface();

            /*Painel Solo*/
            ArrayList<Solo> solosPorProf = (ArrayList<Solo>) mapaDeParametros.get(CHAVE_UTILIZADA.SOLOS);
            Projeto.obterInstancia().getFundacao().setSolos(solosPorProf);
            panelTipoDeSolo.restaurarDadosDeInterface();

            /*Painel Estaca*/
            int alternativa = (int) mapaDeParametros.get(CHAVE_UTILIZADA.ALTERNATIVA);
            Projeto.obterInstancia().setAlternativa(alternativa);
        }
    }

    public void notificarBotoesDeVoltar(boolean l) {
        btnVoltar.setFocusable(l);
        btnVoltar.setEnabled(l);
        jMenuItemVoltar.setEnabled(l);
    }

    public void notificarBotoesDeAvancar(boolean l) {
        btnAvancar.setFocusable(l);
        btnAvancar.setEnabled(l);
        jMenuItemAvancar.setEnabled(l);
    }

    public JButton getBtnBaixarTemplateDeImportacaoSistema() {
        return btnBaixarTemplateDeImportacaoSistema;
    }

    public void setBtnBaixarTemplateDeImportacaoSistema(JButton btnBaixarTemplateDeImportacaoSistema) {
        this.btnBaixarTemplateDeImportacaoSistema = btnBaixarTemplateDeImportacaoSistema;
    }

    public JButton getBtnImportarPLanilhaSistema() {
        return btnImportarPLanilhaSistema;
    }

    public void setBtnImportarPLanilhaSistema(JButton btnImportarPLanilhaSistema) {
        this.btnImportarPLanilhaSistema = btnImportarPLanilhaSistema;
    }

    public JButton getBtnInicioBarraSistema() {
        return btnInicioBarraSistema;
    }

    public void setBtnInicioBarraSistema(JButton btnInicioBarraSistema) {
        this.btnInicioBarraSistema = btnInicioBarraSistema;
    }

    public JLabel getjLabel1() {
        return jLabel1;
    }

    public void setjLabel1(JLabel jLabel1) {
        this.jLabel1 = jLabel1;
    }

    public JMenu getjMenu1() {
        return jMenu1;
    }

    public void setjMenu1(JMenu jMenu1) {
        this.jMenu1 = jMenu1;
    }

    public JMenu getjMenu2() {
        return jMenu2;
    }

    public void setjMenu2(JMenu jMenu2) {
        this.jMenu2 = jMenu2;
    }

    public JMenuBar getjMenuBar1() {
        return jMenuBar1;
    }

    public void setjMenuBar1(JMenuBar jMenuBar1) {
        this.jMenuBar1 = jMenuBar1;
    }

    public void setjMenuItemBaixarTemplate(JMenuItem jMenuItemBaixarTemplate) {
        this.jMenuItemBaixarTemplate = jMenuItemBaixarTemplate;
    }

    public JMenuItem getjMenuItemDesligar() {
        return jMenuItemDesligar;
    }

    public void setjMenuItemDesligar(JMenuItem jMenuItemDesligar) {
        this.jMenuItemDesligar = jMenuItemDesligar;
    }

    public JMenuItem getjMenuItemImportarDados() {
        return jMenuItemImportarDados;
    }

    public void setjMenuItemImportarDados(JMenuItem jMenuItemImportarDados) {
        this.jMenuItemImportarDados = jMenuItemImportarDados;
    }

    public JMenuItem getjMenuItemInicio() {
        return jMenuItemInicio;
    }

    public void setjMenuItemInicio(JMenuItem jMenuItemInicio) {
        this.jMenuItemInicio = jMenuItemInicio;
    }

    public JPanel getjPanelBtnSistema() {
        return jPanelBtnSistema;
    }

    public void setjPanelBtnSistema(JPanel jPanelBtnSistema) {
        this.jPanelBtnSistema = jPanelBtnSistema;
    }

    public JPanel getjPanelInternoDinamico() {
        return jPanelInternoDinamico;
    }

    public void setjPanelInternoDinamico(JPanel jPanelInternoDinamico) {
        this.jPanelInternoDinamico = jPanelInternoDinamico;
    }

    public JProgressBar getjProgressBar1() {
        return jProgressBar1;
    }

    public void setjProgressBar1(JProgressBar jProgressBar1) {
        this.jProgressBar1 = jProgressBar1;
    }

    public JPanel getPanelBoasVindas() {
        return panelBoasVindas;
    }

    public void setPanelBoasVindas(JPanel panelBoasVindas) {
        this.panelBoasVindas = panelBoasVindas;
    }

    public int getDe() {
        return de;
    }

    public void setDe(int de) {
        this.de = de;
    }

    public int getAte() {
        return ate;
    }

    public void setAte(int ate) {
        this.ate = ate;
    }

}
