/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.modelo;

import br.sisfundacoes.modelo.pojo.ItemDeEnsaio;
import br.sisfundacoes.modelo.pojo.Estaca;
import java.util.ArrayList;

/**
 *
 * @author Gabriel B Moro
 */
public class Fundacao {

    private ArrayList<Estaca> estacas;
    private ArrayList<Solo> solos;
    private ArrayList<ItemDeEnsaio> ensaios_profPorNSPT;

    public Fundacao() {
        this.estacas = new ArrayList<>();
        this.solos = new ArrayList<>();
        this.ensaios_profPorNSPT = new ArrayList<ItemDeEnsaio>();
    }

    public boolean definirDadosTipoDeSolo(ArrayList<Object> dados) {
        Projeto projeto = Projeto.obterInstancia();
        Fundacao fundacao = projeto.getFundacao();
        int profundMax = fundacao.getEnsaios_profPorNSPT().size();
        for (int count = 0; count < dados.size(); count++) {
            if (count != (dados.size() - 1)) {
                if (dados.get(count) instanceof Double && dados.get(count + 1) instanceof Double) {
                    if (((Double) dados.get(count) < (Double) dados.get(count + 1)) && (Double) dados.get(count) <= profundMax) {
                        continue;
                    } else {
                        return false;
                    }
                } else {
                    continue;
                }
            }
        }

        this.solos = refatorarVectorProfundTipoSolo(dados);
        fundacao.setSolos(this.solos);
        
        return true;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por converter uma lista de dados de tipo
     * {@link ArrayList} que é composta por tipo {@link Object}, para um lista
     * ({@link ArrayList} de objetos de tipo {@link Solo}
     *
     * @param temp de tipo {@link ArrayList}
     * @return de tipo {@link ArrayList}
     */
    private ArrayList<Solo> refatorarVectorProfundTipoSolo(ArrayList<Object> temp) {
        ArrayList<Solo> solos1 = new ArrayList<>();
        for (int count = 0; count < temp.size(); count++) {
            String tipo = (String) temp.get(count);
            TipoSolo tipoTemp = null;
            double ateTemp = 0;
            double deTemp = 0;

            switch (tipo) {
                case "Areia":
                    tipoTemp = TipoSolo.AREIA;
                    break;
                case "Areia Siltosa":
                    tipoTemp = TipoSolo.AREIA_SITOSA;
                    break;
                case "Areia Silto Argilosa":
                    tipoTemp = TipoSolo.AREIA_SILTO_ARGILOSA;
                    break;
                case "Areia Argilosa":
                    tipoTemp = TipoSolo.AREIA_ARGILOSA;
                    break;
                case "Areia Argilosa Siltosa":
                    tipoTemp = TipoSolo.AREIA_ARGILO_SILTOSA;
                    break;
                case "Silte":
                    tipoTemp = TipoSolo.SILTE;
                    break;
                case "Silte Arenoso":
                    tipoTemp = TipoSolo.SILTE_ARENOSO;
                    break;
                case "Silte Areno Argiloso":
                    tipoTemp = TipoSolo.SILTE_ARENOSO_ARGILOSO;
                    break;
                case "Silte Argiloso":
                    tipoTemp = TipoSolo.SILTE_ARGILOSO;
                    break;
                case "Silte Argiloso Arenoso":
                    tipoTemp = TipoSolo.SILTE_ARGILOSO_ARENOSO;
                    break;
                case "Argila":
                    tipoTemp = TipoSolo.ARGILA;
                    break;
                case "Argila Arenosa":
                    tipoTemp = TipoSolo.ARGILA_ARENOSA;
                    break;
                case "Argila Areno Siltosa":
                    tipoTemp = TipoSolo.ARGILA_ARENO_SILTOSA;
                    break;
                case "Argila Siltosa":
                    tipoTemp = TipoSolo.ARGILA_SILTOSA;
                    break;
                case "Argila Silto Arenosa":
                    tipoTemp = TipoSolo.ARGILA_SILO_ARENOSA;
                    break;
            }
            ateTemp = (double) temp.get(count + 2);
            deTemp = (double) temp.get(count + 1);

            solos1.add(new Solo(tipoTemp, deTemp, ateTemp));
            count += 2;
        }
        return solos1;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar as estacas com dimensões em seus
     * respectivos blocos (de tipo {@link Bloco}).
     *
     * @param estaca que será cadastrada de tipo {@link Estaca}
     * @return de tipo {@link Boolean}
     */
    public boolean registrarDimensoes(ArrayList<Integer> dimensoesDisponiveis, TipoEstaca tipoDeEstaca) {
        Projeto projeto = Projeto.obterInstancia();

        for (int count = 0; count < dimensoesDisponiveis.size(); count++) {
            int dimensaoDefinida = dimensoesDisponiveis.get(count);
            estacas.add(new Estaca(dimensaoDefinida, tipoDeEstaca));
        }
        if (!estacas.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean definirItensDeEnsaio(ArrayList<Integer> nspts) {
        for (int contador = 0; contador < nspts.size(); contador++) {
            ensaios_profPorNSPT.add(new ItemDeEnsaio(contador + 1, nspts.get(contador)));
        }
        if (ensaios_profPorNSPT.size() == nspts.size()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Esse método deve ser utilizado para recuperar o solo válido de acordo com
     * a profundidade, pois essa ferramenta realiza várias operações variando os
     * níveis de profundidade disponíveis no {@link Terreno}.
     *
     * @param profundidade do solo
     * @param solos que devem ser entendidos como as camadas de solo que compõe
     * o {@link Terreno}
     * @return resultado de tipo {@link Solo}
     */
    public Solo recuperaSoloValido(int profundidade) {
        int countPasso = 0;
        double deTempSolo = 0.0, ateTempSolo = 0.0;

        for (Solo soloTemporario : solos) {
            deTempSolo = soloTemporario.getDe();
            ateTempSolo = soloTemporario.getAte();
            if (profundidade >= deTempSolo
                    && profundidade <= ateTempSolo) {
                return soloTemporario;
            }
        }
        return null;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por calcular o nspt médio utilizando os ensaios gerais
     * e um ensaio específico.
     *
     * @param itensEnsaio de tipo {@link ArrayList}
     * @param itemEnsaio de tipo {@link ItemDeEnsaio}
     * @return resultado de tipo {@link Double}
     *
     */
    public double calcularNsptMedio(ArrayList<ItemDeEnsaio> itensEnsaio, ItemDeEnsaio itemEnsaio) {
        double nsptTotal = 0.0;
        int contadorAnterior = 0, contadorPareado = 1, contadorPosterior = 2;
        for (int count = 0; count < itensEnsaio.size(); count++) {
            if (itensEnsaio.get(count).getNSPT() == itemEnsaio.getNSPT()) {
                if (count == 1) {
                    contadorAnterior = 0;
                    contadorPareado = 1;
                    contadorPosterior = 2;
                }
                if (count != 0 && count != 1) {
                    if ((count + 1) < itensEnsaio.size()) {
                        contadorAnterior = count - 1;
                        contadorPareado = count;
                        contadorPosterior = count + 1;
                    } else {
                        contadorAnterior = count - 2;
                        contadorPareado = count - 1;
                        contadorPosterior = count;
                    }
                }
            }
        }
        int numerador = itensEnsaio.get(contadorAnterior).getNSPT() + itensEnsaio.get(contadorPareado).getNSPT() + itensEnsaio.get(contadorPosterior).getNSPT();
        nsptTotal = numerador / 3.0;

        return nsptTotal;
    }

    /**
     * @return the estacas
     */
    public ArrayList<Estaca> getEstacas() {
        return estacas;
    }

    /**
     * @return the solos
     */
    public ArrayList<Solo> getSolos() {
        return solos;
    }

    public void setSolos(ArrayList<Solo> novoArrayDeSolos) {
        this.solos = novoArrayDeSolos;
    }

    /**
     * @return the ensaios_profPorNSPT
     */
    public ArrayList<ItemDeEnsaio> getEnsaios_profPorNSPT() {
        return ensaios_profPorNSPT;
    }

    public void setEnsaios_profPorNSPT(ArrayList<ItemDeEnsaio> ensaios) {
        this.ensaios_profPorNSPT = ensaios;
    }
}
