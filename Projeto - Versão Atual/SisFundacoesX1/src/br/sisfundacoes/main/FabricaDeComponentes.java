/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.main;

import br.sisfundacoes.dagger.componentes.ComponenteDeCapacidadeDeCarga;
import br.sisfundacoes.dagger.componentes.ComponenteDeImportacao;
import br.sisfundacoes.dagger.componentes.ComponenteGeraRelatorios;
import br.sisfundacoes.dagger.componentes.ComponenteVerificaOS;
import br.sisfundacoes.dagger.componentes.DaggerComponenteDeCapacidadeDeCarga;
import br.sisfundacoes.dagger.componentes.DaggerComponenteDeImportacao;
import br.sisfundacoes.dagger.componentes.DaggerComponenteGeraRelatorios;
import br.sisfundacoes.dagger.componentes.DaggerComponenteVerificaOS;
import br.sisfundacoes.dagger.modulos.ModuloEstrategiaCalculoDeCarga;
import br.sisfundacoes.dagger.modulos.ModuloGeraExcel;
import br.sisfundacoes.dagger.modulos.ModuloGeraPdf;
import br.sisfundacoes.dagger.modulos.ModuloGeraTemplate;
import br.sisfundacoes.dagger.modulos.ModuloImportarArquivoExcel;
import br.sisfundacoes.dagger.modulos.ModuloVerificaOS;

/**
 *
 * @author Gabriel B Moro
 */
public class FabricaDeComponentes {

    private ComponenteDeCapacidadeDeCarga componenteDeCapacidadeDeCarga;
    private ComponenteGeraRelatorios componenteDeGeracaoDeRelatorios;
    private ComponenteDeImportacao componenteDeImportacao;
    private ComponenteVerificaOS componenteVerificaOS;

    private static FabricaDeComponentes myInstance;

    private FabricaDeComponentes() {
        componenteDeCapacidadeDeCarga = DaggerComponenteDeCapacidadeDeCarga.builder()
                .moduloEstrategiaCalculoDeCarga(new ModuloEstrategiaCalculoDeCarga())
                .build();

        componenteDeGeracaoDeRelatorios = DaggerComponenteGeraRelatorios.builder()
                .moduloGeraExcel(new ModuloGeraExcel())
                .moduloGeraPdf(new ModuloGeraPdf())
                .moduloGeraTemplate(new ModuloGeraTemplate())
                .build();

        componenteDeImportacao = DaggerComponenteDeImportacao.builder()
                .moduloImportarArquivoExcel(new ModuloImportarArquivoExcel())
                .build();

        componenteVerificaOS = DaggerComponenteVerificaOS.builder()
                .moduloVerificaOS(new ModuloVerificaOS())
                .build();
    }

    public static FabricaDeComponentes forneceInstanciaDeFabrica() {
        if (myInstance == null) {
            myInstance = new FabricaDeComponentes();
        }
        return myInstance;
    }

    public ComponenteDeCapacidadeDeCarga getComponenteDeCapacidadeDeCarga() {
        return componenteDeCapacidadeDeCarga;
    }

    public ComponenteGeraRelatorios getComponenteDeGeracaoDeRelatorios() {
        return componenteDeGeracaoDeRelatorios;
    }

    public ComponenteDeImportacao getComponenteDeImportacao() {
        return componenteDeImportacao;
    }

    public ComponenteVerificaOS getComponenteVerificaOS() {
        return componenteVerificaOS;
    }

}
