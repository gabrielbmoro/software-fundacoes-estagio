/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.modelo;

import br.sisfundacoes.dagger.modulos.ModuloEstrategiaCalculoDeCarga;
import br.sisfundacoes.main.FabricaDeComponentes;
import br.sisfundacoes.modelo.pojo.ResultadoFundacao;
import br.sisfundacoes.modelo.pojo.ItemDeEnsaio;
import br.sisfundacoes.modelo.pojo.Estaca;
import br.sisfundacoes.modelo.pojo.Cliente;
import br.sisfundacoes.dagger.modulos.Utilitaria;
import java.util.ArrayList;
import java.util.Date;
import javax.inject.Inject;

/**
 *
 * @author Gabriel B Moro
 */
public class Projeto {

    private int[] faixaDeCalculo;
    private int passo;
    private int alternativa;
    private Cliente cliente;
    private Fundacao fundacao;
    private static Projeto instancia;
    private String referencia;
    private String perfil;
    private Date data;
    private double tensaoDeConcreto;
    private ArrayList<ResultadoFundacao> linkedResultados = new ArrayList<>();
    @Inject
    ModuloEstrategiaCalculoDeCarga estrategiaCalculoDeCarga;
    
    private Projeto() {
        FabricaDeComponentes.forneceInstanciaDeFabrica()
                .getComponenteDeCapacidadeDeCarga()
                .inject(this);
    }

    public static Projeto obterInstancia() {
        if (instancia == null) {
            instancia = new Projeto();
        }
        return instancia;
    }

    public ArrayList<ResultadoFundacao> definirResultadosDeCapacidadeDeCarga() {
        int passo = 1;
        Projeto projeto = Projeto.obterInstancia();
        Fundacao fundacao = projeto.getFundacao();
        ArrayList<Solo> solos = fundacao.getSolos();
        ArrayList<Estaca> estacas = fundacao.getEstacas();

        ArrayList<ItemDeEnsaio> ensaios = fundacao.getEnsaios_profPorNSPT();

        ArrayList<ItemDeEnsaio> ensaiosUtilizados = ensaiosUtilizados(
                faixaDeCalculo[0], faixaDeCalculo[1], ensaios);

        int nsptPonta = 0;
        Solo soloValido = null;
        double ab = 0.0;
        int resultadoQp_aoki = 0, resultadoQp_decourt = 0, resultadoQl_aoki = 0,
                resultadoQl_decourt = 0, resultadoQt_aoki = 0, resultadoQt_decourt = 0;

        for (ItemDeEnsaio itemDeEnsaio : ensaiosUtilizados) {
            System.err.printf("\n=============Profundidade: %d, Nspt: %d======\n", itemDeEnsaio.getProfundidade(), itemDeEnsaio.getNSPT());

            for (Estaca estacaTemp : estacas) {
                ResultadoFundacao resultadoFundacaoProfunda = new ResultadoFundacao();
                resultadoFundacaoProfunda.setItemDeEnsaio(new ItemDeEnsaio(itemDeEnsaio.getProfundidade(), itemDeEnsaio.getNSPT()));
                System.err.printf("-- Estaca de Dimensão: %d, Tipo de Estaca: %s ---", estacaTemp.getDimensao(), String.valueOf(estacaTemp.getTipo()));
                soloValido = fundacao.recuperaSoloValido(
                        itemDeEnsaio.getProfundidade());
                ab = Utilitaria.calcularAreaDaBase(estacaTemp.getDimensao(), estacaTemp.getTipo());
                nsptPonta = itemDeEnsaio.getNSPT();

                double raio = Utilitaria.calcularRaio(estacaTemp.getDimensao());
                double perimetro = Utilitaria.calcularPerimetro(raio);
                resultadoFundacaoProfunda.setPerimetro(perimetro);
                double nsptMedio = fundacao.calcularNsptMedio(ensaios, itemDeEnsaio);
                double nsptPor3 = nsptPonta / 3.0;
                
                soloValido.configurarValoresPorEstaca(estacaTemp);
                
                estrategiaCalculoDeCarga.setNsptDaCamada(itemDeEnsaio.getNSPT());
                estrategiaCalculoDeCarga.setAreaDaBase(ab);
                estrategiaCalculoDeCarga.setPerimetro(perimetro);
                estrategiaCalculoDeCarga.setNsptMedio(nsptMedio);
                estrategiaCalculoDeCarga.setNsptPor3(nsptPor3);
                estrategiaCalculoDeCarga.setProfundidade(itemDeEnsaio.getProfundidade());
                
                /*CALCULANDO MÉTODO DE AOKI*/
                resultadoQp_aoki = estrategiaCalculoDeCarga.calcularResistenciaDePonta(estacaTemp, soloValido, TipoDeCalculo.AOKI_VELLOSO);
                resultadoQl_aoki = resultadoQl_aoki + estrategiaCalculoDeCarga.calcularResistenciaLateral(estacaTemp, soloValido,TipoDeCalculo.AOKI_VELLOSO);
                resultadoQt_aoki = estrategiaCalculoDeCarga.calcularResistenciaTotal(resultadoQp_aoki, resultadoQl_aoki);

                /*CALCULANDO MÉTODO DE DECOURT*/
                resultadoQp_decourt = estrategiaCalculoDeCarga.calcularResistenciaDePonta(estacaTemp, soloValido, TipoDeCalculo.DECOURT_QUARESMA);
                resultadoQl_decourt = resultadoQl_decourt + estrategiaCalculoDeCarga.calcularResistenciaLateral(estacaTemp, soloValido, TipoDeCalculo.DECOURT_QUARESMA);
                resultadoQt_decourt = estrategiaCalculoDeCarga.calcularResistenciaTotal(resultadoQp_decourt, resultadoQl_decourt);
                
                /*Definindo fatores globais*/
                int qp_aoki_fs = resultadoQp_aoki / 4;
                int ql_aoki_fs = (resultadoQl_aoki / 2);
                int qt_aoki_fs = qp_aoki_fs + ql_aoki_fs;
                int qp_decourt_fs = (resultadoQp_decourt / 4);
                int ql_decourt_fs = (resultadoQl_decourt / 2);
                int qt_decourt_fs = qp_decourt_fs + ql_decourt_fs;
                int resistencia_concreto = estrategiaCalculoDeCarga.calcularResistenciaDoContreto(tensaoDeConcreto);
                
                /*Adicionando resultados em uma estrutura de dados a fim de mapeamento*/
                resultadoFundacaoProfunda.setDeltaL(ModuloEstrategiaCalculoDeCarga.DELTA_L);
                resultadoFundacaoProfunda.setF1(ModuloEstrategiaCalculoDeCarga.F1);
                resultadoFundacaoProfunda.setF2(ModuloEstrategiaCalculoDeCarga.F2);
                resultadoFundacaoProfunda.setNsptPor3(nsptPor3);
                resultadoFundacaoProfunda.setAreaDaBase(ab);
                resultadoFundacaoProfunda.setNsptMedio(nsptMedio);
                resultadoFundacaoProfunda.setQp_aoki_reajustado(qp_aoki_fs);
                resultadoFundacaoProfunda.setQl_aoki(resultadoQl_aoki);
                resultadoFundacaoProfunda.setQl_aoki_reajustado(ql_aoki_fs);
                resultadoFundacaoProfunda.setQt_aoki(resultadoQt_aoki);
                resultadoFundacaoProfunda.setQt_aoki_FS(qt_aoki_fs);
                resultadoFundacaoProfunda.setQp_decourt(resultadoQp_decourt);
                resultadoFundacaoProfunda.setQl_decourt(resultadoQl_decourt);
                resultadoFundacaoProfunda.setQp_decourt_reajustado(qp_decourt_fs);
                resultadoFundacaoProfunda.setQl_decourt_reajustado(ql_decourt_fs);
                resultadoFundacaoProfunda.setQt_decourt(resultadoQt_decourt);
                resultadoFundacaoProfunda.setQt_decourt_FS(qt_decourt_fs);
                resultadoFundacaoProfunda.setSolo(soloValido);
                resultadoFundacaoProfunda.setQp_aoki(resultadoQp_aoki);
                resultadoFundacaoProfunda.setItemDeEnsaio(itemDeEnsaio);
                resultadoFundacaoProfunda.setEstaca(estacaTemp);
                resultadoFundacaoProfunda.setResistenciaConcreto(resistencia_concreto);
                
                linkedResultados.add(resultadoFundacaoProfunda);
            }
        }
        return linkedResultados;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por recuperar os ensaios utilizados de acordo com a
     * profundidade desejada.
     *
     * @param de de tipo {@link Double} que é a profundidade inicial
     * @param ate de tipo {@link Double} que é a profundidade final
     * @param ensaios de tipo {@link ArrayList} que é todos os ensaios
     * utilizados.
     * @return
     */
    public ArrayList<ItemDeEnsaio> ensaiosUtilizados(int de, int ate, ArrayList<ItemDeEnsaio> ensaios) {
        int countPasso = 0;
        ArrayList<ItemDeEnsaio> ensaiosUtilizados = new ArrayList<ItemDeEnsaio>();
        if (ate > ensaios.get(ensaios.size() - 1).getProfundidade()) {
            ate = ensaios.get(ensaios.size() - 1).getProfundidade();
        }
        for (int count = 0; count < ensaios.size(); count++) {
            if (ensaios.get(count).getProfundidade() >= de && ensaios.get(count).getProfundidade() <= ate) {
                ensaiosUtilizados.add(new ItemDeEnsaio(ensaios.get(count).getProfundidade(),
                        ensaios.get(count).getNSPT()));
            } else {
                continue;
            }
        }
        return ensaiosUtilizados;
    }

    public static void destruirEstancia() {
        instancia = null;
    }

    /**
     * @return the faixaDeCalculo
     */
    public int[] getFaixaDeCalculo() {
        return faixaDeCalculo;
    }

    /**
     * @param faixaDeCalculo the faixaDeCalculo to set
     */
    public void setFaixaDeCalculo(int[] faixaDeCalculo) {
        this.faixaDeCalculo = faixaDeCalculo;
    }

    /**
     * @return the passo
     */
    public int getPasso() {
        return passo;
    }

    /**
     * @param passo the passo to set
     */
    public void setPasso(int passo) {
        this.passo = passo;
    }

    /**
     * @return the alternativa
     */
    public int getAlternativa() {
        return alternativa;
    }

    /**
     * @param alternativa the alternativa to set
     */
    public void setAlternativa(int alternativa) {
        this.alternativa = alternativa;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the fundacao
     */
    public Fundacao getFundacao() {
        return fundacao;
    }

    /**
     * @param fundacao the fundacao to set
     */
    public void setFundacao(Fundacao fundacao) {
        this.fundacao = fundacao;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public ArrayList<ResultadoFundacao> getLinkedResultados() {
        return linkedResultados;
    }

    public void setLinkedResultados(ArrayList<ResultadoFundacao> linkedResultados) {
        this.linkedResultados = linkedResultados;
    }

    public double getTensaoDeConcreto() {
        return tensaoDeConcreto;
    }

    public void setTensaoDeConcreto(double tensaoDeConcreto) {
        this.tensaoDeConcreto = tensaoDeConcreto;
    }
    
    

}
