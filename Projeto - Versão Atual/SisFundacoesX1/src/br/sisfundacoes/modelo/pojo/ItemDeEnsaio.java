/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.modelo.pojo;

import java.io.Serializable;

/**
 *
 * @author Gabriel B Moro
 */
public class ItemDeEnsaio implements Serializable{

    private int profundidade;

    private int NSPT;

    public ItemDeEnsaio(int profundidade, int NSPT) {
        this.profundidade = profundidade;
        this.NSPT = NSPT;
    }

    public int getProfundidade() {
        return profundidade;
    }

    public void setProfundidade(int profundidade) {
        this.profundidade = profundidade;
    }

    public int getNSPT() {
        return NSPT;
    }

    public void setNSPT(int NSPT) {
        this.NSPT = NSPT;
    }

}
