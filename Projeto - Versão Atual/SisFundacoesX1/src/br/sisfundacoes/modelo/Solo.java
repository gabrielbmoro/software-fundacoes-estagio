/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.modelo;

import br.sisfundacoes.modelo.pojo.Estaca;

/**
 *
 * @author Gabriel B Moro
 */
public class Solo {

    private TipoSolo tipo;
    private int k;
    private double alfa_usadoPorAokiVelloso;
    private double alfa_usadoPorDecourtQuaresma;
    private double beta_usadoPorDecourtQuaresma;
    private double c_usadoPorDecourtQuaresma;
    private double de, ate;

    public Solo(TipoSolo tipoDeSolo, double de, double ate) {
        this.tipo = tipoDeSolo;
        this.de = de;
        this.ate = ate;
        configurarValores();
    }

    public void configurarValoresPorEstaca(Estaca estaca) {
        if (estaca.getDimensao() <= 25) {
            alfa_usadoPorDecourtQuaresma = 1;
            beta_usadoPorDecourtQuaresma = 3;
        } else {
            if (tipo == TipoSolo.ARGILA
                    || tipo == TipoSolo.ARGILA_ARENOSA
                    || tipo == TipoSolo.ARGILA_ARENO_SILTOSA
                    || tipo == TipoSolo.ARGILA_SILO_ARENOSA
                    || tipo == TipoSolo.ARGILA_SILTOSA) {
                this.c_usadoPorDecourtQuaresma = 12;
                if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
                    this.alfa_usadoPorDecourtQuaresma = 0.3;
                    this.beta_usadoPorDecourtQuaresma = 1;
                } else if (estaca.getTipo() == TipoEstaca.RAIZ) {
                    this.alfa_usadoPorDecourtQuaresma = 0.85;
                    this.beta_usadoPorDecourtQuaresma = 1.5;
                } else if (estaca.getTipo() == TipoEstaca.ROTATIVA) {
                    if (estaca.isLama()) {
                        alfa_usadoPorDecourtQuaresma = 0.85;
                        beta_usadoPorDecourtQuaresma = 0.9;
                    } else {
                        alfa_usadoPorDecourtQuaresma = 0.85;
                        beta_usadoPorDecourtQuaresma = 0.8;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
            if (tipo == TipoSolo.SILTE_ARGILOSO) {
                this.c_usadoPorDecourtQuaresma = 20;
            }
            if (tipo == TipoSolo.SILTE_ARENOSO) {
                this.c_usadoPorDecourtQuaresma = 25;
            }
            if (tipo == TipoSolo.AREIA
                    || tipo == TipoSolo.AREIA_ARGILOSA
                    || tipo == TipoSolo.AREIA_ARGILO_SILTOSA
                    || tipo == TipoSolo.AREIA_SILTO_ARGILOSA
                    || tipo == TipoSolo.AREIA_SITOSA) {
                this.c_usadoPorDecourtQuaresma = 40;
                if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
                    this.alfa_usadoPorDecourtQuaresma = 0.3;
                    this.beta_usadoPorDecourtQuaresma = 1;
                } else if (estaca.getTipo() == TipoEstaca.RAIZ) {
                    this.alfa_usadoPorDecourtQuaresma = 0.5;
                    this.beta_usadoPorDecourtQuaresma = 1.5;
                } else if (estaca.getTipo() == TipoEstaca.ROTATIVA) {
                    if (estaca.isLama()) {
                        alfa_usadoPorDecourtQuaresma = 0.5;
                        beta_usadoPorDecourtQuaresma = 0.6;
                    } else {
                        alfa_usadoPorDecourtQuaresma = 0.5;
                        beta_usadoPorDecourtQuaresma = 0.5;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
            if (tipo == TipoSolo.SILTE
                    || tipo == TipoSolo.SILTE_ARENOSO
                    || tipo == TipoSolo.SILTE_ARENOSO_ARGILOSO
                    || tipo == TipoSolo.SILTE_ARGILOSO
                    || tipo == TipoSolo.SILTE_ARGILOSO_ARENOSO) {
                this.c_usadoPorDecourtQuaresma = 20;
                if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
                    alfa_usadoPorDecourtQuaresma = 0.3;
                    beta_usadoPorDecourtQuaresma = 1;
                } else if (estaca.getTipo() == TipoEstaca.RAIZ) {
                    alfa_usadoPorDecourtQuaresma = 0.6;
                    beta_usadoPorDecourtQuaresma = 1.5;
                } else if (estaca.getTipo() == TipoEstaca.PRE_MOLDADA_CIRCULAR) {
                    beta_usadoPorDecourtQuaresma = 1;
                } else if (estaca.getTipo() == TipoEstaca.ROTATIVA) {
                    if (estaca.isLama()) {
                        alfa_usadoPorDecourtQuaresma = 0.6;
                        beta_usadoPorDecourtQuaresma = 0.75;
                    } else {
                        alfa_usadoPorDecourtQuaresma = 0.6;
                        beta_usadoPorDecourtQuaresma = 0.65;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
        }
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por configurar os valores padrão de acordo com o tipo
     * de solo.
     */
    private void configurarValores() {
        switch (tipo) {
            case AREIA:
                this.k = 100;
                this.alfa_usadoPorAokiVelloso = 1.4;
                break;
            case AREIA_SITOSA:
                this.k = 80;
                this.alfa_usadoPorAokiVelloso = 2;
                break;
            case AREIA_SILTO_ARGILOSA:
                this.k = 70;
                this.alfa_usadoPorAokiVelloso = 2.4;
                break;
            case AREIA_ARGILOSA:
                this.k = 60;
                this.alfa_usadoPorAokiVelloso = 3;
                break;
            case AREIA_ARGILO_SILTOSA:
                this.k = 50;
                this.alfa_usadoPorAokiVelloso = 2.8;
                break;
            case SILTE:
                this.k = 40;
                this.alfa_usadoPorAokiVelloso = 3;
                break;
            case SILTE_ARENOSO:
                this.k = 55;
                this.alfa_usadoPorAokiVelloso = 2.2;
                break;
            case SILTE_ARENOSO_ARGILOSO:
                this.k = 45;
                this.alfa_usadoPorAokiVelloso = 2.8;
                break;
            case SILTE_ARGILOSO:
                this.k = 23;
                this.alfa_usadoPorAokiVelloso = 3.4;
                break;
            case SILTE_ARGILOSO_ARENOSO:
                this.k = 25;
                this.alfa_usadoPorAokiVelloso = 3;
                break;
            case ARGILA:
                this.k = 20;
                this.alfa_usadoPorAokiVelloso = 6;
                break;
            case ARGILA_ARENOSA:
                this.k = 35;
                this.alfa_usadoPorAokiVelloso = 2.4;
                break;
            case ARGILA_ARENO_SILTOSA:
                this.k = 30;
                this.alfa_usadoPorAokiVelloso = 2.8;
                break;
            case ARGILA_SILTOSA:
                this.k = 22;
                this.alfa_usadoPorAokiVelloso = 4;
                break;
            case ARGILA_SILO_ARENOSA:
                this.k = 33;
                this.alfa_usadoPorAokiVelloso = 3;
                break;
        }
    }

    /**
     * @return the tipo
     */
    public TipoSolo getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(TipoSolo tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the k
     */
    public int getK() {
        return k;
    }

    /**
     * @param k the k to set
     */
    public void setK(int k) {
        this.k = k;
    }

    /**
     * @return the alfa_usadoPorAokiVelloso
     */
    public double getAlfa_usadoPorAokiVelloso() {
        return alfa_usadoPorAokiVelloso;
    }

    /**
     * @param alfa_usadoPorAokiVelloso the alfa_usadoPorAokiVelloso to set
     */
    public void setAlfa_usadoPorAokiVelloso(double alfa_usadoPorAokiVelloso) {
        this.alfa_usadoPorAokiVelloso = alfa_usadoPorAokiVelloso;
    }

    /**
     * @return the alfa_usadoPorDecourtQuaresma
     */
    public double getAlfa_usadoPorDecourtQuaresma() {
        return alfa_usadoPorDecourtQuaresma;
    }

    /**
     * @param alfa_usadoPorDecourtQuaresma the alfa_usadoPorDecourtQuaresma to
     * set
     */
    public void setAlfa_usadoPorDecourtQuaresma(double alfa_usadoPorDecourtQuaresma) {
        this.alfa_usadoPorDecourtQuaresma = alfa_usadoPorDecourtQuaresma;
    }

    /**
     * @return the beta_usadoPorDecourtQuaresma
     */
    public double getBeta_usadoPorDecourtQuaresma() {
        return beta_usadoPorDecourtQuaresma;
    }

    /**
     * @param beta_usadoPorDecourtQuaresma the beta_usadoPorDecourtQuaresma to
     * set
     */
    public void setBeta_usadoPorDecourtQuaresma(double beta_usadoPorDecourtQuaresma) {
        this.beta_usadoPorDecourtQuaresma = beta_usadoPorDecourtQuaresma;
    }

    /**
     * @return the c_usadoPorDecourtQuaresma
     */
    public double getC_usadoPorDecourtQuaresma() {
        return c_usadoPorDecourtQuaresma;
    }

    /**
     * @param c_usadoPorDecourtQuaresma the c_usadoPorDecourtQuaresma to set
     */
    public void setC_usadoPorDecourtQuaresma(double c_usadoPorDecourtQuaresma) {
        this.c_usadoPorDecourtQuaresma = c_usadoPorDecourtQuaresma;
    }

    public double getDe() {
        return de;
    }

    public void setDe(double de) {
        this.de = de;
    }

    public double getAte() {
        return ate;
    }

    public void setAte(double ate) {
        this.ate = ate;
    }

}
