/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.modulos;

import br.sisfundacoes.main.ServicoDeLog;
import br.sisfundacoes.modelo.TipoDeCalculo;
import br.sisfundacoes.modelo.Solo;
import br.sisfundacoes.modelo.TipoEstaca;
import br.sisfundacoes.modelo.pojo.Estaca;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 *
 * @author Gabriel B Moro
 */
@Module
@Singleton
public class ModuloEstrategiaCalculoDeCarga {

    public static double FS_GLOBAL = 2.5;
    public static double FS_LOCAL_PONTA = 4.0;
    public static double FS_LOCAL_ATRITO = 2.0;
    public static double F1 = 3.0;
    public static double F2 = 6.0;
    public static int DELTA_L = 1;
    private double nsptMedio;
    private double perimetro;
    private double nsptDaCamada;
    private double areaDaBase;
    private double nsptPor3;
    private int profundidade;

    @Provides
    ModuloEstrategiaCalculoDeCarga forneceEstrategiaDeCalculo() {
        return new ModuloEstrategiaCalculoDeCarga();
    }

    public int calcularResistenciaTotal(int resistenciaDePonta, int resistenciaLateral) {
        return (resistenciaDePonta + resistenciaLateral);
    }

    public int calcularResistenciaDePonta(Estaca estaca, Solo solo, TipoDeCalculo tipo) {

        double result = 0.0;

        switch (tipo) {
            case AOKI_VELLOSO:
                if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
                    F1 = 2.0;
                } else {
                    F1 = 3.0;
                }

                result = solo.getK() * nsptDaCamada * (areaDaBase / F1);

                ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(),
                        "======>>Qp Aoki, ----" + result + " (resultado) = ["
                        + solo.getK() + "(k) * "
                        + nsptDaCamada + "(nsptCamada) * "
                        + areaDaBase + "(ab)] div "
                        + F1 + "(Constante F1 da Estaca)");

                break;
            case DECOURT_QUARESMA:
                result = solo.getAlfa_usadoPorDecourtQuaresma() * (double) solo.getC_usadoPorDecourtQuaresma() * nsptMedio * areaDaBase;
                
                ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(),
                        "======>>Qp Decourt, ---- " + result + "(resultado) = "
                        + solo.getAlfa_usadoPorDecourtQuaresma() + "(alfa) * "
                        + solo.getK() + "(k) * "
                        + nsptMedio + "(nsptMedio) * "
                        + areaDaBase + "(ab) ");

                break;
        }
        return (int) result;
    }

    public int calcularResistenciaDoContreto(double tensaoDoConcreto) {
        return (int) (tensaoDoConcreto * areaDaBase);
    }
    
    public int calcularResistenciaLateral(Estaca estaca, Solo solo, TipoDeCalculo tipo) {

        double result = 0.0;

        switch (tipo) {
            case AOKI_VELLOSO:
                if (estaca.getTipo() == TipoEstaca.HELICE_CONTINUA) {
                    F2 = 4.0;
                } else {
                    F2 = 6.0;
                }
                int nsptMedioInt = (int) Math.ceil(nsptMedio);

                result = (solo.getAlfa_usadoPorAokiVelloso() / 100.0) * solo.getK() * nsptMedio * perimetro * (DELTA_L / F2);

                ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(),
                        "======>>Ql Aoki, ----" + result
                        + "= [" + solo.getAlfa_usadoPorAokiVelloso() + "(alfa) * "
                        + solo.getK() + "(k) * "
                        + nsptMedioInt + "(nsptMedio) * "
                        + perimetro + "(perimetro) * "
                        + DELTA_L + "(deltaL)] div "
                        + F2 + "F2 da Estaca");

                break;
            case DECOURT_QUARESMA:
                setNsptPor3(nsptPor3 + 1);

                result = solo.getBeta_usadoPorDecourtQuaresma() * nsptPor3 * perimetro * 1 * profundidade;

                ServicoDeLog.fornecerInstancia().registrarLogDeInformacao(getClass(),
                        "======>>Ql Decourt, ---- " + result + " (resultado) = "
                        + solo.getBeta_usadoPorDecourtQuaresma() + "(beta) * "
                        + nsptPor3 + "(nsptPor3) * "
                        + perimetro + "(perimetro) * "
                        + profundidade + "(profundidade) ");

                break;
        }
        return (int) result;
    }

    /**
     * @return the nsptMedio
     */
    public double getNsptMedio() {
        return nsptMedio;
    }

    /**
     * @param nsptMedio the nsptMedio to set
     */
    public void setNsptMedio(double nsptMedio) {
        this.nsptMedio = nsptMedio;
    }

    /**
     * @return the perimetro
     */
    public double getPerimetro() {
        return perimetro;
    }

    /**
     * @param perimetro the perimetro to set
     */
    public void setPerimetro(double perimetro) {
        this.perimetro = perimetro;
    }

    /**
     * @return the nsptDaCamada
     */
    public double getNsptDaCamada() {
        return nsptDaCamada;
    }

    /**
     * @param nsptDaCamada the nsptDaCamada to set
     */
    public void setNsptDaCamada(double nsptDaCamada) {
        this.nsptDaCamada = nsptDaCamada;
    }

    /**
     * @return the areaDaBase
     */
    public double getAreaDaBase() {
        return areaDaBase;
    }

    /**
     * @param areaDaBase the areaDaBase to set
     */
    public void setAreaDaBase(double areaDaBase) {
        this.areaDaBase = areaDaBase;
    }

    /**
     * @return the nsptPor3_decourtquaresma
     */
    public double getNsptPor3() {
        return nsptPor3;
    }

    /**
     * @param nsptPor3_decourtquaresma the nsptPor3_decourtquaresma to set
     */
    public void setNsptPor3(double nsptPor3) {
        this.nsptPor3 = nsptPor3;
    }

    /**
     * @return the profundidade
     */
    public int getProfundidade() {
        return profundidade;
    }

    /**
     * @param profundidade the profundidade to set
     */
    public void setProfundidade(int profundidade) {
        this.profundidade = profundidade;
    }

}
