/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.modulos;

import br.sisfundacoes.main.ServicoDeLog;
import br.sisfundacoes.modelo.CHAVE_UTILIZADA;
import br.sisfundacoes.modelo.pojo.ItemDeEnsaio;
import br.sisfundacoes.modelo.Solo;
import br.sisfundacoes.modelo.TipoSolo;
import br.sisfundacoes.visao.GeradorDeMensagem;
import dagger.Module;
import dagger.Provides;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.inject.Singleton;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

/**
 *
 * @author Gabriel B Moro
 */
@Module
@Singleton
public class ModuloImportarArquivoExcel {

    private WorkbookSettings configuracoesDoExcel;
    
    @Provides
    ModuloImportarArquivoExcel obterInstanciaImportarArquivoExcel(){
        return new ModuloImportarArquivoExcel();
    }
    
    public boolean verificarCompatibilidadeDoFormatoDoArquivo(File file) {
        try {
            Workbook workBook = Workbook.getWorkbook(file);
            Sheet[] abas = workBook.getSheets();
            Sheet abaEscolhida = null;
            for (Sheet temp : abas) {
                if (temp.getName().equalsIgnoreCase(ModuloGeraExcel.NOME_DA_ABA1)) {
                    abaEscolhida = temp;
                }
            }
            if (abaEscolhida == null) {
                return false;
            } else {
                if ((abaEscolhida.findCell("Perfil: ") != null)
                        && (abaEscolhida.findCell("Alternativa: ") != null)
                        && (abaEscolhida.findCell("Referencia: ") != null)
                        && (abaEscolhida.findCell("Tipo de Estaca: ") != null)
                        && (abaEscolhida.findCell("Profundidade (m)") != null)
                        && (abaEscolhida.findCell("Nspt") != null)
                        && (abaEscolhida.findCell("Solo") != null)
                        && (abaEscolhida.findCell("De (m)") != null)
                        && (abaEscolhida.findCell("Ate (m)") != null)) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (IOException erro1) {
             ServicoDeLog.fornecerInstancia().registrarLogDeErro(getClass(),erro1.getMessage());
            return false;
        } catch (BiffException erro2) {
             ServicoDeLog.fornecerInstancia().registrarLogDeErro(getClass(),erro2.getMessage());
            return false;
        }
    }

    public void parametrosImportados(File file) {

        HashMap<CHAVE_UTILIZADA, Object> parametrosPorInjecao = new HashMap<CHAVE_UTILIZADA, Object>();

        try {
            Workbook workBook = Workbook.getWorkbook(file);
            Sheet[] abas = workBook.getSheets();
            Sheet abaEscolhida = null;
            for (Sheet temp : abas) {
                if (temp.getName().equalsIgnoreCase(ModuloGeraExcel.NOME_DA_ABA1)) {
                    abaEscolhida = temp;
                }
            }
            if (abaEscolhida != null) {
                Cell celulaPerfil = abaEscolhida.findCell("Perfil: ");

                String perfilValue = abaEscolhida.getCell(celulaPerfil.getColumn() + 1, celulaPerfil.getRow()).getContents();
                parametrosPorInjecao.put(CHAVE_UTILIZADA.PERFIL, perfilValue);

                Cell celulaAlternativa = abaEscolhida.findCell("Alternativa: ");
                int alternativa = Integer.parseInt(abaEscolhida.getCell(celulaAlternativa.getColumn() + 1, celulaAlternativa.getRow()).getContents());
                parametrosPorInjecao.put(CHAVE_UTILIZADA.ALTERNATIVA, alternativa);

                Cell celulaReferencia = abaEscolhida.findCell("Referencia: ");
                String ref = abaEscolhida.getCell(celulaReferencia.getColumn() + 1, celulaReferencia.getRow()).getContents();
                parametrosPorInjecao.put(CHAVE_UTILIZADA.REFERENCIA, ref);

                Cell celulaTipoEstaca = abaEscolhida.findCell("Tipo de Estaca: ");
                String tipoDaEstaca = abaEscolhida.getCell(celulaTipoEstaca.getColumn() + 1, celulaTipoEstaca.getRow()).getContents();
                parametrosPorInjecao.put(CHAVE_UTILIZADA.TIPO_DE_ESTACA, tipoDaEstaca);

                Cell celulaProfundidade = abaEscolhida.findCell("Profundidade (m)");
                Cell celulaNspt = abaEscolhida.findCell("Nspt");

                int linha = celulaProfundidade.getRow() + 1;
                String contentAuxProfNspt = "";
                ArrayList<ItemDeEnsaio> itemDeEnsaio = new ArrayList<>();
                while (contentAuxProfNspt != null) {
                    Cell cellProfundidade = abaEscolhida.getCell(0, linha);
                    Cell cellNspt = abaEscolhida.getCell(1, linha);
                    if (!cellProfundidade.getContents().isEmpty() && !cellNspt.getContents().isEmpty()) {
                        itemDeEnsaio.add(new ItemDeEnsaio(Integer.parseInt(cellProfundidade.getContents()),
                                Integer.parseInt(cellNspt.getContents())));

                        linha++;
                        continue;
                    } else {
                        contentAuxProfNspt = null;
                    }
                }
                parametrosPorInjecao.put(CHAVE_UTILIZADA.PROFUNDIDADE_NSPT, itemDeEnsaio);

                Cell celulaSolo = abaEscolhida.findCell("Solo");
                Cell celulaDe = abaEscolhida.findCell("De (m)");
                Cell celulaAte = abaEscolhida.findCell("Ate (m)");

                linha = celulaSolo.getRow() + 1;

                String contentAuxSolo = "";

                ArrayList<Solo> solosPorProf = new ArrayList<>();

                while (existeElemento(0, linha, abaEscolhida)) {
                    Cell celulaTipoDeSoloTemp = abaEscolhida.getCell(0, linha);
                    Cell celulaDeTemp = abaEscolhida.getCell(1, linha);
                    Cell celulaAteTemp = abaEscolhida.getCell(2, linha);
                    if (!celulaTipoDeSoloTemp.getContents().isEmpty()
                            && !celulaDeTemp.getContents().isEmpty()
                            && !celulaAteTemp.getContents().isEmpty()) {
                        Solo soloTemp = new Solo(retornaTipoDeSolo(celulaTipoDeSoloTemp.getContents()),
                                Double.parseDouble(celulaDeTemp.getContents()),
                                Double.parseDouble(celulaAteTemp.getContents()));
                        //configurar o solo de acordo com o seu tipo
                        solosPorProf.add(soloTemp);
                        linha++;
                    } else {
                        contentAuxSolo = null;
                        break;
                    }
                }
                parametrosPorInjecao.put(CHAVE_UTILIZADA.SOLOS, solosPorProf);
            }

            workBook.close();
            if (!parametrosPorInjecao.isEmpty()) {
                IncubacaoDeDadosImportados.obterInstancia().setParametros(parametrosPorInjecao);
                GeradorDeMensagem.exibirMensagemDeInformacao("Dados importados com sucesso!", "Alerta ao Usuário");
            }
        } catch (Exception erro) {
            GeradorDeMensagem.exibirMensagemDeInformacao("Ocorreu um problema de importação, por favor realize a operação mais tarde!", "Alerta ao Usuário");
            ServicoDeLog.fornecerInstancia().registrarLogDeErro(getClass(),erro.getMessage());
        }
    }

    private TipoSolo retornaTipoDeSolo(String temp) {
        TipoSolo tipoTemp = null;
        switch (temp) {
            case "Areia":
                tipoTemp = TipoSolo.AREIA;
                break;
            case "Areia Siltosa":
                tipoTemp = TipoSolo.AREIA_SITOSA;
                break;
            case "Areia Silto Argilosa":
                tipoTemp = TipoSolo.AREIA_SILTO_ARGILOSA;
                break;
            case "Areia Argiloso":
                tipoTemp = TipoSolo.AREIA_ARGILOSA;
                break;
            case "Areia Argilosa Siltosa":
                tipoTemp = TipoSolo.AREIA_ARGILO_SILTOSA;
                break;
            case "Silte":
                tipoTemp = TipoSolo.SILTE;
                break;
            case "Silte Arenoso":
                tipoTemp = TipoSolo.SILTE_ARENOSO;
                break;
            case "Silte Arenoso Argiloso":
                tipoTemp = TipoSolo.SILTE_ARENOSO_ARGILOSO;
                break;
            case "Silte Argiloso":
                tipoTemp = TipoSolo.SILTE_ARGILOSO;
                break;
            case "Silte Argiloso Arenoso":
                tipoTemp = TipoSolo.SILTE_ARGILOSO_ARENOSO;
                break;
            case "Argila":
                tipoTemp = TipoSolo.ARGILA;
                break;
            case "Argila Arenosa":
                tipoTemp = TipoSolo.ARGILA_ARENOSA;
                break;
            case "Argila Areno Siltosa":
                tipoTemp = TipoSolo.ARGILA_ARENO_SILTOSA;
                break;
            case "Argila Siltosa":
                tipoTemp = TipoSolo.ARGILA_SILTOSA;
                break;
            case "Argila Silto Arenosa":
                tipoTemp = TipoSolo.ARGILA_SILO_ARENOSA;
                break;
        }

        return tipoTemp;
    }

    private boolean existeElemento(int coluna, int linha, Sheet aba) {
        try {
            Cell celula = aba.getCell(coluna, linha);
            if (celula != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception erro) {
            return false;
        }
    }
}
