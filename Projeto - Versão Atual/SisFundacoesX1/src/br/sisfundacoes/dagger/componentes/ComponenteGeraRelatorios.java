/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.componentes;

import br.sisfundacoes.dagger.modulos.ModuloGeraExcel;
import br.sisfundacoes.dagger.modulos.ModuloGeraPdf;
import br.sisfundacoes.dagger.modulos.ModuloGeraTemplate;
import br.sisfundacoes.visao.capacidade_de_carga.PanelGerarDocumento;
import dagger.Component;
import javax.swing.JPanel;

/**
 *
 * @author Gabriel B Moro
 */
@Component( modules = {ModuloGeraExcel.class, ModuloGeraPdf.class, ModuloGeraTemplate.class})
public interface ComponenteGeraRelatorios {
    
    ModuloGeraTemplate forneceInstanciaDeGeraTemplate();
    
    ModuloGeraExcel forneceInstanciaGeraExcel();
    
    ModuloGeraPdf forneceInstanciaGeraPdf();
    
    void inject(PanelGerarDocumento panel);
    
}
