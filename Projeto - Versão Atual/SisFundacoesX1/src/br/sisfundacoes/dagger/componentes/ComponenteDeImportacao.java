/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.sisfundacoes.dagger.componentes;

import br.sisfundacoes.dagger.modulos.ModuloImportarArquivoExcel;
import br.sisfundacoes.visao.capacidade_de_carga.JFrameImportarDados;
import dagger.Component;

/**
 *
 * @author Gabriel B Moro
 */
@Component(modules = {ModuloImportarArquivoExcel.class})
public interface ComponenteDeImportacao {
            
    ModuloImportarArquivoExcel forneceModuloImportarArquivoExcel();
    
    void inject(JFrameImportarDados frameDeImportacao);
    
}
