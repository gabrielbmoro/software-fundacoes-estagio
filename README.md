# LEIA-ME #

Essa aplicação tem por objetivo auxiliar o Engenheiro Civil no projeto de fundações, 
tanto no cálculo de capacidade de carga, como também em orçamentos.
O software iniciou com uma arquitetura em camadas, na qual encontramos problemas na 
manutenibilidade do software. Nisso, mudamos para uma arquitetura em camadas, utilizando 
o framework Dagger para injeção de dependências e a ferramenta Maven para gerenciar as 
bibliotecas utilizadas pela aplicação desktop. Essa nova versão do software, pode ser 
encontrada na pasta *Projeto Versão Atual/SisFundacoesX*.

### O que esse repositório possui? ###

* Arquivos de artigos, onde o trabalho foi publicado
* Artefatos de código
* Diagramas e demais modelos que representam o funcionamento do software e a maneira na qual ele foi construído

### Como posso instalar o software? ###

* O software não precisa de instalação, apenas é necessário que você possua em sua máquina instalado o JRE8.
* Com o JRE instalado e configurado em sua máquina, você poderá executar o .jar da aplicação

### Com quem eu posso conversar caso eu tenha dúvidas? ###

* Desenvolvedor: Gabriel Bronzatti Moro - gabrielbronzattimoro.es@gmail.com - (55)9.99329276 ou (51)9.99329273
* Responsáveis e Idealizadores: Professor Magnos Baroni - magnos.baroni@gmail.com / Eduardo Carvalho da Costa - eduardo.cdc@hotmail.com