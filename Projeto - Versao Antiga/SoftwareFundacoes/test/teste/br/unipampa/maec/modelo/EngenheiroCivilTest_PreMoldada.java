/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste.br.unipampa.maec.modelo;

import br.unipampa.maec.modelo.Cliente;
import br.unipampa.maec.modelo.EngenheiroCivil;
import br.unipampa.maec.modelo.Estaca;
import br.unipampa.maec.modelo.FundacaoProfunda;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.ResultadoFundacaoProfunda;
import br.unipampa.maec.modelo.Terreno;
import br.unipampa.maec.modelo.TipoEstaca;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gabriel B Moro
 */
public class EngenheiroCivilTest_PreMoldada {

    private static ArrayList<ResultadoFundacaoProfunda> resultados;
   
    public EngenheiroCivilTest_PreMoldada() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        /*Testando para a estaca Franki*/
        System.err.println("================== SetupClass- Configurando dados com Estaca Rotativa ==================");
        Projeto projeto = new Projeto();
        projeto.setCliente(new Cliente("Gabriel B Moro", "Uruguaiana"));
        projeto.setDataDeExecucao("12-12-2012");
        projeto.setDataDoProjeto("12-12-2012");
        projeto.setEmpresaResponsavel("Sotrin");
        projeto.setEngenheiroCivil(new EngenheiroCivil());
        projeto.setPerfil("Edifício");
        projeto.setRef("Hotéis Copacabana");
        projeto.setSondagem(2);
        RepositoryProjetoDeEstacas.getInstance().novo(projeto);
        /*1 - Definição de Dados de Profundidade*/
        FundacaoProfunda fundacaoProfunda = new FundacaoProfunda();
        ArrayList<Integer> nspt = new ArrayList<Integer>();
        nspt.add(19);
        nspt.add(22);
        nspt.add(24);
        nspt.add(29);
        nspt.add(36);
        nspt.add(47);
        Terreno terrenoTemp = new Terreno();
        fundacaoProfunda.setTerreno(terrenoTemp);
        projeto.setFundacaoProfund(fundacaoProfunda);
        RepositoryProjetoDeEstacas.getInstance().alterar(projeto);
        fundacaoProfunda.salvarDadosTerreno(nspt);
        Terreno terreno = RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda().getTerreno();
        /*2- Definição dos Dados de Solo*/
        ArrayList<Object> dados = new ArrayList<>();
        dados.add("Argila");
        dados.add(0.0);
        dados.add(5.0);
        terreno.definirDadosTipoDeSolo(dados);

        RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda().setTerreno(terreno);
        projeto.setFundacaoProfund(fundacaoProfunda);
        RepositoryProjetoDeEstacas.getInstance().alterar(projeto);
        /*3- Escolha da Estaca*/
        Estaca estaca = new Estaca(TipoEstaca.PRE_MOLDADA_CIRCULAR);
        ArrayList<Integer> dimensoesDeEstaca = new ArrayList<>();
        dimensoesDeEstaca.add(30);
        dimensoesDeEstaca.add(40);
        dimensoesDeEstaca.add(50);
        dimensoesDeEstaca.add(60);
        dimensoesDeEstaca.add(70);
        dimensoesDeEstaca.add(80);
        dimensoesDeEstaca.add(90);
        dimensoesDeEstaca.add(100);
        dimensoesDeEstaca.add(110);
        dimensoesDeEstaca.add(120);
        estaca.setDimensoesDisponiveis(dimensoesDeEstaca);
        estaca.registrarDimensoes(estaca);
        /*4- Definição de Faixa de Cálculo*/
        int de = 2;
        int ate = 5;
        int passo = 1;
        fundacaoProfunda.definirDadosDeFatores(de, ate, passo);
        projeto.setFundacaoProfund(fundacaoProfunda);
        RepositoryProjetoDeEstacas.getInstance().alterar(projeto);
        RepositoryProjetoDeEstacas.getInstance().recuperar().definirResultadosFundacaoProfunda(de, ate);
        resultados = RepositoryProjetoDeEstacas.getInstance().recuperar().getResultadosFundacaoProfunda();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt() {
        assertTrue(testeProporcional(18.73, resultados.get(10).getQp_decourt()));
    }

    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt1() {
        assertTrue(testeProporcional(33.30, resultados.get(11).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt3() {
        assertTrue(testeProporcional(52.046, resultados.get(12).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt4() {
        assertTrue(testeProporcional(74.946, resultados.get(13).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt5() {
        assertTrue(testeProporcional(102.009, resultados.get(14).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt6() {
        assertTrue(testeProporcional(133.236, resultados.get(15).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt7() {
        assertTrue(testeProporcional(168.627, resultados.get(16).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt8() {
        assertTrue(testeProporcional(208.182, resultados.get(17).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt9() {
        assertTrue(testeProporcional(251.90, resultados.get(18).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt10() {
        assertTrue(testeProporcional(299, resultados.get(19).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt11() {
        assertTrue(testeProporcional(20.89, resultados.get(20).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt12() {
        assertTrue(testeProporcional(37.152, resultados.get(21).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt13() {
        assertTrue(testeProporcional(58.051, resultados.get(22).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt14() {
        assertTrue(testeProporcional(83.593, resultados.get(23).getQp_decourt()));
    }
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt15() {
        assertTrue(testeProporcional(113.779, resultados.get(24).getQp_decourt()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt16() {
        assertTrue(testeProporcional(148.610, resultados.get(25).getQp_decourt()));
    }
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt17() {
        assertTrue(testeProporcional(188.084, resultados.get(26).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt18() {
        assertTrue(testeProporcional(232.203, resultados.get(27).getQp_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt19() {
        assertTrue(testeProporcional(280, resultados.get(28).getQp_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt20() {
        assertTrue(testeProporcional(334.372, resultados.get(29).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt21() {
        assertTrue(testeProporcional(26.663, resultados.get(30).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt22() {
        assertTrue(testeProporcional(47.401, resultados.get(31).getQp_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt23() {
        assertTrue(testeProporcional(74.065, resultados.get(32).getQp_decourt()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt24() {
        assertTrue(testeProporcional(106.653, resultados.get(33).getQp_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt25() {
        assertTrue(testeProporcional(145.167, resultados.get(34).getQp_decourt()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt26() {
        assertTrue(testeProporcional(189.606, resultados.get(35).getQp_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt27() {
        assertTrue(testeProporcional(239.970, resultados.get(36).getQp_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt28() {
        assertTrue(testeProporcional(296.259, resultados.get(37).getQp_decourt()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt29() {
        assertTrue(testeProporcional(358.473, resultados.get(38).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Decourt30() {
        assertTrue(testeProporcional(426.613, resultados.get(39).getQp_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt() {
        assertTrue(testeProporcional(20.347, resultados.get(10).getQl_decourt()));
    }

    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt1() {
        assertTrue(testeProporcional(27.130, resultados.get(11).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt3() {
        assertTrue(testeProporcional(33.912, resultados.get(12).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt4() {
        assertTrue(testeProporcional(40.694, resultados.get(13).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt5() {
        assertTrue(testeProporcional(47.477, resultados.get(14).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt6() {
        assertTrue(testeProporcional(54.259, resultados.get(15).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt7() {
        assertTrue(testeProporcional(61.042, resultados.get(16).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt8() {
        assertTrue(testeProporcional(67.824, resultados.get(17).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt9() {
        assertTrue(testeProporcional(74.606, resultados.get(18).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt10() {
        assertTrue(testeProporcional(81.389, resultados.get(19).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt11() {
        assertTrue(testeProporcional(32.154, resultados.get(20).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt12() {
        assertTrue(testeProporcional(42.871, resultados.get(21).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt13() {
       assertTrue(testeProporcional(53.589, resultados.get(22).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt14() {
        assertTrue(testeProporcional(64.307, resultados.get(23).getQl_decourt()));
    }
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt15() {
        assertTrue(testeProporcional(75.025, resultados.get(24).getQl_decourt()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt16() {
        assertTrue(testeProporcional(85.743, resultados.get(25).getQl_decourt()));
    }
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt17() {
        assertTrue(testeProporcional(96.461, resultados.get(26).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt18() {
        assertTrue(testeProporcional(107.179, resultados.get(27).getQl_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt19() {
        assertTrue(testeProporcional(117.897, resultados.get(28).getQl_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt20() {
        assertTrue(testeProporcional(128.614, resultados.get(29).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt21() {
        assertTrue(testeProporcional(48.984, resultados.get(30).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt22() {
        assertTrue(testeProporcional(65.312, resultados.get(31).getQl_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt23() {
        assertTrue(testeProporcional(81.640, resultados.get(32).getQl_decourt()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt24() {
        assertTrue(testeProporcional(97.968, resultados.get(33).getQl_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt25() {
        assertTrue(testeProporcional(114.926, resultados.get(34).getQl_decourt()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt26() {
        assertTrue(testeProporcional(130.624, resultados.get(35).getQl_decourt()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt27() {
        assertTrue(testeProporcional(146.952, resultados.get(36).getQl_decourt()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt28() {
        assertTrue(testeProporcional(163.280, resultados.get(37).getQl_decourt()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateralDecourt29() {
        assertTrue(testeProporcional(179.608, resultados.get(38).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Decourt30() {
        assertTrue(testeProporcional(195.936, resultados.get(39).getQl_decourt()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki() {
        assertTrue(testeProporcional(21, resultados.get(10).getQp_aoki()));
    }

    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki1() {
        assertTrue(testeProporcional(36, resultados.get(11).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki3() {
        assertTrue(testeProporcional(51, resultados.get(12).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki4() {
        assertTrue(testeProporcional(67, resultados.get(13).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki5() {
        assertTrue(testeProporcional(86, resultados.get(14).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki6() {
        assertTrue(testeProporcional(107, resultados.get(15).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki7() {
        assertTrue(testeProporcional(129, resultados.get(16).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki8() {
        assertTrue(testeProporcional(152, resultados.get(17).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki9() {
        assertTrue(testeProporcional(174, resultados.get(18).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki10() {
        assertTrue(testeProporcional(197, resultados.get(19).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki11() {
        assertTrue(testeProporcional(25, resultados.get(20).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki12() {
        assertTrue(testeProporcional(43, resultados.get(21).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki13() {
        assertTrue(testeProporcional(62, resultados.get(22).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki14() {
        assertTrue(testeProporcional(81, resultados.get(23).getQp_aoki()));
    }
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki15() {
        assertTrue(testeProporcional(104, resultados.get(24).getQp_aoki()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki16() {
        assertTrue(testeProporcional(129, resultados.get(25).getQp_aoki()));
    }
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki17() {
        assertTrue(testeProporcional(156, resultados.get(26).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki18() {
        assertTrue(testeProporcional(183, resultados.get(27).getQp_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki19() {
        assertTrue(testeProporcional(210, resultados.get(28).getQp_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki20() {
        assertTrue(testeProporcional(238, resultados.get(29).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki21() {
        assertTrue(testeProporcional(31, resultados.get(30).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki22() {
        assertTrue(testeProporcional(53, resultados.get(31).getQp_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki23() {
        assertTrue(testeProporcional(77, resultados.get(32).getQp_aoki()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki24() {
        assertTrue(testeProporcional(101, resultados.get(33).getQp_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki25() {
        assertTrue(testeProporcional(129, resultados.get(34).getQp_aoki()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki26() {
        assertTrue(testeProporcional(160, resultados.get(35).getQp_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki27() {
        assertTrue(testeProporcional(194, resultados.get(36).getQp_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki28() {
        assertTrue(testeProporcional(228, resultados.get(37).getQp_aoki()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki29() {
        assertTrue(testeProporcional(261, resultados.get(38).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaDePonta_Aoki30() {
        assertTrue(testeProporcional(296, resultados.get(39).getQp_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki() {
        assertTrue(testeProporcional(9, resultados.get(10).getQl_aoki()));
    }

    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki1() {
        assertTrue(testeProporcional(11, resultados.get(11).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki3() {
        assertTrue(testeProporcional(13, resultados.get(12).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki4() {
        assertTrue(testeProporcional(15, resultados.get(13).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki5() {
        assertTrue(testeProporcional(16, resultados.get(14).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki6() {
        assertTrue(testeProporcional(17, resultados.get(15).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki7() {
        assertTrue(testeProporcional(19, resultados.get(16).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki8() {
        assertTrue(testeProporcional(20, resultados.get(17).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki9() {
        assertTrue(testeProporcional(21, resultados.get(18).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki10() {
        assertTrue(testeProporcional(21, resultados.get(19).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki11() {
        assertTrue(testeProporcional(10, resultados.get(20).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki2() {
        assertTrue(testeProporcional(13, resultados.get(21).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki13() {
       assertTrue(testeProporcional(15, resultados.get(22).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki14() {
        assertTrue(testeProporcional(16 , resultados.get(23).getQl_aoki()));
    }
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki15() {
        assertTrue(testeProporcional(18, resultados.get(24).getQl_aoki()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki16() {
        assertTrue(testeProporcional(19, resultados.get(25).getQl_aoki()));
    }
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki17() {
        assertTrue(testeProporcional(21, resultados.get(26).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki18() {
        assertTrue(testeProporcional(22, resultados.get(27).getQl_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki19() {
        assertTrue(testeProporcional(23, resultados.get(28).getQl_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki20() {
        assertTrue(testeProporcional(24, resultados.get(29).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki21() {
        assertTrue(testeProporcional(13, resultados.get(30).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki22() {
        assertTrue(testeProporcional(16, resultados.get(31).getQl_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki23() {
        assertTrue(testeProporcional(19, resultados.get(32).getQl_aoki()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki24() {
        assertTrue(testeProporcional(21, resultados.get(33).getQl_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki25() {
        assertTrue(testeProporcional(23, resultados.get(34).getQl_aoki()));
    }
    
       @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki26() {
        assertTrue(testeProporcional(25, resultados.get(35).getQl_aoki()));
    }
    
      @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki27() {
        assertTrue(testeProporcional(26, resultados.get(36).getQl_aoki()));
    }
    
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki28() {
        assertTrue(testeProporcional(28, resultados.get(37).getQl_aoki()));
    }
     @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateralAoki29() {
        assertTrue(testeProporcional(29, resultados.get(38).getQl_aoki()));
    }
    
    @Test
    public void testCalcularCapacidadeDeCarga_FundResistenciaLateral_Aoki30() {
        assertTrue(testeProporcional(30, resultados.get(39).getQl_aoki()));
    }
    public boolean testeProporcional(double valor1, double valor2) {
        double valor1Arredondado = (int) Math.round(valor1);
        double valor2Arredondado = (int) Math.round(valor2);

        if (valor1Arredondado == valor2Arredondado
                    || valor2Arredondado + 1 == valor1Arredondado
                    || valor2Arredondado - 1 == valor1Arredondado
                    || valor2Arredondado + 2 == valor1Arredondado
                    || valor2Arredondado - 2 == valor1Arredondado
                    || valor2Arredondado + 3 == valor1Arredondado
                    || valor2Arredondado - 3 == valor1Arredondado) {
            return true;
        } else {
            return false;
        }
    }

}
