/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste.br.unipampa.maec.modelo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Gabriel B Moro
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({teste.br.unipampa.maec.modelo.EngenheiroCivilTest_Rotativa.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_Strauss.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_Franki.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_Raiz.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_PreMoldada.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_HC.class, teste.br.unipampa.maec.modelo.EngenheiroCivilTest_Trilho.class})
public class ConjuntoDeTeste {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
