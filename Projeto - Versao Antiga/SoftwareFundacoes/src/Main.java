
import br.unipampa.maec.apresentacao.JFrameInicial;
import br.unipampa.maec.controle.ControleInicial;



/**
 * <b>Propósito:</b>
 * <br>
 * Classe responsável por inicializar a aplicação.
 * 
 * @author GabrielBMoro
 * @version 0.4
 * @since 24/01/2014
 */
public class Main {
/**
 * <b>Propósito:</b>
 * <br>
 * Acionar o fluxo raíx das atividades da aplicação.
 * @param args 
 */
    public static void main(String[] args) {
        new ControleInicial(new JFrameInicial());
    }
}
