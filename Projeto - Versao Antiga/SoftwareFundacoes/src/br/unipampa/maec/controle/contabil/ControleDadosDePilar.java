/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelDadosDePilar;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.modelo.FundacaoProfunda;
import br.unipampa.maec.modelo.Projeto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleDadosDePilar implements Controller {

    private PanelDadosDePilar jPanelDadosDePilar;
    private JFramePrincipalQuantitativo jFrameQuantitativo;

    public ControleDadosDePilar(PanelDadosDePilar jPanelDadosDePilar, JFramePrincipalQuantitativo jFrameQuantitativo) {
        this.jFrameQuantitativo = jFrameQuantitativo;
        this.jPanelDadosDePilar = jPanelDadosDePilar;
        registrarListeners();
    }

    @Override
    public void registrarListeners() {
        this.jPanelDadosDePilar.getjTablePilarPorCarregamento().addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().addRow(new Object[]{jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().getRowCount() + 1, null});
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        this.jPanelDadosDePilar.getBtnSalvarDadosIniciais().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Projeto projeto = new Projeto();
                FundacaoProfunda fundacaoProfunda = projeto.recuperarFundacaoProfunda();
                boolean resp = fundacaoProfunda.registrarPilares(jPanelDadosDePilar.recuperarDadosDaInterface());
                if (resp) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Os dados foram salvos com sucesso!", "Alerta de Usuário");
                    jPanelDadosDePilar.desabilitarComponentes();
                    jFrameQuantitativo.getBtnAvancar().setEnabled(true);
                    jFrameQuantitativo.getBtnVoltar().setEnabled(false);
                    jPanelDadosDePilar.getBtnEditarDadosIniciais().setEnabled(true);
                    jPanelDadosDePilar.getBtnSalvarDadosIniciais().setEnabled(false);
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Ocorreu um problema, realize a operação mais tarde.", "Alerta ao Usuário");
                }
            }
        });
        this.jPanelDadosDePilar.getBtnEditarDadosIniciais().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPanelDadosDePilar.habilitarComponentes();
                jFrameQuantitativo.getBtnAvancar().setEnabled(false);
                jFrameQuantitativo.getBtnVoltar().setEnabled(true);
                jPanelDadosDePilar.getBtnEditarDadosIniciais().setEnabled(false);
                jPanelDadosDePilar.getBtnSalvarDadosIniciais().setEnabled(true);
            }
        });
        this.jPanelDadosDePilar.getBtnAdd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().addRow(new Object[]{jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().getRowCount() + 1, null});
            }
        });
        this.jPanelDadosDePilar.getBtnRemove().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int linhas = jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().getRowCount() - 1;
                if (linhas != 0) {
                    jPanelDadosDePilar.getModeloTabelaCarregamentoDePilar().removeRow(linhas);
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Não é possível remover linhas...", "Alerta ao Usuário");
                }
            }
        });
    }
}
