/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.JFrameInicial;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelDadosDePilar;
import br.unipampa.maec.apresentacao.contabil.PanelDadosIniciais;
import br.unipampa.maec.apresentacao.contabil.PanelDefinicaoDeEstaca;
import br.unipampa.maec.apresentacao.contabil.PanelEscolhaEstaca;
import br.unipampa.maec.apresentacao.contabil.PanelResumo;
import br.unipampa.maec.controle.ControleInicial;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.controle.TrataWindowListener;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleFramePrincipalQuantitativo implements Controller {

    private JFramePrincipalQuantitativo jFrameQuantitativo;
    private ControleDadosIniciais controleDadosIniciais;
    private ControleEscolhaTipoEstacaQualitativo controleTipoDeEstaca;
    private ControleDadosDePilar controleDadosDePilar;
    private ControleDefinicaoDeEstaca controleDefinicaoDeEstaca;
    private ControleResumo controleResumo;
    private ArrayList<JPanel> vectorJPanels;
    public static int ITERADOR = 0;

    public ControleFramePrincipalQuantitativo(JFramePrincipalQuantitativo jFramePrincipalQuantitativo) {
        this.jFrameQuantitativo = jFramePrincipalQuantitativo;
        this.vectorJPanels = new ArrayList<>();
        this.jFrameQuantitativo.addWindowListener(new TrataWindowListener());
        preencherVector();
        registrarListeners();
    }

    public void preencherVector() {
        this.vectorJPanels.add(jFrameQuantitativo.getjPanelBoasVindas());
        this.vectorJPanels.add(jFrameQuantitativo.getPanelDadosIniciais1());
        this.vectorJPanels.add(jFrameQuantitativo.getPanelEscolhaEstaca1());
        this.vectorJPanels.add(jFrameQuantitativo.getPanelDadosDePilar1());
        this.vectorJPanels.add(jFrameQuantitativo.getPanelDefinicaoDeEstaca1());
        this.vectorJPanels.add(jFrameQuantitativo.getPanelResumo1());
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção voltar.
     */
    private void registrarListenerBotaoVoltar() {
        if (ITERADOR > 0) {
            ITERADOR -= 1;
            registrarListeners(vectorJPanels.get(ITERADOR));
            jFrameQuantitativo.exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            System.err.println("Voltado: " + ITERADOR);
            jFrameQuantitativo.getjProgressBar1().setValue(jFrameQuantitativo.getjProgressBar1().getValue() - 25);
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção avançar.
     */
    private void registrarListenerBotaoAvancar() {
        if (ITERADOR <= vectorJPanels.size()) {
            ITERADOR += 1;
            registrarListeners(vectorJPanels.get(ITERADOR));
            jFrameQuantitativo.exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            System.err.println("Avançado: " + ITERADOR);
            jFrameQuantitativo.getjProgressBar1().setValue(jFrameQuantitativo.getjProgressBar1().getValue() + 25);
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por configurar os listeners para um determinado JPanel,
     * diferentemente do método anterior essa sobrecarga foi utilizada para aplicar a
     * mesma funcionalidade para outra situação.
     *
     * @param jPanel de tipo {@link JPanel}
     */
    public void registrarListeners(JPanel jPanel) {
        if (jPanel instanceof PanelDadosDePilar) {
            PanelDadosDePilar panel = (PanelDadosDePilar) jPanel;
            this.controleDadosDePilar = new ControleDadosDePilar(panel, jFrameQuantitativo);
        } else if (jPanel instanceof PanelDadosIniciais) {
            PanelDadosIniciais panel = (PanelDadosIniciais) jPanel;
            this.controleDadosIniciais = new ControleDadosIniciais(panel, jFrameQuantitativo);
        } else if (jPanel instanceof PanelEscolhaEstaca) {
            PanelEscolhaEstaca panel = (PanelEscolhaEstaca) jPanel;
            this.controleTipoDeEstaca = new ControleEscolhaTipoEstacaQualitativo(panel, jFrameQuantitativo);
        } else if (jPanel instanceof PanelDefinicaoDeEstaca) {
            PanelDefinicaoDeEstaca panel = (PanelDefinicaoDeEstaca) jPanel;
            this.controleDefinicaoDeEstaca = new ControleDefinicaoDeEstaca(panel, jFrameQuantitativo);
        } else if (jPanel instanceof PanelResumo) {
            PanelResumo panelResumo = (PanelResumo) jPanel;
            this.controleResumo = new ControleResumo(panelResumo, jFrameQuantitativo);
        }
    }

    public void registrarListeners() {
        this.jFrameQuantitativo.getBtnAvancar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
                jFrameQuantitativo.getBtnAvancar().setEnabled(false);
                jFrameQuantitativo.getBtnVoltar().setEnabled(true);

            }
        });
        this.jFrameQuantitativo.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
                jFrameQuantitativo.getBtnVoltar().setEnabled(false);
                jFrameQuantitativo.getBtnAvancar().setEnabled(true);
            }
        });
        this.jFrameQuantitativo.getjMenuItemInicio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RepositoryProjetoDeEstacas.getInstance().alterar(null);
                jFrameQuantitativo.dispose();
                new ControleInicial(new JFrameInicial());
            }
        });
        this.jFrameQuantitativo.getjMenuItemAvancar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
            }
        });
        this.jFrameQuantitativo.getjMenuItemVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
            }
        });
    }
}
