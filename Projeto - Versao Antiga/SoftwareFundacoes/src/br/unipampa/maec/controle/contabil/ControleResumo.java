/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelResumo;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.ResultadoQualitativo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleResumo implements Controller {

    private PanelResumo panelResumo;
    private JFramePrincipalQuantitativo jFramePrincipalQuantitativo;

    public ControleResumo(PanelResumo panelResumo, JFramePrincipalQuantitativo jFramePrincipalQuantitativo) {
        this.panelResumo = panelResumo;
        this.jFramePrincipalQuantitativo = jFramePrincipalQuantitativo;
        registrarListeners();
    }

    @Override
    public void registrarListeners() {
        this.panelResumo.getBtnCalcular().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Projeto projeto = new Projeto();
                projeto.definirResultadosQualitativos();
               ArrayList<ResultadoQualitativo> resultadosQualitativos =  projeto.getResultadosQualitativo();
               ArrayList<Integer> dadosInteiros = new ArrayList<>();
               if(resultadosQualitativos!=null){
                   for(ResultadoQualitativo resultado : 
                               resultadosQualitativos){
                       dadosInteiros.add(resultado.getPilar().getIdentificador());
                       dadosInteiros.add(resultado.getPilar().getCarga());
                       dadosInteiros.add(resultado.getEstaca().getDimensaoDefinida());
                       dadosInteiros.add(resultado.getQuantidade());
                   }
               }
               panelResumo.adicionarDadosNaTabela(dadosInteiros);
               panelResumo.getjScrollPane1().setVisible(true);
               panelResumo.revalidate();
            }
        });
    }
}
