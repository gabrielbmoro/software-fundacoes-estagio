/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelDefinicaoDeEstaca;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.modelo.Estaca;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.TipoEstaca;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleDefinicaoDeEstaca implements Controller {
    
    private PanelDefinicaoDeEstaca panelDefinicaoDeEstaca;
    private JFramePrincipalQuantitativo jFramePrincipalQuantitativo;
    
    public ControleDefinicaoDeEstaca(PanelDefinicaoDeEstaca panelDefinicaoDeEstaca, JFramePrincipalQuantitativo jFramePrincipalQuantitativo) {
        this.panelDefinicaoDeEstaca = panelDefinicaoDeEstaca;
        this.jFramePrincipalQuantitativo = jFramePrincipalQuantitativo;
        this.registrarListeners();
    }
    
    @Override
    public void registrarListeners() {
        this.panelDefinicaoDeEstaca.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<Estaca> estacas = new ArrayList<Estaca>();
                if (!panelDefinicaoDeEstaca.restauraDadosDeInterface().isEmpty()) {
                    ArrayList<Object> valores = panelDefinicaoDeEstaca.restauraDadosDeInterface();
                    for (int count = 0; count < valores.size(); count++) {
                        if ((count % 2) == 0) {
                            if (valores.get(count) instanceof Integer && valores.get(count + 1) instanceof Integer) {
                                int valorDimensao = (int) valores.get(count);
                                int valorCapacidadeDeCarga = (int) valores.get(count + 1);
                                Estaca estaca = new Estaca(valorDimensao, TipoEstaca.FRANKI);
                                estaca.setCapacidadeDeCarga(valorCapacidadeDeCarga);
                                estacas.add(estaca);
                                count++;
                            }
                        }
                    }
                }
                Projeto projetoTemp = new Projeto();
                projetoTemp.registrarEstacas(estacas);
                if (estacas != null) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Os dados foram cadastrados com sucesso!", "Alerta ao Usuário");
                    panelDefinicaoDeEstaca.desabilitarComponentes();
                    panelDefinicaoDeEstaca.getBtnEditar().setEnabled(true);
                    panelDefinicaoDeEstaca.getBtnSalvar().setEnabled(false);
                    jFramePrincipalQuantitativo.getBtnAvancar().setEnabled(true);
                    jFramePrincipalQuantitativo.getBtnVoltar().setEnabled(false);
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Ocorreu um problema, por favor realize a operação novamente!", "Alerta ao Usuário");
                    
                }
            }
        });
        
        this.panelDefinicaoDeEstaca.getBtnEditar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelDefinicaoDeEstaca.getBtnSalvar().setEnabled(true);
                panelDefinicaoDeEstaca.getBtnEditar().setEnabled(false);
                jFramePrincipalQuantitativo.getBtnVoltar().setEnabled(true);
                jFramePrincipalQuantitativo.getBtnAvancar().setEnabled(false);
                panelDefinicaoDeEstaca.habilitarComponentes();
            }
        });
        
        this.panelDefinicaoDeEstaca.getTableDimensaoQp().addKeyListener(new KeyListener() {
            
            @Override
            public void keyTyped(KeyEvent e) {
            }
            
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    panelDefinicaoDeEstaca.getModeloDeTabela().addRow(new Object[]{null, null});
                }
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
            }
        }
        );
        this.panelDefinicaoDeEstaca.getBtnAdd().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelDefinicaoDeEstaca.getModeloDeTabela().addRow(new Object[]{null, null});
            }
        });
        this.panelDefinicaoDeEstaca.getBtnRemove().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int linhas = panelDefinicaoDeEstaca.getModeloDeTabela().getRowCount() - 1;
                if (linhas != 0) {
                    panelDefinicaoDeEstaca.getModeloDeTabela().removeRow(linhas);
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Não é possível remover linhas...", "Alerta ao Usuário");
                }
            }
        });
    }
}
