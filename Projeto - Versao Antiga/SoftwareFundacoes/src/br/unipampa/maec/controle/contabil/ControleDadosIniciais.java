/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.OperarGrafico;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelDadosIniciais;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.modelo.Cliente;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleDadosIniciais implements Controller {

    private PanelDadosIniciais jPanelDadosIniciais;
    private JFramePrincipalQuantitativo jFrameQuantitativo;
    private Projeto projeto;

    public ControleDadosIniciais(PanelDadosIniciais jPanelDadosIniciais, JFramePrincipalQuantitativo jFrameQuantitativo) {
        this.jFrameQuantitativo = jFrameQuantitativo;
        this.jPanelDadosIniciais = jPanelDadosIniciais;
        registrarListeners();
    }

    @Override
    public void registrarListeners() {
        this.jPanelDadosIniciais.getBtnSalvarDadosIniciais().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String txtLocal = jPanelDadosIniciais.getTxtLocal().getText();
                    String nomeCliente = jPanelDadosIniciais.getTxtCliente().getText();
                    String projetoEstrutural = jPanelDadosIniciais.getTxtProjetoEstrutural().getText();
                    String dadosReferencia = jPanelDadosIniciais.getTxtReferencia().getText();
                    String sondagem = jPanelDadosIniciais.getTxtSondagem().getText();
                    String empresaResponsavel = jPanelDadosIniciais.getTxtEmpresaResponsavel().getText();
                    String dataDeExecucao = OperarGrafico.recuperarData(jPanelDadosIniciais.getjDateDeExecucao().getCalendar());
                    String dataDeProjeto = OperarGrafico.recuperarData(jPanelDadosIniciais.getjDateDoProjeto().getCalendar());

                    if (!txtLocal.isEmpty() && !nomeCliente.isEmpty()
                                && !projetoEstrutural.isEmpty() && !dadosReferencia.isEmpty() && !sondagem.isEmpty()) {
                        try {
                            Projeto projeto = new Projeto();
                            boolean resposta = projeto.registrarDados(nomeCliente,
                                        txtLocal, dadosReferencia,
                                        dataDeProjeto, dataDeExecucao,
                                        Integer.parseInt(sondagem),
                                        empresaResponsavel);

                            if (resposta) {
                                jPanelDadosIniciais.desabilitarComponentes();
                                jPanelDadosIniciais.getBtnEditarDadosIniciais().setEnabled(true);
                                jPanelDadosIniciais.getBtnSalvarDadosIniciais().setEnabled(false);
                                jFrameQuantitativo.getBtnAvancar().setEnabled(true);
                                jFrameQuantitativo.getBtnVoltar().setEnabled(false);
                                GeradorDeMensagem.exibirMensagemDeInformacao("Dados cadastrados com sucesso!", "Alerta ao Usuário");
                            } else {
                                GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um problema, por favor realize a operação novamente!");
                            }
                        } catch (Exception erro) {
                            GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um problema, por favor realize a operação novamente!");
                        }
                    } else {
                        GeradorDeMensagem.exibirMensagemDeInformacao("Digite novamente os seus dados!", "Alerta ao Usuário");
                    }
                } catch (Exception erro) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Digite novamente os seus dados!", "Alerta ao Usuário");
                }
            }
        });
        this.jPanelDadosIniciais.getBtnEditarDadosIniciais().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jPanelDadosIniciais.habilitarComponentes();
                jPanelDadosIniciais.getBtnEditarDadosIniciais().setEnabled(false);
                jPanelDadosIniciais.getBtnSalvarDadosIniciais().setEnabled(true);
            }
        });
    }
}
