/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle.contabil;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.contabil.PanelEscolhaEstaca;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.modelo.Estaca;
import br.unipampa.maec.modelo.FundacaoProfunda;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.TipoEstaca;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleEscolhaTipoEstacaQualitativo implements Controller {
    
    private PanelEscolhaEstaca panelEscolhaEstaca;
    private JFramePrincipalQuantitativo jFrameQualitativo;
    
    public ControleEscolhaTipoEstacaQualitativo(PanelEscolhaEstaca panelEscolhaEstaca, JFramePrincipalQuantitativo jFramePrincipalQualitativo) {
        this.panelEscolhaEstaca = panelEscolhaEstaca;
        this.jFrameQualitativo = jFramePrincipalQualitativo;
        registrarListeners();
    }
    
    @Override
    public void registrarListeners() {
        this.panelEscolhaEstaca.getBtnSalvar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String comprimentoTxt = panelEscolhaEstaca.getTxtComprimento().getText();
                String alternativaTexto = panelEscolhaEstaca.getjFormattedTxtAlternativa().getText();
                panelEscolhaEstaca.escolhaTipoEstaca();
                TipoEstaca tipoTabela = panelEscolhaEstaca.TIPO_ESCOLHIDO;
                try {
                    int comprimento = Integer.parseInt(comprimentoTxt);
                    int alternativa = Integer.parseInt(alternativaTexto);
                    Estaca estacaEscolhida = converteTipoEscolhidoParaEstaca(tipoTabela);
                    if (estacaEscolhida != null && comprimento > 0) {
                        Projeto projeto = new Projeto();
                        FundacaoProfunda fundacaoProfunda = projeto.recuperarFundacaoProfunda();
                        estacaEscolhida.setAltura(comprimento);
                        fundacaoProfunda.setAlternativa(alternativa);
                        fundacaoProfunda.setEstacaReferencia(estacaEscolhida);
                        projeto.registrarFundacaoProfunda(fundacaoProfunda);
                        GeradorDeMensagem.exibirMensagemDeInformacao("Os dados foram cadastrados com sucesso!",
                                    "Alerta ao Usuário");
                        jFrameQualitativo.getBtnAvancar().setEnabled(true);
                        jFrameQualitativo.getBtnAvancar().setFocusable(true);
                        panelEscolhaEstaca.desabilitarComponentes();
                        panelEscolhaEstaca.getBtnSalvar().setEnabled(false);
                        panelEscolhaEstaca.getBtnEditar().setEnabled(true);
                    } else {
                        GeradorDeMensagem.exibirMensagemDeInformacao("Por favor, preencha com dados válidos, realize novamente a operação!",
                                    "Alerta ao Usuário");
                    }
                } catch (Exception erro) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Por favor, preencha com dados válidos, realize novamente a operação!",
                                "Alerta ao Usuário");
                }
            }
        });
        
        this.panelEscolhaEstaca.getBtnEditar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                panelEscolhaEstaca.habilitarComponentes();
                panelEscolhaEstaca.getBtnSalvar().setEnabled(true);
                panelEscolhaEstaca.getBtnEditar().setEnabled(false);
            }
        });
        
        this.panelEscolhaEstaca.getjTableTiposEstacas().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (panelEscolhaEstaca.getjTextFieldTipoDeEstacaEscolhido().isEnabled()
                            && panelEscolhaEstaca.getjFormattedTxtAlternativa().isEnabled()) {
                    panelEscolhaEstaca.escolhaTipoEstaca();
                }
            }
        });
        this.panelEscolhaEstaca.getjTableTiposEstacas().addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (panelEscolhaEstaca.getjTextFieldTipoDeEstacaEscolhido().isEnabled()
                            && panelEscolhaEstaca.getjFormattedTxtAlternativa().isEnabled()) {
                    if (evt.getKeyCode() == evt.VK_ENTER) {
                        panelEscolhaEstaca.escolhaTipoEstaca();
                    }
                }
            }
        });
    }
    
    public Estaca converteTipoEscolhidoParaEstaca(TipoEstaca tipoEstaca) {
//        switch (tipoEstaca) {
//            case FRANKI:
//                return new EstacaFranki();
//            case HELICE_CONTINUA:
//                return new EstacaHeliceContinua();
//            case PRE_MOLDADA_QUADRADA:
//                return new EstacaPreMoldadaQuadrada();
//            case PRE_MOLDADA_SEXTAVADA:
//                return new EstacaPreMoldadaSextavada();
//            case PRE_MOLDADA_TUBULAR:
//                return new EstacaPreMoldadaTubular();
//            case ROTATIVA:
//                return new EstacaRotativa();
//            case RAIZ:
//                return new EstacaRaiz();
//            case STRAUSS:
//                return new EstacaStraus();
//            case TRILHO:
//                return new EstacaTrilho();
//        }
        return null;
    }
    
}
