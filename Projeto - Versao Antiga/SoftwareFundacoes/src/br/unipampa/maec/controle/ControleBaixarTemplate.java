/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.carga.JFrameBaixarTemplate;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.servicos.GeraTemplate;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleBaixarTemplate implements Controller {

    private JFrameBaixarTemplate jFrameBaixarTemplate;
    private File diretorio;

    public ControleBaixarTemplate(JFrameBaixarTemplate jFrameBaixarTemplate) {
        this.jFrameBaixarTemplate = jFrameBaixarTemplate;
        registrarListeners();
        this.jFrameBaixarTemplate.setVisible(true);
    }

    @Override
    public void registrarListeners() {

        this.jFrameBaixarTemplate.getBtnPesquisarParaBaixar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameBaixarTemplate.setjFileChooser(new JFileChooser());
                jFrameBaixarTemplate.getjFileChooser().setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int resposta = jFrameBaixarTemplate.getjFileChooser().showOpenDialog(null);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    diretorio = jFrameBaixarTemplate.getjFileChooser().getSelectedFile();
                    GeradorDeMensagem.exibirMensagemDeInformacao("Você escolheu o diretório: " + diretorio.getName().toString(), "Alerta à Usuário");
                    jFrameBaixarTemplate.getLblCaminhoDiretorio().setText(diretorio.getPath().toString());
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Você não selecionou nenhum diretório", "Alerta à Usuário");
                }
            }
        });
        this.jFrameBaixarTemplate.getBtnBaixar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GeraTemplate geraTemplate = new GeraTemplate();
                boolean resposta = geraTemplate.gerarTemplate(jFrameBaixarTemplate.getLblCaminhoDiretorio().getText().toString());
                if (resposta) {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Template Gerado com Sucesso!", "Alerta ao Usuário");
                    jFrameBaixarTemplate.dispose();
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Não foi possível realizar essa operação, tente-a mais tarde!", "Alerta ao Usuário");
                }
            }
        });
    }

}
