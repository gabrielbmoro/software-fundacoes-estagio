/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.controle.contabil.ControleFramePrincipalQuantitativo;
import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.JFrameInicial;
import br.unipampa.maec.apresentacao.carga.JFramePrincipalCarga;
import br.unipampa.maec.apresentacao.contabil.JFramePrincipalQuantitativo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleInicial implements Controller {

    private JFrameInicial jFrameInicial;
    private ControlePrincipalCarga controleCapacidadeDeCarga;
    private ControleFramePrincipalQuantitativo controleQuantitativo;

    public ControleInicial(JFrameInicial jFrameInicial) {
        this.jFrameInicial = jFrameInicial;
        this.jFrameInicial.addWindowListener(new TrataWindowListener());
        registrarListeners();
    }

    @Override
    public void registrarListeners() {
        this.jFrameInicial.getBtnCapacidadeDeCarga().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameInicial.dispose();
                controleCapacidadeDeCarga= new ControlePrincipalCarga(new JFramePrincipalCarga());
            }
        });
        this.jFrameInicial.getBtnQuantitativo().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameInicial.dispose();
                controleQuantitativo = new ControleFramePrincipalQuantitativo(new JFramePrincipalQuantitativo());
            }
        });
        this.jFrameInicial.getBtnSair().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean resp = GeradorDeMensagem.exibirMensagemDeConfirmacao("Você deseja realmente sair da aplicação?", "Alerta ao Usuário");
                if (resp) {
                    System.exit(0);
                } else {
                    return;
                }
            }
        });
    }
}
