/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.modelo.FundacaoProfunda;
import br.unipampa.maec.modelo.ItemDeEnsaio;
import br.unipampa.maec.modelo.Solo;
import br.unipampa.maec.modelo.Terreno;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import br.unipampa.maec.servicos.CHAVE_UTILIZADA;
import br.unipampa.maec.servicos.IncubacaoDeDadosImportados;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleObservador implements Observer {

    private ControlePrincipalCarga controleGeral;

    public ControleObservador(ControlePrincipalCarga controleGeral) {
        IncubacaoDeDadosImportados.getMyInstance().addObserver(this);
        this.controleGeral = controleGeral;
    }

    @Override
    public void update(Observable o, Object arg) {
        HashMap<CHAVE_UTILIZADA, Object> parametros = IncubacaoDeDadosImportados.getMyInstance().getParametros();
        if (parametros != null) {
            Object perfil = parametros.get(CHAVE_UTILIZADA.PERFIL);
            Object alternativa = parametros.get(CHAVE_UTILIZADA.ALTERNATIVA);
            Object referencia = parametros.get(CHAVE_UTILIZADA.REFERENCIA);
            Object profPorNspt = parametros.get(CHAVE_UTILIZADA.PROFUNDIDADE_NSPT);
            Object listaSolos = parametros.get(CHAVE_UTILIZADA.SOLOS);
            if (perfil != null) {
                String perfilValor = (String) perfil;
                RepositoryProjetoDeEstacas.getInstance().recuperar().setPerfil(perfilValor);
            }
//            if (alternativa != null) {
//                int alternativaVAlor = (int) alternativa;
//                RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda()
//            }
            if (referencia != null) {
                String referenciaVAlor = (String) referencia;
                RepositoryProjetoDeEstacas.getInstance().recuperar().setRef(referenciaVAlor);
            }

            if (profPorNspt != null) {
                ArrayList<ItemDeEnsaio> profundidadePorNspt = (ArrayList<ItemDeEnsaio>) profPorNspt;
                if (profundidadePorNspt != null && !profundidadePorNspt.isEmpty()) {
                    FundacaoProfunda fundacaoProf = new FundacaoProfunda();
                    Terreno terreno = new Terreno();
                    terreno.setEnsaios(profundidadePorNspt);
                    if (listaSolos != null && listaSolos instanceof ArrayList) {
                        ArrayList<Solo> solos = (ArrayList<Solo>) listaSolos;
                        terreno.setSolos(solos);
                    }
                    fundacaoProf.setTerreno(terreno);
                    RepositoryProjetoDeEstacas.getInstance().novo(fundacaoProf);
                }

            }

        }
    }
}