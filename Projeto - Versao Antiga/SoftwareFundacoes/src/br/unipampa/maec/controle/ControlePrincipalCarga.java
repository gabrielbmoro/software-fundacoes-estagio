/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.JFrameInicial;
import br.unipampa.maec.apresentacao.carga.JFrameBaixarTemplate;
import br.unipampa.maec.apresentacao.carga.JFrameImportarDados;
import br.unipampa.maec.apresentacao.carga.JFramePrincipalCarga;
import br.unipampa.maec.apresentacao.carga.PanelTipoDeSolo;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * <b>Propósito:</b>
 * <br>
 * Classe responsável por configurar a interface gráfica {@link JFramePrincipalCarga}.
 * <br>
 * <b>Instruções de uso:</b>
 * <br>
 * Deverá ser utilizado como inicializador e configurador do panel ou jframe associado.
 *
 * @author GabrielBMoro
 * @version 0.4
 * @since 24/01/2014
 */
public class ControlePrincipalCarga implements Controller {
    
    private JFramePrincipalCarga jFramePrincipalCarga;
    private ArrayList<JPanel> vectorJPanels;
    public static int ITERADOR = -1;
    private Projeto projeto;
    private ControleImportarDados controlerImportarDados;
    private ControleObservador controleObservador;
    private ControleBaixarTemplate controleBaixaTemplate;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um jframe.
     *
     * @param jFramePrinciapal de tipo {@link JFramePrincipalCarga}
     */
    public ControlePrincipalCarga(JFramePrincipalCarga jFramePrinciapal) {
        this.jFramePrincipalCarga = jFramePrinciapal;
        this.jFramePrincipalCarga.addWindowListener(new TrataWindowListener());
        this.vectorJPanels = new ArrayList<>();
        this.projeto = RepositoryProjetoDeEstacas.getInstance().recuperar();
        this.controleObservador = new ControleObservador(this);
        preencherVector();
        registrarListeners();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por configurar os listeners respectivos da interface gráfica.
     */
    public void registrarListeners() {
        this.jFramePrincipalCarga.getjMenuItemInicio().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RepositoryProjetoDeEstacas.getInstance().alterar(null);
                jFramePrincipalCarga.dispose();
                new ControleInicial(new JFrameInicial());
            }
        });
        this.jFramePrincipalCarga.getBtnInicioBarraSistema().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RepositoryProjetoDeEstacas.getInstance().alterar(null);
                jFramePrincipalCarga.dispose();
                new ControleInicial(new JFrameInicial());
            }
        });
        this.jFramePrincipalCarga.getjMenuItemDesligar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean resposta = GeradorDeMensagem.exibirMensagemDeConfirmacao("Você realmente quer sair da aplicação?", "Alerta ao Usuário");
                if (resposta) {
                    System.exit(0);
                }
            }
        });
        this.jFramePrincipalCarga.getBtnAvancar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
            }
        });
        this.jFramePrincipalCarga.getjMenuItemAvancar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoAvancar();
            }
        });
        this.jFramePrincipalCarga.getjMenuItemVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
            }
        });
        this.jFramePrincipalCarga.getBtnVoltar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                registrarListenerBotaoVoltar();
            }
        });
        this.jFramePrincipalCarga.getjMenuItemImportarDados().addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                controlerImportarDados = new ControleImportarDados(new JFrameImportarDados());
            }
        });
        this.jFramePrincipalCarga.getBtnImportarPLanilhaSistema().addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                controlerImportarDados = new ControleImportarDados(new JFrameImportarDados());
            }
        });
        this.jFramePrincipalCarga.getjMenuItemBaixarTemplate().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controleBaixaTemplate = new ControleBaixarTemplate(new JFrameBaixarTemplate());
            }
        });
        this.jFramePrincipalCarga.getBtnBaixarTemplateDeImportacaoSistema().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controleBaixaTemplate = new ControleBaixarTemplate(new JFrameBaixarTemplate());
            }
        });
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por configurar os listeners para um determinado JPanel,
     * diferentemente do método anterior essa sobrecarga foi utilizada para aplicar a
     * mesma funcionalidade para outra situação.
     *
     * @param jPanel de tipo {@link JPanel}
     */
    public void registrarPossiveisParametros(JPanel jPanel) {
         if (jPanel instanceof PanelTipoDeSolo) {
            PanelTipoDeSolo panel = (PanelTipoDeSolo) jPanel;
            int de
                        = this.projeto.getFundacaoProfunda().getTerreno().getEnsaios().get(0).getProfundidade();
            int ate
                        = this.projeto.getFundacaoProfunda().getTerreno().getEnsaios().get(
                                    this.projeto.getFundacaoProfunda().getTerreno().getEnsaios().size() - 1).getProfundidade();
            this.jFramePrincipalCarga.setDe(de);
            this.jFramePrincipalCarga.setAte(ate);
         }
        
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por alimentar o vetor utilizado para armazenar os JPanels que o
     * JFrame principal conterá, por usar navegabilidade, tal técnica é necessária.
     */
    private void preencherVector() {
        this.vectorJPanels.add(this.jFramePrincipalCarga.getPanelIdentificacao1());
        this.vectorJPanels.add(this.jFramePrincipalCarga.getPanelProfundMax1());
        this.vectorJPanels.add(this.jFramePrincipalCarga.getPanelTipoDeSolo1());
        this.vectorJPanels.add(this.jFramePrincipalCarga.getPanelEscolhaEstaca1());
        this.vectorJPanels.add(this.jFramePrincipalCarga.getPanelFatoresDeSeguranca1());
        this.vectorJPanels.add(this.jFramePrincipalCarga.getjPanelGerarDocumento1());
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção voltar.
     */
    private void registrarListenerBotaoVoltar() {
        if (ITERADOR > 0) {
            ITERADOR -= 1;
            registrarPossiveisParametros(vectorJPanels.get(ITERADOR));
            jFramePrincipalCarga.getBtnVoltar().setEnabled(true);
            jFramePrincipalCarga.getjMenuItemVoltar().setEnabled(true);
            jFramePrincipalCarga.exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            System.err.println("Voltado: " + ITERADOR);
            jFramePrincipalCarga.getjProgressBar1().setValue(jFramePrincipalCarga.getjProgressBar1().getValue() - 14);
        }else{
            jFramePrincipalCarga.getBtnVoltar().setEnabled(false);
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para registrar o listener da opção avançar.
     */
    private void registrarListenerBotaoAvancar() {
        this.jFramePrincipalCarga.getjMenuItemImportarDados().setEnabled(false);
        if (ITERADOR <= vectorJPanels.size()) {
            ITERADOR += 1;
            registrarPossiveisParametros(vectorJPanels.get(ITERADOR));
            jFramePrincipalCarga.getBtnAvancar().setEnabled(true);
            jFramePrincipalCarga.getjMenuItemAvancar().setEnabled(true);
            jFramePrincipalCarga.exibirJPanelSelecionado(vectorJPanels.get(ITERADOR));
            System.err.println("Avançado: " + ITERADOR);
            jFramePrincipalCarga.getjProgressBar1().setValue(jFramePrincipalCarga.getjProgressBar1().getValue() + 14);
        }else{
            jFramePrincipalCarga.getBtnAvancar().setEnabled(false);
        }
    }
    
}
