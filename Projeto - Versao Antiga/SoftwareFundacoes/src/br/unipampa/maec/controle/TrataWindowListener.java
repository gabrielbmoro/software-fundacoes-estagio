/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * <b>Propósito:</b>
 * <br>
 * Classe responsável por implementar os listeners padrão de um JFrame.
 * <br>
 * <b>Instruções de uso:</b>
 * <br>
 * Deverá ser utilizado pelas classes de controle na atribuição do {@link WindowListener}.
 *
 * @author GabrielBMoro
 * @version 0.4
 * @since 24/01/2014
 */
public class TrataWindowListener implements WindowListener {

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        boolean resp = GeradorDeMensagem.exibirMensagemDeConfirmacao("Você deseja realmente sair da aplicação?", "Alerta ao Usuário");
        if (resp) {
            System.exit(0);
        } else {
            return;
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

}
