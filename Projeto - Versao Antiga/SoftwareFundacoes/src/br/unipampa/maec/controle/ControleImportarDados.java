/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.controle;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.apresentacao.carga.JFrameImportarDados;
import br.unipampa.maec.controle.Controller;
import br.unipampa.maec.servicos.ImportarArquivoExcel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author Gabriel B Moro
 */
public class ControleImportarDados implements Controller {

    private JFrameImportarDados jFrameImportarDados;
    private File diretorio;

    public ControleImportarDados(JFrameImportarDados jFrameImportarDados) {
        this.jFrameImportarDados = jFrameImportarDados;
//        this.jFrameImportarDados.addWindowListener(new TrataWindowListener());
        registrarListeners();
    }

    @Override
    public void registrarListeners() {
        this.jFrameImportarDados.getBtnPesquisarParaImportar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jFrameImportarDados.setjFileChooser(new JFileChooser());
                jFrameImportarDados.getjFileChooser().setFileSelectionMode(JFileChooser.FILES_ONLY);
                int resposta = jFrameImportarDados.getjFileChooser().showOpenDialog(null);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    diretorio = jFrameImportarDados.getjFileChooser().getSelectedFile();
                    GeradorDeMensagem.exibirMensagemDeInformacao("Você escolheu o diretório: " + diretorio.getName().toString(), "Alerta à Usuário");
                    jFrameImportarDados.getLblCaminhoDiretorio().setText(diretorio.getPath().toString());
                } else {
                    GeradorDeMensagem.exibirMensagemDeInformacao("Você não selecionou nenhum diretório", "Alerta à Usuário");
                }
            }
        });
        this.jFrameImportarDados.getBtnVerificarCompatibilidade().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ImportarArquivoExcel importarArquivoExcel = new ImportarArquivoExcel();
                diretorio = new File(jFrameImportarDados.getLblCaminhoDiretorio().getText());
                if (diretorio.isFile() && diretorio.getAbsolutePath().contains(".xls") && importarArquivoExcel.verificarCompatibilidadeDoFormatoDoArquivo(diretorio)) {
                    jFrameImportarDados.configurarCompatibilidadeDoArquivo(true);
                    jFrameImportarDados.getBtnImportar().setEnabled(true);
                } else {
                    jFrameImportarDados.configurarCompatibilidadeDoArquivo(false);
                }
            }
        });
        this.jFrameImportarDados.getBtnImportar().addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (diretorio != null) {
                    ImportarArquivoExcel importarArquivoExcel = new ImportarArquivoExcel();
                    importarArquivoExcel.parametrosImportados(diretorio);
                    jFrameImportarDados.dispose();
                }
            }
        });
    }
 
}
