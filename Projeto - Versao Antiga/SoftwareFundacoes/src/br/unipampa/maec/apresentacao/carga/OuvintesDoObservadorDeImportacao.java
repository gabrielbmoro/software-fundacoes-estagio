/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.apresentacao.carga;

import br.unipampa.maec.modelo.Projeto;

/**
 *
 * @author Gabriel B Moro
 */
public interface OuvintesDoObservadorDeImportacao {
    
    public void restauraDadosDeInterface(Projeto projeto);
}
