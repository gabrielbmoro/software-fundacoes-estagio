/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.apresentacao;

/**
 *
 * @author Gabriel B Moro
 */
public interface OperarPanels {
    
    public void habilitarComponentes();
    
    public void desabilitarComponentes();
}
