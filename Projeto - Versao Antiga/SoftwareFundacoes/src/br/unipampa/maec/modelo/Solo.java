package br.unipampa.maec.modelo;

/**
 * <b>Propósito:</b>
 * <br>
 * Representar a camada de solo, a qual compõe um terreno (de tipo {@link Terreno}). De
 * acordo com a profundidade a camada de solo é instanciada.
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link FundacaoProfunda} indiretamente via
 * {@link Terreno} que possui várias camadas de solo agrupadas em uma lista.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Solo {

    private double alfa;
    private TipoDeSolo tipoDeSolo;
    private int k;
    private double de, ate;
    private double alfaDaCategoria;
    private double betaDaCategoria;
    private int CDaCategoria;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um objeto de tipo solo com um estado definido.
     *
     * @param tipoDeSolo de tipo {@link TipoDeSolo}
     * @param de de tipo {@link Double}
     * @param ate de tipo {@link Double}
     */
    public Solo(TipoDeSolo tipoDeSolo, double de, double ate) {
        this.tipoDeSolo = tipoDeSolo;
        this.de = de;
        this.ate = ate;
        configurarValores();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um objeto Solo com estado definido
     * diferentemente do método anterior, os quais são utilizados de acordo com a
     * necessidade do programador.
     *
     * @param alfa de tipo {@link Double}
     * @param tipoDeSolo de tipo {@link TipoDeSolo}
     * @param k de tipo {@link Integer}
     */
    public Solo(double alfa, TipoDeSolo tipoDeSolo, int k) {
        this.alfa = alfa;
        this.tipoDeSolo = tipoDeSolo;
        this.k = k;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por configurar os valores padrão de acordo com o tipo de solo.
     */
    private void configurarValores() {
        switch (this.tipoDeSolo) {
            case AREIA:
                this.k = 100;
                this.alfa = 1.4;
                break;
            case AREIA_SITOSA:
                this.k = 80;
                this.alfa = 2;
                break;
            case AREIA_SILTO_ARGILOSA:
                this.k = 70;
                this.alfa = 2.4;
                break;
            case AREIA_ARGILOSA:
                this.k = 60;
                this.alfa = 3;
                break;
            case AREIA_ARGILO_SILTOSA:
                this.k = 50;
                this.alfa = 2.8;
                break;
            case SILTE:
                this.k = 40;
                this.alfa = 3;
                break;
            case SILTE_ARENOSO:
                this.k = 55;
                this.alfa = 2.2;
                break;
            case SILTE_ARENOSO_ARGILOSO:
                this.k = 45;
                this.alfa = 2.8;
                break;
            case SILTE_ARGILOSO:
                this.k = 23;
                this.alfa = 3.4;
                break;
            case SILTE_ARGILOSO_ARENOSO:
                this.k = 25;
                this.alfa = 3;
                break;
            case ARGILA:
                this.k = 20;
                this.alfa = 6;
                break;
            case ARGILA_ARENOSA:
                this.k = 35;
                this.alfa = 2.4;
                break;
            case ARGILA_ARENO_SILTOSA:
                this.k = 30;
                this.alfa = 2.8;
                break;
            case ARGILA_SILTOSA:
                this.k = 22;
                this.alfa = 4;
                break;
            case ARGILA_SILO_ARENOSA:
                this.k = 33;
                this.alfa = 3;
                break;
        }
    }

    public void configurarValoresPorEstaca(Estaca estaca) {
        if (estaca.getDimensaoDefinida() <= 25) {
            alfaDaCategoria = 1;
            betaDaCategoria = 3;
        } else {
            if (tipoDeSolo == TipoDeSolo.ARGILA
                        || tipoDeSolo == TipoDeSolo.ARGILA_ARENOSA
                        || tipoDeSolo == TipoDeSolo.ARGILA_ARENO_SILTOSA
                        || tipoDeSolo == TipoDeSolo.ARGILA_SILO_ARENOSA
                        || tipoDeSolo == TipoDeSolo.ARGILA_SILTOSA) {
                this.CDaCategoria = 12;
                if (estaca.getTipoDaEstaca()==TipoEstaca.HELICE_CONTINUA) {
                    this.alfaDaCategoria = 0.3;
                    this.betaDaCategoria = 1;
                } else if (estaca.getTipoDaEstaca()==TipoEstaca.RAIZ) {
                    this.alfaDaCategoria = 0.85;
                    this.betaDaCategoria = 1.5;
                }else if(estaca.getTipoDaEstaca()==TipoEstaca.ROTATIVA){
                    if(estaca.isLama()){
                        alfaDaCategoria= 0.85;
                        betaDaCategoria= 0.9;
                    }else{
                        alfaDaCategoria= 0.85;
                        betaDaCategoria= 0.8;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
            if (tipoDeSolo == TipoDeSolo.SILTE_ARGILOSO) {
                this.CDaCategoria = 20;
            }
            if (tipoDeSolo == tipoDeSolo.SILTE_ARENOSO) {
                this.CDaCategoria = 25;
            }
            if (tipoDeSolo == TipoDeSolo.AREIA
                        || tipoDeSolo == TipoDeSolo.AREIA_ARGILOSA
                        || tipoDeSolo == TipoDeSolo.AREIA_ARGILO_SILTOSA
                        || tipoDeSolo == TipoDeSolo.AREIA_SILTO_ARGILOSA
                        || tipoDeSolo == TipoDeSolo.AREIA_SITOSA) {
                this.CDaCategoria = 40;
                if (estaca.getTipoDaEstaca()==TipoEstaca.HELICE_CONTINUA) {
                    this.alfaDaCategoria = 0.3;
                    this.betaDaCategoria = 1;
                } else if (estaca.getTipoDaEstaca()==TipoEstaca.RAIZ) {
                    this.alfaDaCategoria = 0.5;
                    this.betaDaCategoria = 1.5;
                }else if(estaca.getTipoDaEstaca()==TipoEstaca.ROTATIVA){
                    if(estaca.isLama()){
                        alfaDaCategoria= 0.5;
                        betaDaCategoria= 0.6;
                    }else{
                        alfaDaCategoria=0.5;
                        betaDaCategoria= 0.5;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
            if (tipoDeSolo == TipoDeSolo.SILTE
                        || tipoDeSolo == TipoDeSolo.SILTE_ARENOSO
                        || tipoDeSolo == TipoDeSolo.SILTE_ARENOSO_ARGILOSO
                        || tipoDeSolo == TipoDeSolo.SILTE_ARGILOSO
                        || tipoDeSolo == TipoDeSolo.SILTE_ARGILOSO_ARENOSO) {
             this.CDaCategoria=20;
                if (estaca.getTipoDaEstaca()==TipoEstaca.HELICE_CONTINUA) {
                    alfaDaCategoria = 0.3;
                    betaDaCategoria = 1;
                } else if (estaca.getTipoDaEstaca()==TipoEstaca.RAIZ) {
                    alfaDaCategoria = 0.6;
                    betaDaCategoria = 1.5;
                }else if(estaca.getTipoDaEstaca()==TipoEstaca.ROTATIVA){
                    if(estaca.isLama()){
                        alfaDaCategoria= 0.6;
                        betaDaCategoria= 0.75;
                    }else{
                        alfaDaCategoria= 0.6;
                        betaDaCategoria= 0.65;
                    }
                }
                //Falta para estaca escavada e microestaca
            }
        }
    }

    /**
     * @return the alfa
     */
    public double getAlfa() {
        return alfa;
    }

    /**
     * @return the tipoDeSolo
     */
    public TipoDeSolo getTipoDeSolo() {
        return tipoDeSolo;
    }

    /**
     * @return the k
     */
    public int getK() {
        return k;
    }

    /**
     * @return the ate
     */
    public double getAte() {
        return this.ate;
    }

    /**
     * @return the de
     */
    public double getDe() {
        return this.de;
    }

    public double getAlfaDaCategoria() {
        return alfaDaCategoria;
    }

    public void setAlfaDaCategoria(double alfaDaCategoria) {
        this.alfaDaCategoria = alfaDaCategoria;
    }

    public double getBetaDaCategoria() {
        return betaDaCategoria;
    }

    public void setBetaDaCategoria(double betaDaCategoria) {
        this.betaDaCategoria = betaDaCategoria;
    }

    public int getCDaCategoria() {
        return CDaCategoria;
    }

    public void setCDaCategoria(int kDaCategoria) {
        this.CDaCategoria = kDaCategoria;
    }
    
    
}
