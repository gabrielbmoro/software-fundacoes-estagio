package br.unipampa.maec.modelo;

import java.util.ArrayList;

/**
 * <b>Propósito:</b>
 * <br>
 * Abstrair o estado e comportamento de uma sapata, a qual é utilizada em fundações rasas
 * ({@link FundacaoRasa} para ser sustentada sobre pilares (tipo {@link Pilar}).
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link FundacaoRasa} para a definição de estado em
 * memória.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Sapata {

    private double tensaoAdmissivel;
    private double comprimentoMedioEsperado;
    private ArrayList<Pilar> pilares;

    /**
     * <b>Propósito:</b>
     * <br>
     * Responsável por inicializar um objeto de tipo Sapata.
     * <br>
     */
    public Sapata() {
        this.pilares = new ArrayList<>();
    }

    /**
     * @return the tensaoAdmissivel
     */
    public double getTensaoAdmissivel() {
        return tensaoAdmissivel;
    }

    /**
     * @param tensaoAdmissivel the tensaoAdmissivel to set
     */
    public void setTensaoAdmissivel(double tensaoAdmissivel) {
        this.tensaoAdmissivel = tensaoAdmissivel;
    }

    /**
     * @return the comprimentoMedioEsperado
     */
    public double getComprimentoMedioEsperado() {
        return comprimentoMedioEsperado;
    }

    /**
     * @param comprimentoMedioEsperado the comprimentoMedioEsperado to set
     */
    public void setComprimentoMedioEsperado(double comprimentoMedioEsperado) {
        this.comprimentoMedioEsperado = comprimentoMedioEsperado;
    }

    /**
     * @return the pilares
     */
    public ArrayList<Pilar> getPilares() {
        return pilares;
    }

    /**
     * @param pilares the pilares to set
     */
    public void setPilares(ArrayList<Pilar> pilares) {
        this.pilares = pilares;
    }
}
