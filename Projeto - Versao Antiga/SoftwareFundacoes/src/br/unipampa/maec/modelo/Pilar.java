package br.unipampa.maec.modelo;

/**
 * <b>Propósito:</b>
 * <br>
 * Abstrair de maneira geral as propriedades de um pilar, que é utilizado para
 * {@link FundacaoRasa}.
 * <br>
 * <b>Instruções de uso:</b>
 * Essa classe deve ser utilizada para projetos de {@link FundacaoRasa}, atualmente essa
 * classe está construída com objetivo de prever um módulo de cálculo para fundação rasa.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Pilar {
    
    private int identificador;
    private int carga;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um objeto de tipo pilar, que possui um estado
     * definido.
     *
     * @param carga de tipo {@link Double}
     */
    public Pilar(int id, int carga) {
        this.identificador = id;
        this.carga = carga;
    }

    /**
     * @return the carga
     */
    public int getCarga() {
        return carga;
    }

    /**
     * @param carga the carga to set
     */
    public void setCarga(int carga) {
        this.carga = carga;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
    
    
}
