package br.unipampa.maec.modelo;
/**
 * <b>Propósito:</b>
 * <br>
 * Deve ser utilizado para representar o tipo de bloco ({@link Bloco}), que muda de acordo com 
 * quantidade de pilares.
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link Bloco}.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public enum TipoBloco {

    SIMPLES, RETANGULAR, TRIANGULAR;
	 	 
}
 
