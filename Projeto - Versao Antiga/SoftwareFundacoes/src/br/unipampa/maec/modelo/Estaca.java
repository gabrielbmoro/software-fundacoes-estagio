package br.unipampa.maec.modelo;

import java.util.ArrayList;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;

/**
 * <b>Propósito:</b>
 * <br>
 * Essa classe deve ser utilizada para abstrair os objetos gerais estacas, dentre as suas
 * propriedades um objeto de tipo estaca possui uma dimensão, atrito lateral e resistencia
 * de ponta.
 *
 * <br>
 * <b>Instruções de uso:</b>
 * <br>
 *
 * Essa classe deve ser utilizada indiretamente pela classe {@link Projeto} que manipulará
 * diretamente a classe {@link Fundacao} (que possui várias Estacas).
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Estaca {

    private int dimensaoDefinida;
    private double fs_atritoLateral;
    private double fs_resistenciaDePonta;
    private int capacidadeDeCarga;
    private double CONST_F1;
    private double CONST_F2;
    private ArrayList<Integer> dimensoesDisponiveis;
    private RepositoryProjetoDeEstacas repositorio;
    private boolean lama;
    private double volume;
    private int altura;
    private TipoEstaca tipoDaEstaca;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método construtor que inicializa o repositorio de projeto de estacas.
     */
    public Estaca(TipoEstaca tipoEstacaTemp) {
        this.repositorio = RepositoryProjetoDeEstacas.getInstance();
        this.lama = false;
        this.tipoDaEstaca = tipoEstacaTemp;
        configurarFatoresDaEstaca();
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método utilizada para inicializar um objeto estaca com uma dimensão definida.
     *
     * @param dimensaoDefinida de tipo {@link Integer}
     */
    public Estaca(int dimensaoDefinida, TipoEstaca tipoEstacaTemp) {
        this.dimensaoDefinida = dimensaoDefinida;
        this.tipoDaEstaca = tipoEstacaTemp;
        configurarFatoresDaEstaca();
        this.repositorio = RepositoryProjetoDeEstacas.getInstance();
    }
    
    private void configurarFatoresDaEstaca(){
        //  FRANKI, HELICE_CONTINUA, PRE_MOLDADA, RAIZ, ROTATIVA, STRAUS, TRILHO;
        switch(this.tipoDaEstaca){
            case FRANKI:
                CONST_F1 = 2.5;
                CONST_F2 = 5.0;
                break;
            case HELICE_CONTINUA:
                CONST_F1 = 2.0;
                CONST_F2 = 4.0;
                break;
            case PRE_MOLDADA_CIRCULAR:
                CONST_F1 = (1+ (((double) dimensaoDefinida)/100))/0.8;
                CONST_F2 = 2.0 * CONST_F1;
                break;
            case RAIZ:
                CONST_F1= 2.0;
                CONST_F2= 4.0;
                break;
            case ROTATIVA:
                CONST_F1=3.0;
                CONST_F2=6.0;
                break;
            case STRAUS://A mudar
                CONST_F1=4.2;
                CONST_F2=3.9;
                break;
            case TRILHO:
                CONST_F1=1.75;
                CONST_F2=3.5;
                break;
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar as estacas com dimensões em seus respectivos
     * blocos (de tipo {@link Bloco}).
     *
     * @param estaca que será cadastrada de tipo {@link Estaca}
     * @return de tipo {@link Boolean}
     */
    public boolean registrarDimensoes(Estaca estaca) {
        this.repositorio = RepositoryProjetoDeEstacas.getInstance();
        Projeto projeto = this.repositorio.recuperar();
        ArrayList<Bloco> blocos = new ArrayList<>();
        blocos.add(new Bloco(0.0, 0.0));
        projeto.getFundacaoProfunda().setBlocos(blocos);

        ArrayList<Estaca> estacas = new ArrayList<>();

        for (int count = 0; count < this.dimensoesDisponiveis.size(); count++) {
            int dimensaoVolume = this.dimensoesDisponiveis.get(count);
            estacas.add(new Estaca(dimensaoVolume, tipoDaEstaca));
        }
        projeto.getFundacaoProfunda().getBlocos().get(0).setEstacas(estacas);
        if (projeto.getFundacaoProfunda().getBlocos().get(0).getEstacas() != null) {
            return true;
        } else {
            return false;
        }
    }

    public void configurarVolume(int numeroDeEstacas) {
        if (dimensaoDefinida != 0 && altura != 0 && numeroDeEstacas != 0) {
            this.volume = (RepositoryProjetoDeEstacas.PI * (dimensaoDefinida * dimensaoDefinida)) / 4 * altura * numeroDeEstacas;
        }
    }

    /**
     * @return the dimensaoDefinida
     */
    public RepositoryProjetoDeEstacas getRepositorio() {
        return repositorio;
    }

    /**
     *
     * @param repositorio the volume to set
     */
    public void setRepositorio(RepositoryProjetoDeEstacas repositorio) {
        this.repositorio = repositorio;
    }

    /**
     * @return the dimensaoDefinida
     */
    public int getDimensaoDefinida() {
        return dimensaoDefinida;
    }

    /**
     * @param dimensaoDefinida the volume to set
     */
    public void setDimensaoDefinida(int dimensaoDefinida) {
        this.dimensaoDefinida = dimensaoDefinida;
    }

    /**
     * @return the fs_atritoLateral
     */
    public double getFs_atritoLateral() {
        return fs_atritoLateral;
    }

    /**
     * @param fs_atritoLateral the fs_atritoLateral to set
     */
    public void setFs_atritoLateral(double fs_atritoLateral) {
        this.fs_atritoLateral = fs_atritoLateral;
    }

    /**
     * @return the fs_resistenciaDePonta
     */
    public double getFs_resistenciaDePonta() {
        return fs_resistenciaDePonta;
    }

    /**
     * @param fs_resistenciaDePonta the fs_resistenciaDePonta to set
     */
    public void setFs_resistenciaDePonta(double fs_resistenciaDePonta) {
        this.fs_resistenciaDePonta = fs_resistenciaDePonta;
    }

    /**
     * @return the CONST_F1
     */
    public double getCONST_F1() {
        return CONST_F1;
    }

    /**
     * @return the CONST_F2
     */
    public double getCONST_F2() {
        return CONST_F2;
    }

    /**
     * @return the dimensoesDisponiveis
     */
    public ArrayList<Integer> getDimensoesDisponiveis() {
        return dimensoesDisponiveis;
    }

    /**
     * @param dimensoesDisponiveis the dimensoesDisponiveis to set
     */
    public void setDimensoesDisponiveis(ArrayList<Integer> dimensoesDisponiveis) {
        this.dimensoesDisponiveis = dimensoesDisponiveis;
    }

    public boolean isLama() {
        return lama;
    }

    public void setLama(boolean lama) {
        this.lama = lama;
    }

    public int getCapacidadeDeCarga() {
        return capacidadeDeCarga;
    }

    public void setCapacidadeDeCarga(int capacidadeDeCarga) {
        this.capacidadeDeCarga = capacidadeDeCarga;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public TipoEstaca getTipoDaEstaca() {
        return tipoDaEstaca;
    }

    public void setTipoDaEstaca(TipoEstaca tipoDaEstaca) {
        this.tipoDaEstaca = tipoDaEstaca;
    }


}
