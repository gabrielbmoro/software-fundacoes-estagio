package br.unipampa.maec.modelo;
/**
 * <b>Propósito:</b>
 * <br>
 * Deve ser utilizado para representar o tipo de solo ({@link Solo}), que muda de acordo com 
 * quantidade a profundidade do terreno.
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link Solo}.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public enum TipoDeSolo {

    AREIA, AREIA_SITOSA, AREIA_SILTO_ARGILOSA,
    AREIA_ARGILOSA, AREIA_ARGILO_SILTOSA, SILTE,
    SILTE_ARENOSO, SILTE_ARENOSO_ARGILOSO, SILTE_ARGILOSO,
    SILTE_ARGILOSO_ARENOSO, ARGILA, ARGILA_ARENOSA,
    ARGILA_ARENO_SILTOSA, ARGILA_SILTOSA, ARGILA_SILO_ARENOSA;

}
