package br.unipampa.maec.modelo;

import java.util.ArrayList;

/**
 * <b>Propósito:</b>
 * <br>
 * Essa classe deve ser utilizada como abstração de um bloco de construção, o qual contém
 * volume, excentricidade e possui seu tipo variado de acordo com a quantidade de estacas.
 * <br>
 * <b>Instruções de uso:</b>
 * <br>
 * Deve ser utilizado pelas classes de Tipo {@link Fundacao}.
 *
 * @author GabrielBMoro
 * @version 0.4
 * @since 24/01/2014
 */
public class Bloco {

    private TipoBloco tipoBloco;
    private double excentricidade;
    private double volume;
    private ArrayList<Estaca> estacas;
    protected double altura;

    /**
     * <b>Propósito:</b>
     * <br>
     * O método construtor tem o objetivo de inicializar um bloco com excentricidade e
     * volume.
     *
     * @param excentricidade é o parâmetro de tipo {@link Double}.
     * @param volume é o parâmetro de tipo {@link Double} e deve ser utilizado para
     * calcular a excentricidade do bloco.
     */
    public Bloco(double excentricidade, double volume) {
        this.estacas = new ArrayList<>();
    }

    public Bloco() {
        this.estacas = new ArrayList<>();
    }

    /**
     * @return the tipoBloco
     */
    public TipoBloco getTipoBloco() {
        return tipoBloco;
    }

    /**
     * @param tipoBloco the tipoBloco to set
     */
    public void setTipoBloco(TipoBloco tipoBloco) {
        this.tipoBloco = tipoBloco;
    }

    /**
     * @return the excentricidade
     */
    public double getExcentricidade() {
        return excentricidade;
    }

    /**
     * @param excentricidade the excentricidade to set
     */
    public void setExcentricidade(double excentricidade) {
        this.excentricidade = excentricidade;
    }

    /**
     * @return the volume
     */
    public double getVolume() {
        return volume;
    }

    /**
     * @param volume the volume to set
     */
    public void setVolume(double volume) {
        this.volume = volume;
    }

    /**
     * @return the estacas
     */
    public ArrayList<Estaca> getEstacas() {
        return estacas;
    }

    /**
     * @param estacas the estacas to set
     */
    public void setEstacas(ArrayList<Estaca> estacas) {
        this.estacas = estacas;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    

}
