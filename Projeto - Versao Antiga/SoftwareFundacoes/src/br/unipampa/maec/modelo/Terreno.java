/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo;

import java.util.ArrayList;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;

/**
 * <b>Propósito:</b>
 * <br>
 * Representar o terreno em fundações profundas e rasas, o qual possui itens de ensaios e 
 * diferentes camadas de solos (de tipo {@link Solo}) associadas.
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizada pela classe {@link FundacaoProfunda} ou {@link FundacaoRasa}
 *  de acordo com a necessidade.
 * Ocorrerá a necessidade de utilizá-la na classe {@link Projeto} para a recuperação dos resultados
 *  ou dados de terreno.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Terreno {

    private ArrayList<ItemDeEnsaio> ensaios;
    private RepositoryProjetoDeEstacas repository;
    private ArrayList<Solo> solos;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um objeto de tipo Terreno, com um estado definido.
     */
    public Terreno() {
        this.solos = new ArrayList<>();
    }
    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por inicializar um objeto que possui um conjunto definido de nspt
     * @param nspt de tipo {@link ArrayList}
     */
    public Terreno(ArrayList<Integer> nspt) {
        preencherProfundidade(nspt);
    }
    
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por preencher a profundidade de acordo com os nspts definidos, 
 * ou seja, 
 * para cada nspt terá uma profundidade em sequência associada.
 * @param nspt de tipo {@link Integer}
 */
    public void preencherProfundidade(ArrayList<Integer> nspt) {
        int count = 0,profund=1;
        this.ensaios = new ArrayList<>();
        do {
            this.ensaios.add(new ItemDeEnsaio(profund, nspt.get(count)));
            count++;
            profund++;
        } while (count < nspt.size());
    }
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por definir os dados de tipo de solo em memória, tais dados são 
 * transmitidos pelo módulo controle oriundos do módulo de apresentação.
 * @param dados de tipo {@link ArrayList}
 * @return {@link Boolean}
 */
    public boolean definirDadosTipoDeSolo(ArrayList<Object> dados) {
        this.repository = RepositoryProjetoDeEstacas.getInstance();
        Projeto projeto = this.repository.recuperar();
        FundacaoProfunda fundacao = (FundacaoProfunda) projeto.getFundacaoProfunda();
        int profundMax = fundacao.getTerreno().getEnsaios().get(
                fundacao.getTerreno().getEnsaios().size() - 1
        ).getProfundidade();
        for (int count = 0; count < dados.size(); count++) {
            if (count != (dados.size() - 1)) {
                if (dados.get(count) instanceof Double && dados.get(count + 1) instanceof Double) {
                    if (((Double) dados.get(count) < (Double) dados.get(count + 1)) && (Double) dados.get(count) <= profundMax) {
                        continue;
                    } else {
                        return false;
                    }
                } else {
                    continue;
                }
            }
        }

        this.solos = refatorarVectorProfundTipoSolo(dados);
        fundacao.getTerreno().setSolos(this.solos);
        return true;
    }
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por recuperar os dados tratados da memória para o módulo 
 * de apresentação, via módulo controle.
 * @return de tipo {@link ArrayList}
 */
    public ArrayList<Object> getDadosTipoSOlosPorMetrosForInterface() {
        this.repository = RepositoryProjetoDeEstacas.getInstance();
        Projeto projeto = this.repository.recuperar();
        Terreno terreno = projeto.getFundacaoProfunda().getTerreno();
        this.solos = terreno.getSolos();

        ArrayList<Object> temp = refatorarInversoVectorProfundTipoSolo();
        return temp;
    }
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por realizar a conversão de dados com tipagem para dados generalizados 
 * como tipo {@link Object}.
 * @return de tipo {@link ArrayList}
 */
    private ArrayList<Object> refatorarInversoVectorProfundTipoSolo() {
        ArrayList<Object> temp = new ArrayList<>();
if(this.solos!=null){
        for (int count = 0; count < this.solos.size(); count++) {
            Solo solo = this.solos.get(count);
            TipoDeSolo tipo = solo.getTipoDeSolo();
            switch (tipo) {
                case AREIA:
                    temp.add("Areia");
                    break;
                case AREIA_SITOSA:
                    temp.add("Areia Siltosa");
                    break;
                case AREIA_SILTO_ARGILOSA:
                    temp.add("Areia Silto Argilosa");
                    break;
                case AREIA_ARGILOSA:
                    temp.add("Areia Argilosa");
                    break;
                case AREIA_ARGILO_SILTOSA:
                    temp.add("Areia Argilosa Siltosa");
                    break;
                case SILTE:
                    temp.add("Silte");
                    break;
                case SILTE_ARENOSO:
                    temp.add("Silte Arenoso");
                    break;
                case SILTE_ARENOSO_ARGILOSO:
                    temp.add("Silte Arenoso Argiloso");
                    break;
                case SILTE_ARGILOSO:
                    temp.add("Silte Argiloso");
                    break;
                case SILTE_ARGILOSO_ARENOSO:
                    temp.add("Silte Argiloso Arenoso");
                    break;
                case ARGILA:
                    temp.add("Argila");
                    break;
                case ARGILA_ARENOSA:
                    temp.add("Argila Arenosa");
                    break;
                case ARGILA_ARENO_SILTOSA:
                    temp.add("Argila Areno Siltosa");
                    break;
                case ARGILA_SILTOSA:
                    temp.add("Argila Siltosa");
                    break;
                case ARGILA_SILO_ARENOSA:
                    temp.add("Argila Silto Arenosa");
                    break;
            }
            temp.add(solo.getDe());
            temp.add(solo.getAte());
        }
}
        return temp;
    }
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por converter uma lista de dados de tipo {@link ArrayList} que é composta 
 * por tipo {@link Object}, para um lista ({@link ArrayList} de objetos de tipo {@link Solo}
 * @param temp de tipo {@link ArrayList}
 * @return de tipo {@link ArrayList}
 */
    private ArrayList<Solo> refatorarVectorProfundTipoSolo(ArrayList<Object> temp) {
        ArrayList<Solo> solos1 = new ArrayList<>();
        for (int count = 0; count < temp.size(); count++) {
            String tipo = (String) temp.get(count);
            TipoDeSolo tipoTemp = null;
            double ateTemp = 0;
            double deTemp = 0;

            switch (tipo) {
                case "Areia":
                    tipoTemp = TipoDeSolo.AREIA;
                    break;
                case "Areia Siltosa":
                    tipoTemp = TipoDeSolo.AREIA_SITOSA;
                    break;
                case "Areia Silto Argilosa":
                    tipoTemp = TipoDeSolo.AREIA_SILTO_ARGILOSA;
                    break;
                case "Areia Argilosa":
                    tipoTemp = TipoDeSolo.AREIA_ARGILOSA;
                    break;
                case "Areia Argilosa Siltosa":
                    tipoTemp = TipoDeSolo.AREIA_ARGILO_SILTOSA;
                    break;
                case "Silte":
                    tipoTemp = TipoDeSolo.SILTE;
                    break;
                case "Silte Arenoso":
                    tipoTemp = TipoDeSolo.SILTE_ARENOSO;
                    break;
                case "Silte Areno Argiloso":
                    tipoTemp = TipoDeSolo.SILTE_ARENOSO_ARGILOSO;
                    break;
                case "Silte Argiloso":
                    tipoTemp = TipoDeSolo.SILTE_ARGILOSO;
                    break;
                case "Silte Argiloso Arenoso":
                    tipoTemp = TipoDeSolo.SILTE_ARGILOSO_ARENOSO;
                    break;
                case "Argila":
                    tipoTemp = TipoDeSolo.ARGILA;
                    break;
                case "Argila Arenosa":
                    tipoTemp = TipoDeSolo.ARGILA_ARENOSA;
                    break;
                case "Argila Areno Siltosa":
                    tipoTemp = TipoDeSolo.ARGILA_ARENO_SILTOSA;
                    break;
                case "Argila Siltosa":
                    tipoTemp = TipoDeSolo.ARGILA_SILTOSA;
                    break;
                case "Argila Silto Arenosa":
                    tipoTemp = TipoDeSolo.ARGILA_SILO_ARENOSA;
                    break;
            }
            ateTemp = (double) temp.get(count + 2);
            deTemp = (double) temp.get(count + 1);

            solos1.add(new Solo(tipoTemp, deTemp, ateTemp));
            count += 2;
        }
        return solos1;
    }
/**
 * <b>Propósito:</b>
 * <br>
 * Método responsável por recuperar o nspt médio da camada, o qual é calculado de acordo com a profundidade inicial e final.
 * @param de de tipo {@link Integer}
 * @param ate de tipo {@link Integer}
 * @return de tipo {@link Double}
 */
    public double getNsptMedioDaCamada(int de, int ate) {
        int indiceDe = 0, indiceAte = 0, denominador = 0;
        double nm = 0.0;

        for (int count = 0; count < this.ensaios.size(); count++) {
            if (this.ensaios.get(count).getProfundidade() == de) {
                indiceDe = count;
            } else if (this.ensaios.get(count).getProfundidade() == ate) {
                indiceAte = count;
            }
            if (count >= indiceDe && count <= indiceAte) {
                nm = +this.ensaios.get(count).getNspt();
                denominador++;
            }
        }
        return (nm / denominador);
    }
    /**
     * 
     * @return the solos
     */
    public ArrayList<Solo> getSolos() {
        return this.solos;
    }
/**
 * 
 * @return the ensaios
 */
    public ArrayList<ItemDeEnsaio> getEnsaios() {
        return ensaios;
    }
/**
 * 
 * @param ensaios the ensaios to set
 */
    public void setEnsaios(ArrayList<ItemDeEnsaio> ensaios) {
        this.ensaios = ensaios;
    }
/**
 * 
 * @return the repository
 */
    public RepositoryProjetoDeEstacas getRepository() {
        return repository;
    }
/**
 * 
 * @param repository to set repository
 */
    public void setRepository(RepositoryProjetoDeEstacas repository) {
        this.repository = repository;
    }
/**
 * 
 * @param solos to set solos
 */
    public void setSolos(ArrayList<Solo> solos) {
        this.solos = solos;
    }

}
