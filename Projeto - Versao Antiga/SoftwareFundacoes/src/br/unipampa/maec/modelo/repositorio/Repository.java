/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo.repositorio;


/**
 * <b>Propósito:</b>
 * <br>
 * A interface Repository é utilizada para garantir que os métodos da camada repositorio
 * serão implementados. Como nesse tipo de software não faz-se necessário uma comunicação
 * com banco de dados, a informação carregada na memória deve ser gerenciada por esse
 * padrão arquitetural, o qual é indicado para a gestão de coleções de objetos.
 * <br>
 * <b>Instruções de uso:</b>
 * Essa interface deve ser implementada pelas classes de tipo repositório.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public interface Repository {
    /**
     * <b>Propósito:</b>
     * <br>
     * Esse método deve ser utilizadao para o registro de dados na memória.
     *
     * @param objeto que pode ser de qualquer tipo
     * @return de tipo {@link Boolean}
     */
    public abstract boolean novo(Object objeto);

    /**
     * <b>Propósito</b>
     * <br>
     * Método que deve ser utilizado para alterar registros já cadastrados.
     *
     * @param objeto de tipo {@link Object}
     * @return de tipo {@link Boolean}
     */
    public abstract boolean alterar(Object objeto);

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para recuperar coleções de objeto ou apenas o único objeto
     * cadastrado na memória.
     *
     * @return de {@link Object}
     */
    public abstract Object recuperar();

    /**
     * <b>Propósito:</b>
     * <br>
     * Método que deve ser utilizado para a exclusão de coleções de objetos da memória.
     *
     * @return
     */
    public abstract boolean deletar();
}
