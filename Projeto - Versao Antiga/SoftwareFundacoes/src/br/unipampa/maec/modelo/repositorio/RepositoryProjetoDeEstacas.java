package br.unipampa.maec.modelo.repositorio;

import br.unipampa.maec.modelo.FundacaoProfunda;
import br.unipampa.maec.modelo.Projeto;
import br.unipampa.maec.modelo.Terreno;

/**
 * <b>Propósito:</b>
 * <br>
 * A interface Repository é utilizada para garantir que os métodos da camada repositorio
 * serão implementados. Como nesse tipo de software não faz-se necessário uma comunicação
 * com banco de dados, a informação carregada na memória deve ser gerenciada por esse
 * padrão arquitetural, o qual é indicado para a gestão de coleções de objetos.
 * <br>
 * <b>Instruções de uso:</b>
 * Essa interface deve ser implementada pelas classes de tipo repositório.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class RepositoryProjetoDeEstacas implements Repository {

    private Projeto projeto;
    private static RepositoryProjetoDeEstacas repositorio;
    /*INICIALIZAÇÃO DE CONSTANTES*/
    public static final double PI = 3.14;
//    public static double F1 = 3;
//    public static double F2 = 6;
    public static double TENSAO_CALCULO_CONCRETO = 60;
    public static final double FS_GLOBAL = 2.5;
    public static final double FS_LOCAL_PONTA = 4.0;
    public static final double FS_LOCAL_ATRITO = 2.0;

    private RepositoryProjetoDeEstacas() {
        this.projeto = new Projeto();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por recuperar a única instância do repositório.
     *
     * @return de tipo {@link RepositoryProjetoDeEstacas}
     */
    public static RepositoryProjetoDeEstacas getInstance() {
        if (repositorio == null) {
            repositorio = new RepositoryProjetoDeEstacas();
            return repositorio;
        } else {
            return repositorio;
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para recuperar coleções de objeto ou apenas o único objeto
     * cadastrado na memória.
     *
     * @return de {@link Object}
     */
    @Override
    public Projeto recuperar() {
        return this.projeto;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Esse método deve ser utilizadao para o registro de dados na memória.
     *
     * @param fundacao que pode ser de qualquer tipo
     * @return de tipo {@link Boolean}
     */
    @Override
    public boolean novo(Object objeto) {
        if (objeto != null) {
            if (objeto instanceof FundacaoProfunda) {
                this.projeto.setFundacaoProfund((FundacaoProfunda) objeto);
                return true;
            } else if (objeto instanceof Terreno) {
                if (this.projeto.getFundacaoProfunda() != null) {
                    this.projeto.getFundacaoProfunda().setTerreno((Terreno) objeto);
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método que deve ser utilizado para alterar registros já cadastrados.
     *
     * @param fundacao de tipo {@link Object}
     * @return de tipo {@link Boolean}
     */
    @Override
    public boolean alterar(Object fundacao) {
        if (this.projeto != null) {
            if (fundacao instanceof FundacaoProfunda) {
                this.projeto.setFundacaoProfund((FundacaoProfunda) fundacao);
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean alterar(Projeto projeto) {
        Projeto projetoTemp = (Projeto) projeto;
        if (projetoTemp != null) {
            this.projeto.setPerfil(projetoTemp.getPerfil());
            this.projeto.setRef(projetoTemp.getRef());
            this.projeto.setDataDoProjeto(projetoTemp.getDataDoProjeto());
            this.projeto.setResultadosFundacaoProfunda(projetoTemp.getResultadosFundacaoProfunda());
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método que deve ser utilizado para a exclusão de coleções de objetos da memória.
     *
     * @return
     */
    @Override
    public boolean deletar() {
        this.projeto = null;
        if (this.projeto == null) {
            return true;
        } else {
            return false;
        }
    }
}
