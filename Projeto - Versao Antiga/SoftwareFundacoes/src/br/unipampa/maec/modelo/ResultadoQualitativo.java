/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo;

/**
 *
 * @author Gabriel B Moro
 */
public class ResultadoQualitativo {

    private Pilar pilar;
    private Estaca estaca;
    private int quantidade;

    public ResultadoQualitativo() {

    }

    public ResultadoQualitativo(Estaca estaca, int quantidade, Pilar pilarTemp) {
        this.estaca = estaca;
        this.quantidade = quantidade;
        this.pilar = pilarTemp;
    }

    public Estaca getEstaca() {
        return estaca;
    }

    public void setEstaca(Estaca estaca) {
        this.estaca = estaca;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Pilar getPilar() {
        return pilar;
    }

    public void setPilar(Pilar pilar) {
        this.pilar = pilar;
    }


}
