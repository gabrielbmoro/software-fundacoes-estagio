package br.unipampa.maec.modelo;

import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.util.ArrayList;

/**
 * <b>Propósito:</b>
 * <br>
 * Classe fundamental, responsável por encapsular todo o potencial do software. Através do
 * projeto é possível acessar classes como: {@link FundacaoProfunda}, {@link FundacaoRasa},
 * {@link EngenheiroCivil}, {@link Cliente} e {@link RepositoryProjetoDeEstacas}.
 * <br>
 * <b>Instruções de uso:</b>
 * Essa classe deve ser utilizada como parâmetro de entrada para o
 * {@link RepositoryProjetoDeEstacas} o qual manterá as informações do projeto em memória.
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class Projeto {

    private int sondagem;
    private FundacaoProfunda fundacaoProfunda;
    private Cliente cliente;
    private String ref;
    private String perfil;
    private double pesoEspCilindro;
    private RepositoryProjetoDeEstacas repositoryProjEst;
    private EngenheiroCivil engenheiroCivil;
    private ArrayList<ResultadoFundacaoProfunda> resultadosFundacaoProfunda;
    private ArrayList<ResultadoQualitativo> resultadosQualitativo;
    private String dataDoProjeto;
    private String empresaResponsavel;
    private String dataDeExecucao;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método que inicializa um objeto de tipo Projeto.
     */
    public Projeto() {
        this.engenheiroCivil = new EngenheiroCivil();
        this.resultadosFundacaoProfunda = new ArrayList<>();
        this.resultadosQualitativo = new ArrayList<>();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Através desse método é possível registrar em memória a referência e o perfil do
     * projeto.
     *
     * @param ref de tipo {@link Integer}
     * @param perfil de tipo {@link Integer}
     * @param dataDeProjeto
     * @return de tipo {@link Boolean}
     */
    public boolean registrarDados(String ref, String perfil, String dataDeProjeto) {
        this.ref = ref;
        this.perfil = perfil;
        this.dataDoProjeto = dataDeProjeto;
        this.repositoryProjEst = RepositoryProjetoDeEstacas.getInstance();

        this.getRepositoryProjEst().alterar(this);

        if (this.getRepositoryProjEst().recuperar() == null) {
            return this.getRepositoryProjEst().novo(this.getFundacaoProfunda());
        } else {
            return this.getRepositoryProjEst().alterar(this.getFundacaoProfunda());
        }
    }

    public boolean registrarDados(String clienteNome, String local, String referencia,
                String dataDoProjeto, String dataDeExecucao, int sondagem, String empresaResponsavel) {
        this.cliente = new Cliente(clienteNome, local);
        this.ref = referencia;
        this.dataDoProjeto = dataDoProjeto;
        this.dataDeExecucao = dataDeExecucao;
        this.sondagem = sondagem;
        this.empresaResponsavel = empresaResponsavel;
        this.repositoryProjEst = RepositoryProjetoDeEstacas.getInstance();

        this.getRepositoryProjEst().alterar(this);

        if (this.getRepositoryProjEst().recuperar() == null) {
            return this.getRepositoryProjEst().novo(this.getFundacaoProfunda());
        } else {
            return this.getRepositoryProjEst().alterar(this.getFundacaoProfunda());
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para recuperar os dados de identificação de um projeto.
     *
     * @return uma lista de dados de tipo {@link Object}
     */
    public ArrayList<Object> getDadosIdentificacao() {
        this.repositoryProjEst = RepositoryProjetoDeEstacas.getInstance();
        Projeto projTemp = this.getRepositoryProjEst().recuperar();
        ArrayList<Object> dados = new ArrayList<>();
        if (projTemp != null) {
            dados.add(projTemp.getRef());
            dados.add(projTemp.getPerfil());
            if (projTemp.getPesoEspCilindro() != 0.0) {
                dados.add(projTemp.getPesoEspCilindro());
            }
        }
        return dados;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por organizar as informações registradas em memória e calcular
     * com um limite de profundidade (profundidade inicial e final) os cálculos de
     * fundações profundas, os quais são providos pelo objeto de tipo
     * {@link EngenheiroCivil}.
     *
     * @param de de tipo {@link Integer} que reflete a profundidade inicial
     * @param ate de tipo {@link Integer} que reflete a profundidade final
     */
    public void definirResultadosFundacaoProfunda(int de, int ate) {
        int passo = 1;
        this.repositoryProjEst = RepositoryProjetoDeEstacas.getInstance();
        Projeto projeto = this.repositoryProjEst.recuperar();
        FundacaoProfunda fundacao = projeto.getFundacaoProfunda();
        ArrayList<Solo> solos = fundacao.getTerreno().getSolos();
        ArrayList<Estaca> estacas = fundacao.getBlocos().get(0).getEstacas();

        ArrayList<ItemDeEnsaio> ensaios = fundacao.getTerreno().getEnsaios();

        ArrayList<ItemDeEnsaio> ensaiosUtilizados = this.engenheiroCivil.ensaiosUtilizados(
                    de, ate, ensaios);

        int nsptPonta = 0;
        Solo soloValido = null;
        double ab = 0.0, resultadoQp_aoki = 0.0, resultadoQp_decourt = 0.0, resultadoQl_aoki = 0.0,
                    resultadoQl_decourt = 0.0, resultadoQt_aoki = 0.0, resultadoQt_decourt = 0.0;
        for (ItemDeEnsaio itemDeEnsaio : ensaiosUtilizados) {
            System.err.printf("\n=============Profundidade: %d, Nspt: %d======\n", itemDeEnsaio.getProfundidade(), itemDeEnsaio.getNspt());

            for (Estaca estacaTemp : estacas) {

                ResultadoFundacaoProfunda resultadoFundacaoProfunda = new ResultadoFundacaoProfunda();
                resultadoFundacaoProfunda.setProfundidade(itemDeEnsaio.getProfundidade());
                resultadoFundacaoProfunda.setNsptDaCamada(itemDeEnsaio.getNspt());
                System.err.printf("-- Estaca de Dimensão: %d, Tipo de Estaca: %s ---", estacaTemp.getDimensaoDefinida(), String.valueOf(estacaTemp.getTipoDaEstaca()));
                soloValido = this.engenheiroCivil.recuperaSoloValido(
                            itemDeEnsaio.getProfundidade(), solos);
                ab = this.engenheiroCivil.calcularAreaDaBase(estacaTemp.getDimensaoDefinida(), estacaTemp.getTipoDaEstaca());
                nsptPonta = itemDeEnsaio.getNspt();
                resultadoFundacaoProfunda.setAreaDaBase(ab);
                resultadoQp_aoki = this.engenheiroCivil.
                            calcularCapacidadeDeCarga_FundResistenciaDePonta_AOKI(estacaTemp.getCONST_F1(),
                                        soloValido.getK(),
                                        nsptPonta, ab);
                resultadoFundacaoProfunda.setkDoSolo(soloValido.getK());
                resultadoFundacaoProfunda.setF1(estacaTemp.getCONST_F1());
                double raio = this.engenheiroCivil.calcularRaio(estacaTemp.getDimensaoDefinida());
                double perimetro = this.engenheiroCivil.calcularPerimetro(raio);
                resultadoFundacaoProfunda.setPerimetro(perimetro);
                double nsptMedio = this.engenheiroCivil.calcularNsptMedio(ensaios, itemDeEnsaio);
                resultadoFundacaoProfunda.setNsptMedio(nsptMedio);
                int deltaL = 1;
                /*CALCULANDO MÉTODO DE AOKI*/
                resultadoQl_aoki = resultadoQl_aoki + this.engenheiroCivil.
                            calcularCapacidadeDeCarga_FundAtritoLateral_AOKI(estacaTemp.getCONST_F2(),
                                        soloValido.getAlfa(), soloValido.getK(), nsptMedio,
                                        perimetro, deltaL);
                resultadoFundacaoProfunda.setDeltaL(deltaL);
                resultadoFundacaoProfunda.setF2(estacaTemp.getCONST_F2());
                resultadoFundacaoProfunda.setAlfaDoSolo(soloValido.getAlfa());
                resultadoQt_aoki = this.engenheiroCivil.
                            calcularCapacidadeDeCarga_FundProfund_AOKI(resultadoQl_aoki, resultadoQp_aoki);

                soloValido.configurarValoresPorEstaca(estacaTemp);

                /*CALCULANDO MÉTODO DE DECOURT*/
                resultadoQp_decourt = this.engenheiroCivil.calcularCapacidadeDeCarga_FundResistenciaDePonta_DECOURT(soloValido.
                            getCDaCategoria(),
                            nsptMedio, soloValido.getAlfaDaCategoria(), ab);

                resultadoFundacaoProfunda.setKdaCategoria(soloValido.getCDaCategoria());
                resultadoFundacaoProfunda.setAlfaDaCategoria(soloValido.getAlfaDaCategoria());
                resultadoFundacaoProfunda.setBetaDoSolo(soloValido.getBetaDaCategoria());
                double nsptPor3 = this.engenheiroCivil.calcularNsptPor3(nsptPonta);
                resultadoFundacaoProfunda.setNsptPor3(nsptPor3);
                resultadoQl_decourt = resultadoQl_decourt+ this.engenheiroCivil.calcularCapacidadeDeCarga_FundAtritoLateral_DECOURT(soloValido.getBetaDaCategoria(),
                            nsptPonta, perimetro, itemDeEnsaio.getProfundidade(), nsptPor3);

                resultadoQt_decourt = this.engenheiroCivil.calcularCapacidadeDeCarga_FundProfund_DECOURT(resultadoQl_decourt, resultadoQp_decourt);

                resultadoFundacaoProfunda.setDimensaoDeEstaca(estacaTemp.getDimensaoDefinida());
                resultadoFundacaoProfunda.setProfundidade(itemDeEnsaio.getProfundidade());
                resultadoFundacaoProfunda.setQp_aoki(resultadoQp_aoki);

                double qp_aoki_fs = resultadoQp_aoki / 4;

                resultadoFundacaoProfunda.setQp_aoki_reajustado(qp_aoki_fs);
                resultadoFundacaoProfunda.setQl_aoki(resultadoQl_aoki);

                double ql_aoki_fs = (resultadoQl_aoki / 2);
                resultadoFundacaoProfunda.setQl_aoki_reajustado(ql_aoki_fs);

                double qt_aoki_fs = qp_aoki_fs + ql_aoki_fs;

                resultadoFundacaoProfunda.setQt_aoki(resultadoQt_aoki);
                resultadoFundacaoProfunda.setQt_aoki_FS(qt_aoki_fs);

                resultadoFundacaoProfunda.setQp_decourt(resultadoQp_decourt);

                double qp_decourt_fs = (resultadoQp_decourt / 4);
                resultadoFundacaoProfunda.setQp_decourt_reajustado(qp_decourt_fs);

                resultadoFundacaoProfunda.setQl_decourt(resultadoQl_decourt);

                double ql_decourt_fs = (resultadoQl_decourt / 2);
                resultadoFundacaoProfunda.setQl_decourt_reajustado(ql_decourt_fs);

                double qt_decourt_fs = qp_decourt_fs + ql_decourt_fs;

                resultadoFundacaoProfunda.setQt_decourt(resultadoQt_decourt);
                resultadoFundacaoProfunda.setQt_decourt_FS(qt_decourt_fs);
                projeto.getResultadosFundacaoProfunda().add(resultadoFundacaoProfunda);

                System.out.println("");
            }
        }

        RepositoryProjetoDeEstacas.getInstance().alterar(this);
    }

    public FundacaoProfunda recuperarFundacaoProfunda() {
        if (RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda() == null) {
            return new FundacaoProfunda();
        } else {
            return RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda();
        }
    }

    public void definirResultadosQualitativos() {
        FundacaoProfunda fundacao = recuperarFundacaoProfunda();
        ArrayList<Pilar> pilares = fundacao.getPilares();
        for (Pilar pilarTemp : pilares) {
            this.resultadosQualitativo.add(this.engenheiroCivil.analizarMelhorOpcaoQualitativo(pilarTemp, fundacao.getEstacas()));
        }
//        for (Pilar pilarTemp : pilares) {
//            int quantidadeTemp = 0;
//            Estaca estacaEscolhidaTemp = null;
//            for (int count = 0; count < fundacao.getEstacas().size(); count++) {
//                if (count == 0) {
//                    quantidadeTemp = this.engenheiroCivil.calcularQuantidadeDeEstacas(pilarTemp.getCarga(), fundacao.getEstacas().get(count).getCapacidadeDeCarga());
//                    estacaEscolhidaTemp = fundacao.getEstacas().get(count);
//                } else {
//                    int quantidadeTemp2 = this.engenheiroCivil.calcularQuantidadeDeEstacas(pilarTemp.getCarga(), fundacao.getEstacas().get(count).getCapacidadeDeCarga());
//                    if (quantidadeTemp == quantidadeTemp2) {
//                        if (estacaEscolhidaTemp.getDimensaoDefinida() > fundacao.getEstacas().get(count).getDimensaoDefinida()) {
//                            estacaEscolhidaTemp = fundacao.getEstacas().get(count);
//                        }
//                    }
//                    if (quantidadeTemp2 < quantidadeTemp) {
//                        quantidadeTemp = quantidadeTemp2;
//                        estacaEscolhidaTemp = fundacao.getEstacas().get(count);
//                    } else {
//                        continue;
//                    }
//                }

//                if (quantidadeTemp == 0) {
//                    quantidadeTemp = this.engenheiroCivil.calcularQuantidadeDeEstacas(pilarTemp.getCarga(), estacaTemp.getCapacidadeDeCarga());
//                    estacaEscolhidaTemp = estacaTemp;
//                } else {
//                    int quantidadeSubTemp = this.engenheiroCivil.calcularQuantidadeDeEstacas(pilarTemp.getCarga(), estacaTemp.getCapacidadeDeCarga());
//                    if (quantidadeSubTemp == quantidadeTemp) {
//                        if (estacaTemp.getDimensaoDefinida() < estacaEscolhidaTemp.getDimensaoDefinida()) {
//                            estacaEscolhidaTemp = estacaTemp;
//                        }
//                    } if (quantidadeSubTemp < quantidadeTemp) {
//                        if (estacaTemp.getDimensaoDefinida() > estacaEscolhidaTemp.getDimensaoDefinida()) {
//                            quantidadeTemp = quantidadeSubTemp;
//                            estacaEscolhidaTemp = estacaTemp;
//                        }
//                    }
//                }
//            }
//            resultadosQualitativo.add(new ResultadoQualitativo(estacaEscolhidaTemp, quantidadeTemp, pilarTemp));
//        }
        Projeto projeto = RepositoryProjetoDeEstacas.getInstance().recuperar();
        projeto.setFundacaoProfund(fundacao);
        projeto.setResultadosQualitativo(resultadosQualitativo);
        RepositoryProjetoDeEstacas.getInstance().alterar(projeto);
    }

    public void registrarEstacas(ArrayList<Estaca> estacas) {
        FundacaoProfunda fundacaoProfundaTemp = recuperarFundacaoProfunda();
        fundacaoProfundaTemp.setEstacas(estacas);
        RepositoryProjetoDeEstacas.getInstance().alterar(fundacaoProfundaTemp);
    }

    public void registrarFundacaoProfunda(FundacaoProfunda fundacaoProfunda) {
        RepositoryProjetoDeEstacas.getInstance().alterar(fundacaoProfunda);
    }

    public Projeto recuperarProjeto() {
        return RepositoryProjetoDeEstacas.getInstance().recuperar();
    }

    /**
     * @return the data
     */
    public String getDataDoProjeto() {
        return dataDoProjeto;
    }

    /**
     * @param data the data to set
     */
    public void setDataDoProjeto(String data) {
        this.dataDoProjeto = data;
    }

    /**
     * @return the sondagem
     */
    public int getSondagem() {
        return sondagem;
    }

    /**
     * @param sondagem the sondagem to set
     */
    public void setSondagem(int sondagem) {
        this.sondagem = sondagem;
    }

    /**
     * @return the fundacao
     */
    public FundacaoProfunda getFundacaoProfunda() {
        return fundacaoProfunda;
    }

    /**
     * @param fundacaoProfun the fundacao to set
     */
    public void setFundacaoProfund(FundacaoProfunda fundacaoProfun) {
        this.fundacaoProfunda = fundacaoProfun;
    }

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref the ref to set
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * @return the perfil
     */
    public String getPerfil() {
        return this.perfil;
    }

    /**
     * @param perfil the ref to set
     */
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    /**
     *
     * @return the Projeto
     */
    public Projeto getProjetoInstance() {
        this.repositoryProjEst = RepositoryProjetoDeEstacas.getInstance();
        return this.getRepositoryProjEst().recuperar();
    }

    /**
     * @return the perfil
     */
    public double getPesoEspCilindro() {
        return this.pesoEspCilindro;
    }

    /**
     * @param peso the ref to set
     */
    public void setPesoEspCilindro(double peso) {
        this.pesoEspCilindro = peso;
    }

    /**
     * @return the repositoryProjEst
     */
    public RepositoryProjetoDeEstacas getRepositoryProjEst() {
        return repositoryProjEst;
    }

    /**
     * @return the resultadosFundacaoProfunda
     */
    public ArrayList<ResultadoFundacaoProfunda> getResultadosFundacaoProfunda() {
        return resultadosFundacaoProfunda;
    }

    /**
     * @param resultadosFundacaoProfunda the fundacaoRasa to set
     */
    public void setResultadosFundacaoProfunda(ArrayList<ResultadoFundacaoProfunda> resultadosFundacaoProfunda) {
        this.resultadosFundacaoProfunda = resultadosFundacaoProfunda;
    }

    public EngenheiroCivil getEngenheiroCivil() {
        return engenheiroCivil;
    }

    public void setEngenheiroCivil(EngenheiroCivil engenheiroCivil) {
        this.engenheiroCivil = engenheiroCivil;
    }

    public String getEmpresaResponsavel() {
        return empresaResponsavel;
    }

    public void setEmpresaResponsavel(String empresaResponsavel) {
        this.empresaResponsavel = empresaResponsavel;
    }

    public String getDataDeExecucao() {
        return dataDeExecucao;
    }

    public void setDataDeExecucao(String dataDeExecucao) {
        this.dataDeExecucao = dataDeExecucao;
    }

    public ArrayList<ResultadoQualitativo> getResultadosQualitativo() {
        return resultadosQualitativo;
    }

    public void setResultadosQualitativo(ArrayList<ResultadoQualitativo> resultadosQualitativo) {
        this.resultadosQualitativo = resultadosQualitativo;
    }

}
