/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo;

import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;
import java.util.ArrayList;

/**
 * <b>Propósito:</b>
 * <br>
 * Abstrair de maneira geral as propriedades de uma fundação profunda, a qual é aplicada
 * sobre um determinado {@link Terreno}.
 * <br>
 * <b>Instruções de uso:</b>
 * <br>
 *
 * Essa classe deve ser utilizada em parceria com a classe {@link Projeto}, pois o projeto
 * possui um tipo de fundação alocada a ele.
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class FundacaoProfunda {

    private Terreno terreno;
    private RepositoryProjetoDeEstacas repository;
    private int alternativa;
    private ArrayList<Bloco> blocos;
    private double tensaoCalculoConcreto;
    private double fs_global;
    private double pesoEspecificoCilindroDeTracao;
    private int[] faixaDeCalculo;
    private int passo;
    private ArrayList<Pilar> pilares;
    private ArrayList<Estaca> estacas;
    private Estaca estacaReferencia;

    /**
     * <b>Propósito:</b>
     * <br>
     * Esse método construtor inicializa um objeto de tipo fundação profunda que possui
     * variáveis como terreno, coeficientes de segurança e demais variáveis.
     */
    public FundacaoProfunda() {
        super();
        this.terreno = new Terreno();
        this.tensaoCalculoConcreto = this.repository.TENSAO_CALCULO_CONCRETO;
        this.fs_global = this.repository.FS_GLOBAL;
        this.repository = RepositoryProjetoDeEstacas.getInstance();
        this.faixaDeCalculo = new int[2];
        this.blocos = new ArrayList<>();
        this.pilares = new ArrayList<>();
        this.estacas = new ArrayList<>();
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Através desse método é possível registrar a alternativa da fundação.
     *
     * @param alternativa de tipo {@link Integer}
     * @return de tipo {@link Boolean}
     */
    public boolean registrarAlternativa(int alternativa) {
        this.alternativa = alternativa;
        this.repository = RepositoryProjetoDeEstacas.getInstance();

        Projeto projeto = this.repository.recuperar();
        projeto.getFundacaoProfunda().setAlternativa(alternativa);
        if (projeto.getFundacaoProfunda().getAlternativa() == alternativa) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Os dados de fatores são definidos a partir desse método, através dos limites de
     * profundidade.
     *
     * @param deFaixaDeCalculo de tipo {@link Integer}
     * @param ateFaixaDeCalculo de tipo {@link Integer}
     * @param passo de tipo {@link Integer}
     * @return o resultado de tipo {@link Integer}
     */
    public boolean definirDadosDeFatores(int deFaixaDeCalculo, int ateFaixaDeCalculo, int passo) {
        this.faixaDeCalculo[0] = deFaixaDeCalculo;
        this.faixaDeCalculo[1] = ateFaixaDeCalculo;
        this.passo = passo;
        double profundMax = 0;

        Projeto projeto = this.repository.recuperar();
        Terreno terreno = projeto.getFundacaoProfunda().getTerreno();
        if (terreno.getEnsaios().size() != 0) {
            profundMax = terreno.getEnsaios().get(terreno.getEnsaios().size() - 1).getProfundidade();
            if (terreno.getEnsaios().size() == 0) {
                profundMax = terreno.getEnsaios().get(0).getProfundidade();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Esse método objetiva o registro dos nspts por profundidade.
     *
     * @param nspt de tipo {@link Integer} é definido por profundidade.
     * @return de tipo {@link Boolean}
     */
    public boolean salvarDadosTerreno(ArrayList<Integer> nspt) {
        if (nspt != null) {
            if (RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda() != null
                        && RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda().getTerreno() != null) {
                this.terreno = RepositoryProjetoDeEstacas.getInstance().recuperar().getFundacaoProfunda().getTerreno();
                this.terreno.preencherProfundidade(nspt);
            } else {
                this.terreno = new Terreno(nspt);
            }
            this.repository.recuperar().setFundacaoProfund(new FundacaoProfunda());
            this.repository.recuperar().getFundacaoProfunda().setTerreno(this.terreno);
            return true;
        } else {
            return false;
        }
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por recuperar o nspt da ponta das estacas de acordo com a
     * profundidade inicial.
     *
     * @param de de tipo {@link Integer}
     * @param ensaios de tipo {@link ArrayList}
     * @return de tipo {@link Integer}
     */
    public int recuperarNsptDaPonta(double de, ArrayList<ItemDeEnsaio> ensaios) {
        int deInt = (int) Math.round(de);

        for (ItemDeEnsaio temp : ensaios) {
            if (deInt >= temp.getProfundidade() && deInt <= temp.getProfundidade()) {
                return temp.getNspt();
            }
        }
        return 0;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Esse método retorna os Itens de Ensaio da Fundação profunda, os quais incluem
     * objetos de tipo {@link ItemDeEnsaio} que possuem um estado que condiz de acordo com
     * a sua profundidade e o seu nspt associado.
     *
     * @return um conjunto de dados de tipo {@link Object}
     */
    public ArrayList<Object> getResultadosPrfNsp() {
        Projeto projeto = this.repository.recuperar();
        FundacaoProfunda tmp = (FundacaoProfunda) projeto.getFundacaoProfunda();
        ArrayList<ItemDeEnsaio> ensaios = tmp.getTerreno().getEnsaios();
        int tamanho = ensaios.size();
        int count = 1;
        ArrayList<Object> tudoJunto = new ArrayList<>();
        for (int c = 0; c < tamanho; c++) {
            tudoJunto.add(count);
            count++;
            tudoJunto.add(ensaios.get(c).getNspt());
        }
        return tudoJunto;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por recuperar a alternativa cadastrada na memória do software.
     *
     * @return de tipo {@link Integer}
     */
    public int recuperarAlternativa() {
        this.repository = RepositoryProjetoDeEstacas.getInstance();
        Projeto temp = this.repository.recuperar();
        return temp.getFundacaoProfunda().getAlternativa();
    }

    public boolean registrarPilares(ArrayList<Integer> cargas) {
        for (int count=0;count< cargas.size();count++) {
            pilares.add(new Pilar(cargas.get(count), cargas.get(count+1)));
            count++;
        }
        Projeto projeto = RepositoryProjetoDeEstacas.getInstance().recuperar();
        projeto.setFundacaoProfund(this);
        RepositoryProjetoDeEstacas.getInstance().alterar(projeto);
        return true;
    }

    /**
     * @return the fs_global
     */
    public double getFs_global() {
        return fs_global;
    }

    /**
     * @param fs_global the fs_global to set
     */
    public void setFs_global(double fs_global) {
        this.fs_global = fs_global;
    }

    /**
     * @return the terreno
     */
    public Terreno getTerreno() {
        return this.terreno;
    }

    /**
     * @param terreno the alternativa to set
     */
    public void setTerreno(Terreno terreno) {
        this.terreno = terreno;
    }

    /**
     * @return the alternativa
     */
    public int getAlternativa() {
        return alternativa;
    }

    /**
     * @param alternativa the alternativa to set
     */
    public void setAlternativa(int alternativa) {
        this.alternativa = alternativa;
    }

    /**
     * @return the blocos
     */
    public ArrayList<Bloco> getBlocos() {
        return blocos;
    }

    /**
     * @param blocos the blocos to set
     */
    public void setBlocos(ArrayList<Bloco> blocos) {
        this.blocos = blocos;
    }

    /**
     * @return the tensaoCalculoConcreto
     */
    public double getTensaoCalculoConcreto() {
        return tensaoCalculoConcreto;
    }

    /**
     * @param tensaoCalculoConcreto the tensaoCalculoConcreto to set
     */
    public void setTensaoCalculoConcreto(double tensaoCalculoConcreto) {
        this.tensaoCalculoConcreto = tensaoCalculoConcreto;
    }

    /**
     * @return the pesoEspecificoCilindroDeTracao
     */
    public double getPesoEspecificoCilindroDeTracao() {
        return pesoEspecificoCilindroDeTracao;
    }

    /**
     * @param pesoEspecificoCilindroDeTracao the pesoEspecificoCilindroDeTracao to set
     */
    public void setPesoEspecificoCilindroDeTracao(double pesoEspecificoCilindroDeTracao) {
        this.pesoEspecificoCilindroDeTracao = pesoEspecificoCilindroDeTracao;
    }

    /**
     * @return the faixaDeCalculo
     */
    public int[] getFaixaDeCalculo() {
        return faixaDeCalculo;
    }

    /**
     * @param faixaDeCalculo the faixaDeCalculo to set
     */
    public void setFaixaDeCalculo(int[] faixaDeCalculo) {
        this.faixaDeCalculo = faixaDeCalculo;
    }

    /**
     * @return the passo
     */
    public int getPasso() {
        return passo;
    }

    /**
     * @param passo the passo to set
     */
    public void setPasso(int passo) {
        this.passo = passo;
    }

    public ArrayList<Pilar> getPilares() {
        return pilares;
    }

    public void setPilares(ArrayList<Pilar> pilares) {
        this.pilares = pilares;
    }

    public ArrayList<Estaca> getEstacas() {
        return estacas;
    }

    public void setEstacas(ArrayList<Estaca> estacas) {
        this.estacas = estacas;
    }

    public RepositoryProjetoDeEstacas getRepository() {
        return repository;
    }

    public void setRepository(RepositoryProjetoDeEstacas repository) {
        this.repository = repository;
    }

    public Estaca getEstacaReferencia() {
        return estacaReferencia;
    }

    public void setEstacaReferencia(Estaca estacaReferencia) {
        this.estacaReferencia = estacaReferencia;
    }

}
