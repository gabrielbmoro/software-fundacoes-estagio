/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo;

/**
 * <b>Propósito:</b>
 * <br>
 * Abstrair de maneira geral as propriedades de um item de ensaio, o qual é alimentado de
 * acordo com o {@link Projeto} e descreve ensaios individuais que incluem profundidade e
 * um nspt associado.
 * <br>
 * <b>Instruções de uso:</b>
 * Essa classe deve ser utilizada para descrever ensaios individuais de profundidades em
 * unidades. Além disso, essa classe deve ser utilizada como item de lista (estrutura de
 * dados), a qual um {@link Projeto} possui uma lista de itens de
 * ensaio ({@link ItemDeEnsaio}).
 * <br>
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class ItemDeEnsaio {

    private int profundidade;
    private int nspt;

    /**
     * <b>Propósito:</b>
     * <br>
     * Método que inicializa um objeto que possui como estado profundidade e nspt.
     *
     * @param profundidade de tipo {@link Integer}
     * @param nspt de tipo {@link Integer}
     */
    public ItemDeEnsaio(int profundidade, int nspt) {
        this.profundidade = profundidade;
        this.nspt = nspt;
    }

    /**
     *
     * @return the profundidade
     */
    public int getProfundidade() {
        return profundidade;
    }

    /**
     * @param profundidade the profundidade to set
     */
    public void setProfundidade(int profundidade) {
        this.profundidade = profundidade;
    }

    /**
     *
     * @return the nspt
     */
    public int getNspt() {
        return nspt;
    }

    /**
     * @param nspt the nspt to set
     */
    public void setNspt(int nspt) {
        this.nspt = nspt;
    }
}
