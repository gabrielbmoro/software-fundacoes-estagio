package br.unipampa.maec.modelo;
/**
    * <b>Propósito:</b>
    * <br>
    * Essa classe deve ser utilizada como abstração de um cliente que possui como estado 
    * nome e endereço. Esses dados compõe o relatório gerado pela ferramenta.
    * <br>
    * <b>Instruções de uso:</b>
    * <br>
    * Deve ser utilizado pelas classes de Tipo {@link EngenheiroCivil}.
    * 
    * @author GabrielBMoro
    * @version 0.1
    * @since 24/01/2014 
    */
public class Cliente {

    private String nome;
    private String enderecoLocal;
/**
 * <b>Propósito:</b>
 * <br>
 * O método construtor tem o objetivo de inicializar um cliente que possui nome e endereço local.
 * @param nome
 * @param enderecoLocal 
 */
    public Cliente(String nome, String enderecoLocal) {
        this.nome = nome;
        this.enderecoLocal = enderecoLocal;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the enderecoLocal
     */
    public String getEnderecoLocal() {
        return enderecoLocal;
    }

    /**
     * @param enderecoLocal the enderecoLocal to set
     */
    public void setEnderecoLocal(String enderecoLocal) {
        this.enderecoLocal = enderecoLocal;
    }

}
