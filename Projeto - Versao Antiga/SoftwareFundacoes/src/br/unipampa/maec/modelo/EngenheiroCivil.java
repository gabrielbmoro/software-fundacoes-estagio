/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.modelo;

import java.util.ArrayList;
import java.util.List;
import br.unipampa.maec.modelo.repositorio.RepositoryProjetoDeEstacas;

/**
 * <b>Propósito:</b>
 * <br>
 * Essa classe deve ser utilizada para acionar os cálculos e todas as operações
 * matemáticas de Engenharia Civil, essas operações e cálculos são acionados pelo Usuário
 * desse software.
 *
 * <br>
 * <b>Instruções de uso:</b>
 * Deve ser utilizado em parceria com as classes de Tipo {@link Projeto}.
 *
 * @author GabrielBMoro
 * @version 0.3
 * @since 24/01/2014
 */
public class EngenheiroCivil {

    /**
     * <b>Propósito:</b>
     * <br>
     * Existem dois tipos de cálculo de atrito lateral, o de aoki e o de decourt. Esse
     * cálculo realizado tem por objetivo utilizar os parâmetros de aoki.
     *
     * @param alfa é a razão de atrito que pode variar de acordo com camada de solo
     * @param k é o coeficiente que varia de acordo com a camada de solo
     * @param nm é a média de nspts do solo, de acordo com a camada ele pode variar
     * @param p é o perímetro, o qual varia de acordo com a dimensão da estaca
     * @return resultado de tipo {@link Integer} o qual é arredondado sempre para o
     * tamanho inferior
     */
    public double calcularCapacidadeDeCarga_FundAtritoLateral_AOKI(double constante_F2, 
                double alfa, int k, double nm, double p, int deltaL) {

        int nsptMedio = (int) Math.ceil(nm);
        alfa /= 100;
        double result = alfa * k * nsptMedio * p * (deltaL / constante_F2);
        System.err.printf("======>>Ql Aoki, ----\n %f (resultado) = %f (alfa) x %d (k) x %d (nsptMedio) x %f (p) x (%d (deltaL) div %f (F2 da Estaca)\n", 
                    result, alfa, k, nsptMedio,
                    p, deltaL, constante_F2);

        return result;
    }
    public double calcularNsptPor3(int nsptDaCamada){
        return nsptDaCamada / 3.0;
    }
    /**
     * <b>Propósito:</b>
     * <br>
     * Existem dois tipos de cálculo de atrito lateral, o de aoki e o de decourt. Esse
     * cálculo realizado tem por objetivo utilizar os parâmetros de aoki.
     *
     * @param alfa é a razão de atrito que pode variar de acordo com camada de solo
     * @param k é o coeficiente que varia de acordo com a camada de solo
     * @param nm é a média de nspts do solo, de acordo com a camada ele pode variar
     * @param p é o perímetro, o qual varia de acordo com a dimensão da estaca
     * @return resultado de tipo {@link Integer} o qual é arredondado sempre para o
     * tamanho inferior
     */
    public double calcularCapacidadeDeCarga_FundAtritoLateral_DECOURT(double beta, int nsptDaCamada,
                double perimetro,int profundidade, double nsptPor3) {
        nsptPor3 += 1;
        
        double result = beta * nsptPor3 * perimetro * 1 * profundidade;
       System.err.printf("======>>Ql Decourt, ----\n%f (resultado) = %f (beta)x %f (nsptPor3) x %f (perimetro) x %d profundidade\n", 
                    result, beta, nsptPor3, perimetro,profundidade);
        return result;
    }

    /**
     * <b>Propósito: </b>
     * <br>
     * Esse método tem por objetivo calcular a resistência de ponta da estaca, a qual
     * varia de acordo com a sua dimensão e com a camada de solo utilizada.
     *
     * @param k é coeficiente do solo
     * @param nsptCamada é o valor nspt da base (ponta) da estaca
     * @param ab é área da base (ponta) da estaca (m²)
     * @return
     */
    public double calcularCapacidadeDeCarga_FundResistenciaDePonta_AOKI(double constante_F1, int k, 
                int nsptCamada, double ab) {
        double result = k * nsptCamada * (ab / constante_F1);
                System.err.printf("======>>Qp Aoki, ----\n%f (resultado) = %d (k) x %d (nsptCamada x %f (ab) / %f (Constante F1 da Estaca)\n", 
                    result, k, nsptCamada, ab, constante_F1);
        return result;
    }

    /**
     * <b>EM CONSTRUÇÃO</b>
     *
     * @return
     */
    public double calcularCapacidadeDeCarga_FundResistenciaDePonta_DECOURT(int k, 
                double nsptMedio, double alfa, double ab) {
        double result = alfa * (double) k * nsptMedio * ab;
           System.err.printf("======>>Qp Decourt, ----\n%f (resultado) = %f (alfa) x %d (k) x %.2f (nsptMedio) x %f (ab) \n", 
                    result, alfa, k, nsptMedio, ab);
        return result;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Esse método deve ser utilizado para recuperar o solo válido de acordo com a
     * profundidade, pois essa ferramenta realiza várias operações variando os níveis de
     * profundidade disponíveis no {@link Terreno}.
     *
     * @param profundidade do solo
     * @param solos que devem ser entendidos como as camadas de solo que compõe o
     * {@link Terreno}
     * @return resultado de tipo {@link Solo}
     */
    public Solo recuperaSoloValido(int profundidade, ArrayList<Solo> solos) {
        int countPasso = 0;
        double deTempSolo = 0.0, ateTempSolo = 0.0;

        for (Solo soloTemporario : solos) {
            deTempSolo = soloTemporario.getDe();
            ateTempSolo = soloTemporario.getAte();
            if (profundidade >= deTempSolo
                        && profundidade <= ateTempSolo) {
                return soloTemporario;
            }
        }
        return null;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método utilizado para calcular a área da base da estaca de acordo com o seu
     * diâmetro.
     *
     * @param diametro de tipo {@link Double} varia de estaca para estaca
     * @return de tipo {@link Double}
     */
    public double calcularAreaDaBase(double diametro, TipoEstaca tipoEstaca) {
        diametro = diametro / 100.0;
        if(tipoEstaca!=TipoEstaca.PRE_MOLDADA_QUADRADA){
        return (RepositoryProjetoDeEstacas.getInstance().PI * (diametro * diametro) / 4.0);
        }else{
            return diametro*diametro;
        }
    }
    /*PRE MOLDADA QUADRADA L * L*/

    /**
     * <b>Propósito</b>
     * <br>
     * Método utilizado para calcular o raio do diâmetro da estaca
     *
     * @param diametro de tipo {@link Double} que varia de acordo com a dimensão da
     * estaca.
     * @return de tipo {@link Double}
     */
    public double calcularRaio(double diametro) {
        return diametro / 2.0;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por calcular o perímetro de uma estaca.
     *
     * @param raio de tipo {@link Double} que varia de acordo com a dimensão da estaca
     * utilizada.
     * @return de tipo {@link Double}
     */
    public double calcularPerimetro(double raio) {
        return (2 * RepositoryProjetoDeEstacas.getInstance().PI * raio) / 100.0;
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por recuperar os ensaios utilizados de acordo com a profundidade
     * desejada.
     *
     * @param de de tipo {@link Double} que é a profundidade inicial
     * @param ate de tipo {@link Double} que é a profundidade final
     * @param ensaios de tipo {@link ArrayList} que é todos os ensaios utilizados.
     * @return
     */
    public ArrayList<ItemDeEnsaio> ensaiosUtilizados(int de, int ate, ArrayList<ItemDeEnsaio> ensaios) {
        int countPasso = 0;
        ArrayList<ItemDeEnsaio> ensaiosUtilizados = new ArrayList<ItemDeEnsaio>();
        if (ate > ensaios.get(ensaios.size() - 1).getProfundidade()) {
            ate = ensaios.get(ensaios.size() - 1).getProfundidade();
        }
        for (int count = 0; count < ensaios.size(); count++) {
            if (ensaios.get(count).getProfundidade() >= de && ensaios.get(count).getProfundidade() <= ate) {
                ensaiosUtilizados.add(new ItemDeEnsaio(ensaios.get(count).getProfundidade(),
                            ensaios.get(count).getNspt()));
            } else {
                continue;
            }
        }
        return ensaiosUtilizados;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por calcular a média de acordo com os valores informados por
     * parâmetros.
     *
     * @param valores de tipo {@link Integer}
     * @return de tipo {@link Double}
     */
    private double calcularMedia(List<Integer> valores) {
        double total = 0.0;
        for (int count = 0; count < valores.size(); count++) {
            total = total + valores.get(count);
        }
        return total / (double) valores.size();
    }

    /**
     * <b>Propósito</b>
     * <br>
     * Método responsável por calcular o nspt médio utilizando os ensaios gerais e um
     * ensaio específico.
     *
     * @param itensEnsaio de tipo {@link ArrayList}
     * @param itemEnsaio de tipo {@link ItemDeEnsaio}
     * @return resultado de tipo {@link Double}
     *
     */
    public double calcularNsptMedio(ArrayList<ItemDeEnsaio> itensEnsaio, ItemDeEnsaio itemEnsaio) {
        double nsptTotal = 0.0;
        int contadorAnterior = 0, contadorPareado = 1, contadorPosterior = 2;
        for (int count = 0; count < itensEnsaio.size(); count++) {
            if (itensEnsaio.get(count).getNspt() == itemEnsaio.getNspt()) {
                if (count == 1) {
                    contadorAnterior = 0;
                    contadorPareado = 1;
                    contadorPosterior = 2;
                }
                if (count != 0 && count != 1) {
                    if ((count + 1) < itensEnsaio.size()) {
                        contadorAnterior = count - 1;
                        contadorPareado = count;
                        contadorPosterior = count + 1;
                    } else {
                        contadorAnterior = count - 2;
                        contadorPareado = count - 1;
                        contadorPosterior = count;
                    }
                }
            }
        }
        int numerador = itensEnsaio.get(contadorAnterior).getNspt()+ itensEnsaio.get(contadorPareado).getNspt()+ itensEnsaio.get(contadorPosterior).getNspt();
        nsptTotal = numerador / 3.0;

        return  nsptTotal;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por calcular a capacidade de carga, que nada mais é que somar os
     * resultados adquiridos no atrito lateral e na resistência de ponta, claro que é
     * esses parâmetros são utilizados de acordo com a abordagem utilizada (Aoki ou
     * Decort).
     *
     * @param atrito_lateral de tipo {@link Double}
     * @param resistencia_ponta de tipo {@link Double}
     * @return resultado de tipo {@link Double}
     */
    public double calcularCapacidadeDeCarga_FundProfund_AOKI(double atrito_lateral, double resistencia_ponta) {
        return (atrito_lateral + resistencia_ponta)/RepositoryProjetoDeEstacas.getInstance().FS_GLOBAL;
    }

    /**
     * <b>Propósito:</b>
     * <br>
     * Método responsável por calcular a capacidade de carga, que nada mais é que somar os
     * resultados adquiridos no atrito lateral e na resistência de ponta, claro que é
     * esses parâmetros são utilizados de acordo com a abordagem utilizada (Aoki ou
     * Decort).
     *
     * @param atrito_lateral de tipo {@link Double}
     * @param resistencia_ponta de tipo {@link Double}
     * @return resultado de tipo {@link Double}
     */
    public double calcularCapacidadeDeCarga_FundProfund_DECOURT(double atrito_lateral, double resistencia_ponta) {
        return (atrito_lateral + resistencia_ponta)/RepositoryProjetoDeEstacas.getInstance().FS_GLOBAL;
    }


    public double calcularQuantidadeDeEstacas(int cargaTotal, int capacidadeDeCargaDaEstaca) {
        double capacidadeDeCargaDaEstacaT = capacidadeDeCargaDaEstaca + 0.1;
        return cargaTotal / capacidadeDeCargaDaEstacaT;
    }

    public ResultadoQualitativo analizarMelhorOpcaoQualitativo(Pilar pilarTemp, ArrayList<Estaca> estacas) {
        double quantidadeTemp = 0.0;
        Estaca estacaEscolhidaTemp = null;
        for (int count = 0; count < estacas.size(); count++) {
                if (count == 0) {
                    quantidadeTemp = calcularQuantidadeDeEstacas(pilarTemp.getCarga(), estacas.get(count).getCapacidadeDeCarga());
                    estacaEscolhidaTemp = estacas.get(count);
                } else {
                    double quantidadeTemp2 = calcularQuantidadeDeEstacas(pilarTemp.getCarga(), estacas.get(count).getCapacidadeDeCarga());
                    if (quantidadeTemp == quantidadeTemp2) {
                        if (estacaEscolhidaTemp.getDimensaoDefinida() > estacas.get(count).getDimensaoDefinida()) {
                            estacaEscolhidaTemp = estacas.get(count);
                        }
                    }
                    if (quantidadeTemp2 < quantidadeTemp) {
                        quantidadeTemp = quantidadeTemp2;
                        estacaEscolhidaTemp = estacas.get(count);
                    } else {
                        continue;
                    }
                }
        }

        return new ResultadoQualitativo(estacaEscolhidaTemp,
                    (int) Math.ceil(quantidadeTemp), pilarTemp);
    }

}
