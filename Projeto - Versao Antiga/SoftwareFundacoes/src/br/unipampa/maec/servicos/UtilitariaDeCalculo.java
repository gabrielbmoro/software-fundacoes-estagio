/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.servicos;

import java.text.NumberFormat;

/**
 *
 * @author Gabriel B Moro
 */
public class UtilitariaDeCalculo {
    
    public static String retornarTextoDeResultadoDouble(double valor)
    {
        NumberFormat nf = NumberFormat.getInstance(); 
        nf.setMaximumFractionDigits(2);
        return nf.format(valor);  
    }
    
}
