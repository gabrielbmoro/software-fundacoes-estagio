/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.servicos;

import java.util.HashMap;
import java.util.Observable;

/**
 *
 * @author Gabriel B Moro
 */
public class IncubacaoDeDadosImportados extends Observable{
    
    private HashMap<CHAVE_UTILIZADA,Object> parametros;
    private static IncubacaoDeDadosImportados myInstance;
    
    private IncubacaoDeDadosImportados(){
        
    }
    
    public static IncubacaoDeDadosImportados getMyInstance(){
        if(myInstance==null){
            myInstance= new IncubacaoDeDadosImportados();
        }
        return myInstance;
    }

    public Object getParametro(CHAVE_UTILIZADA chave){
        return parametros.get(chave);
    }
    
    public HashMap<CHAVE_UTILIZADA, Object> getParametros() {
        return parametros;
    }

    public void setParametros(HashMap<CHAVE_UTILIZADA, Object> parametros) {
        this.parametros = parametros;
        setChanged();
        notifyObservers();
    }
   
}
