/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.servicos;

/**
 *
 * @author Gabriel B Moro
 */
public enum CHAVE_UTILIZADA {
    DATA,
    PERFIL, 
    ALTERNATIVA, 
    REFERENCIA,
    TIPO_DE_ESTACA, 
    PROFUNDIDADE_NSPT,
    SOLOS,
    RESULTADOS_GERADOS
}
