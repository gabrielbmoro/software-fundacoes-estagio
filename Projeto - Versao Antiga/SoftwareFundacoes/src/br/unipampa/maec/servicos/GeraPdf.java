/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unipampa.maec.servicos;

import br.unipampa.maec.apresentacao.GeradorDeMensagem;
import br.unipampa.maec.modelo.Estaca;
import br.unipampa.maec.modelo.ItemDeEnsaio;
import br.unipampa.maec.modelo.ResultadoFundacaoProfunda;
import br.unipampa.maec.modelo.Solo;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author gabrielbmoro
 */
public class GeraPdf extends GeradorDeRelatorio {

    private static String CAMINHO_LOGO = "/br/unipampa/maec/apresentacao/icones/logoTipoSistemaCopia.png";
    private static final int COR_TABELA_CABECALHO_RED = 255;
    private static final int COR_TABELA_CABECALHO_GREEN = 255;
    private static final int COR_TABELA_CABECALHO_BLUE = 255;
    private Font fontePadrao = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
    private Font fontePadraoNegrito = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private Font fontePadraoItalico = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.ITALIC);
    private PdfWriter pdfWriter;

    public void executaPdf(HashMap<CHAVE_UTILIZADA, Object> parametros, String diretorio, String titulo) {
        Document doc = null;
        OutputStream os = null;
        fontePadraoNegrito.setColor(BaseColor.BLACK);
        try {
            doc = new Document(new Rectangle(1108, 508), 72, 72, 72, 72);
            File file = new File(diretorio);
            if (!file.isDirectory()) {
                diretorio = System.getProperty("user.home") + titulo;
                GeradorDeMensagem.exibirMensagemDeInformacao("O relatório pode ser encontrado no seguinte endereço padrão: \n"
                            + "<sua pasta de usuário>//", titulo);
            } else {
                diretorio = diretorio + File.separator + titulo;
            }
            os = new FileOutputStream(diretorio);
            pdfWriter = PdfWriter.getInstance(doc, os);
            doc.open();

            doc.add(recuperarLogoRelatorio());
            doc.add(recuperarCabecalho());
            doc.add(new Paragraph("                      ", fontePadrao));
            doc.add(new Paragraph("                       ", fontePadrao));
            Paragraph p1 = new Paragraph("Perfil: " + parametros.get(CHAVE_UTILIZADA.PERFIL).toString()
                        + "                          " + "Alternativa: " + parametros.get(CHAVE_UTILIZADA.ALTERNATIVA).toString()
                        + "                          " + "Referência: " + parametros.get(CHAVE_UTILIZADA.REFERENCIA).toString(), fontePadrao);
            doc.add(p1);
            doc.add(new Paragraph("                      ", fontePadrao));
            Paragraph p2 = new Paragraph("Estaca escolhida: " + recuperaParametroTextoEstaca((Estaca) parametros.get(CHAVE_UTILIZADA.TIPO_DE_ESTACA)), fontePadrao);
            doc.add(p2);
            doc.add(new Paragraph("                      ", fontePadrao));
            ArrayList<ItemDeEnsaio> ensaiosRealizados = (ArrayList<ItemDeEnsaio>) parametros.get(CHAVE_UTILIZADA.PROFUNDIDADE_NSPT);
            if (ensaiosRealizados != null || !ensaiosRealizados.isEmpty()) {
                escreverTabelaProfundidadePorNSPT(ensaiosRealizados, doc);
            }
            doc.add(new Paragraph("                      ", fontePadrao));

            ArrayList<Solo> solos = (ArrayList<Solo>) parametros.get(CHAVE_UTILIZADA.SOLOS);
            if (solos != null || !solos.isEmpty()) {
                escreverTabelaSolos(solos, doc);
            }

            ArrayList<ResultadoFundacaoProfunda> resultadosFundacaoProfunda = (ArrayList<ResultadoFundacaoProfunda>) parametros.get(CHAVE_UTILIZADA.RESULTADOS_GERADOS);

            if (!resultadosFundacaoProfunda.isEmpty() || resultadosFundacaoProfunda != null) {
                escreverTabelaResultados(resultadosFundacaoProfunda, doc);
            }
            if (parametros.get(CHAVE_UTILIZADA.DATA) != null) {
                doc.add(new Paragraph((String) (parametros.get(CHAVE_UTILIZADA.DATA))));
            }
            doc.add(recuperaRodape());

            GeradorDeMensagem.exibirMensagemDeInformacao("O relatório Pdf foi gerado com sucesso!", "Alerta ao Usuário");

        } catch (FileNotFoundException erro1) {
            GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um erro na geração do relatório, favor realize novamente a operação!");
        } catch (DocumentException erro2) {
            GeradorDeMensagem.exibirMensagemDeErro("O documento gerado contém elementos incompletos, favor realize os cadastros novamente!");
        } finally {
            if (doc != null) {
                //fechamento do documento
                doc.close();
            }
            if (os != null) {
                try {
                    //fechamento da stream de saída
                    os.close();
                } catch (IOException ex) {
                    GeradorDeMensagem.exibirMensagemDeErro("Ocorreu um erro de comunicação com arquivo, favor realize a operação novamente!");
                }
            }
        }
    }

    private Image recuperarLogoRelatorio() {
        try {
            Image img = Image.getInstance(
                        getClass().
                        getResource(CAMINHO_LOGO));
            img.setAlignment(Element.ALIGN_CENTER);
            return img;
        } catch (BadElementException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    private Paragraph recuperarCabecalho() {
        Font fonteCabecalho = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Paragraph cabecalho = new Paragraph("Relatório de Capacidade de Carga", fonteCabecalho);
        cabecalho.setAlignment(Element.ALIGN_CENTER);
        return cabecalho;

    }

    public void escreverTabelaProfundidadePorNSPT(List<ItemDeEnsaio> ensaiosRealizados, Document doc) throws DocumentException {
        if (!ensaiosRealizados.isEmpty()) {
            doc.newPage();
            PdfPTable tabela1 = new PdfPTable(2);
            tabela1.setWidthPercentage(new float[]{120f, 40f}, doc.getPageSize());
            
            Paragraph paragrafoProf = new Paragraph("Profundidade (m)", fontePadraoNegrito);
            paragrafoProf.setAlignment(Paragraph.ALIGN_CENTER);
            PdfPCell celulaCabecalhoTabela1 = new PdfPCell(paragrafoProf);
            celulaCabecalhoTabela1.setColspan(1);
            celulaCabecalhoTabela1.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela1.addCell(celulaCabecalhoTabela1);

            Paragraph paragrafoNspt = new Paragraph("Nspt", fontePadraoNegrito);
            paragrafoNspt.setAlignment(Paragraph.ALIGN_CENTER);
            PdfPCell celulaCabecalho2Tabela1 = new PdfPCell(paragrafoNspt);
            celulaCabecalho2Tabela1.setColspan(1);
            celulaCabecalho2Tabela1.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela1.addCell(celulaCabecalho2Tabela1);

            int count = 0;
            while (count < ensaiosRealizados.size()) {
                ItemDeEnsaio itemTemp = ensaiosRealizados.get(count);
                PdfPCell celulaTemporariaTabela1 = new PdfPCell(new Paragraph(String.valueOf(itemTemp.getProfundidade()), fontePadrao));
                PdfPCell celulaTemporaria2Tabela1 = new PdfPCell(new Paragraph(String.valueOf(itemTemp.getNspt()), fontePadrao));
                tabela1.addCell(celulaTemporariaTabela1);
                tabela1.addCell(celulaTemporaria2Tabela1);
                count++;

                if (count == 20) {
                    break;
                }
            }
            doc.add(tabela1);
            if (ensaiosRealizados.size() < 20) {
                return;
            } else {
                int indiceFinal = ensaiosRealizados.size() - 1;
                List<ItemDeEnsaio> itensRestantes = (List<ItemDeEnsaio>) ensaiosRealizados.subList(count, indiceFinal);

                escreverTabelaProfundidadePorNSPT(itensRestantes, doc);
            }
        }
    }

    public void escreverTabelaSolos(ArrayList<Solo> solos, Document doc) throws DocumentException {
        if (!solos.isEmpty()) {
            doc.newPage();
            PdfPTable tabela2 = new PdfPTable(new float[]{0.3f, 0.1f, 0.1f});
            Paragraph paragrafoSolo = new Paragraph("Solo", fontePadraoNegrito);
            paragrafoSolo.setAlignment(Paragraph.ALIGN_CENTER);
            PdfPCell celulaCabecalhoTabela2 = new PdfPCell(paragrafoSolo);
            celulaCabecalhoTabela2.setColspan(1);
            celulaCabecalhoTabela2.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela2.addCell(celulaCabecalhoTabela2);
            
            Paragraph paragrafoDe = new Paragraph("De (m)", fontePadraoNegrito);
            paragrafoDe.setAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell celulaCabecalho2Tabela2 = new PdfPCell(paragrafoDe);
            celulaCabecalho2Tabela2.setColspan(1);
            celulaCabecalho2Tabela2.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela2.addCell(celulaCabecalho2Tabela2);
            
            Paragraph paragrafoAte = new Paragraph("Até (m)", fontePadraoNegrito);
            paragrafoAte.setAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell celulaCabecalho3Tabela2 = new PdfPCell(paragrafoAte);
            celulaCabecalho3Tabela2.setColspan(1);
            celulaCabecalho3Tabela2.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela2.addCell(celulaCabecalho3Tabela2);

            int count = 0;
            for (Solo solo : solos) {
                count++;
                if (count == 20) {
                    break;
                } else {
                    PdfPCell temp1tabela2 = new PdfPCell(new Paragraph(
                                recuperaParametroTextoTipoDeSolo(solo.getTipoDeSolo()), fontePadrao));
                    temp1tabela2.setColspan(1);
                    tabela2.addCell(temp1tabela2);
                    PdfPCell temp2tabela2 = new PdfPCell(new Paragraph(String.valueOf(solo.getDe()), fontePadrao));
                    temp2tabela2.setColspan(1);
                    tabela2.addCell(temp2tabela2);
                    PdfPCell temp3tabela2 = new PdfPCell(new Paragraph(String.valueOf(solo.getAte()), fontePadrao));
                    temp3tabela2.setColspan(1);
                    tabela2.addCell(temp3tabela2);
                }
            }
            doc.add(tabela2);
            if (solos.size() < 20) {
                return;
            } else {
                int indiceFinal = solos.size() - 1;
                List<Solo> solosRestantes = solos.subList(count, indiceFinal);
                escreverTabelaSolos(solos, doc);
            }
        }
    }

    public void escreverTabelaResultados(ArrayList<ResultadoFundacaoProfunda> resultados, Document doc) throws DocumentException {
        if (!resultados.isEmpty()) {
            doc.newPage();

            PdfPTable tabela3 = new PdfPTable(new float[]{0.3f, 0.3f, 0.3f,0.3f, 0.3f, 0.3f,0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f});
           
            Paragraph paragrafoEstaca = new Paragraph("Estaca (cm)", fontePadraoNegrito);
            paragrafoEstaca.setAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell celulaMaeTabela3 = new PdfPCell(paragrafoEstaca);
            celulaMaeTabela3.setColspan(1);
            celulaMaeTabela3.setRowspan(2);
            celulaMaeTabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaMaeTabela3);

             Paragraph metodoDecourt= new Paragraph("Método Decóurt-Quaresma", fontePadraoNegrito);
            metodoDecourt.setAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell celulaMae1Tabela3 = new PdfPCell(metodoDecourt);
            celulaMae1Tabela3.setColspan(6);
            celulaMae1Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaMae1Tabela3);

             Paragraph metodoAoki= new Paragraph("Método Aoki-Velloso", fontePadraoNegrito);
            metodoAoki.setAlignment(Paragraph.ALIGN_CENTER);
            
            PdfPCell celulaMae2Tabela3 = new PdfPCell(metodoAoki);
            celulaMae2Tabela3.setColspan(6);
            celulaMae2Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaMae2Tabela3);

            PdfPCell celulaCabecalho2Tabela3 = new PdfPCell(new Paragraph("Qp", fontePadraoNegrito));
            celulaCabecalho2Tabela3.setColspan(1);
            celulaCabecalho2Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho2Tabela3);

            PdfPCell celulaCabecalho3Tabela3 = new PdfPCell(new Paragraph("Ql", fontePadraoNegrito));
            celulaCabecalho3Tabela3.setColspan(1);
            celulaCabecalho3Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho3Tabela3);

            PdfPCell celulaCabecalho4Tabela3 = new PdfPCell(new Paragraph("Qt", fontePadraoNegrito));
            celulaCabecalho4Tabela3.setColspan(1);
            celulaCabecalho4Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho4Tabela3);

            PdfPCell celulaCabecalho2Tabela3s = new PdfPCell(new Paragraph("[Qp/FS", fontePadraoNegrito));
            celulaCabecalho2Tabela3s.setColspan(1);
            celulaCabecalho2Tabela3s.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho2Tabela3s);

            PdfPCell celulaCabecalho3Tabela3s2 = new PdfPCell(new Paragraph("Ql/FS", fontePadraoNegrito));
            celulaCabecalho3Tabela3s2.setColspan(1);
            celulaCabecalho3Tabela3s2.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho3Tabela3s2);

            PdfPCell celulaCabecalho4Tabela3s4 = new PdfPCell(new Paragraph("Qt]", fontePadraoNegrito));
            celulaCabecalho4Tabela3s4.setColspan(1);
            celulaCabecalho4Tabela3s4.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho4Tabela3s4);
          
            PdfPCell celulaCabecalho5Tabela3 = new PdfPCell(new Paragraph("Qp", fontePadraoNegrito));
            celulaCabecalho5Tabela3.setColspan(1);
            celulaCabecalho5Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho5Tabela3);

            PdfPCell celulaCabecalho6Tabela3 = new PdfPCell(new Paragraph("Ql", fontePadraoNegrito));
            celulaCabecalho6Tabela3.setColspan(1);
            celulaCabecalho6Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho6Tabela3);

            PdfPCell celulaCabecalho7Tabela3 = new PdfPCell(new Paragraph("Qt", fontePadraoNegrito));
            celulaCabecalho7Tabela3.setColspan(1);
            celulaCabecalho7Tabela3.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho7Tabela3);

           
            PdfPCell celulaCabecalho2Tabela3sd1 = new PdfPCell(new Paragraph("[Qp/FS", fontePadraoNegrito));
            celulaCabecalho2Tabela3sd1.setColspan(1);
            celulaCabecalho2Tabela3sd1.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho2Tabela3sd1);

            PdfPCell celulaCabecalho3Tabela3sd2 = new PdfPCell(new Paragraph("Ql/FS", fontePadraoNegrito));
            celulaCabecalho3Tabela3sd2.setColspan(1);
            celulaCabecalho3Tabela3sd2.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho3Tabela3sd2);

            PdfPCell celulaCabecalho4Tabela3sd4 = new PdfPCell(new Paragraph("Qt]", fontePadraoNegrito));
            celulaCabecalho4Tabela3sd4.setColspan(1);
            celulaCabecalho4Tabela3sd4.setBackgroundColor(new BaseColor(COR_TABELA_CABECALHO_RED, COR_TABELA_CABECALHO_GREEN, COR_TABELA_CABECALHO_BLUE));
            tabela3.addCell(celulaCabecalho4Tabela3sd4);
          
            int tempNumber = 0, count = 0;

            for (ResultadoFundacaoProfunda resultado : resultados) {
                count++;
                if (count == 18) {
                    break;
                } else {
                    if (tempNumber != resultado.getProfundidade()) {
                        tempNumber = resultado.getProfundidade();
                        PdfPCell celulaTempDireta = new PdfPCell(new Paragraph("L (m)=   " + resultado.getProfundidade(), fontePadraoItalico));
                        celulaTempDireta.setColspan(13);
                        celulaTempDireta.setBackgroundColor(BaseColor.LIGHT_GRAY);
                        tabela3.addCell(celulaTempDireta);

//                    
                    }
                    PdfPCell celulaTemp1Tabela3 = new PdfPCell(new Paragraph(String.valueOf(resultado.getDimensaoDeEstaca()), fontePadrao));
                    tabela3.addCell(celulaTemp1Tabela3);

                    PdfPCell celulaTemp2Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQp_decourt()), fontePadrao));
                    tabela3.addCell(celulaTemp2Tabela3);
                    PdfPCell celulaTemp3Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQl_decourt()), fontePadrao));
                    tabela3.addCell(celulaTemp3Tabela3);
                    PdfPCell celulaTemp4Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQt_decourt()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3);

                    PdfPCell celulaTemp4Tabela3s1 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQp_aoki_reajustado()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3s1);
                    PdfPCell celulaTemp4Tabela3s2 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQl_aoki_reajustado()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3s2);
                    PdfPCell celulaTemp4Tabela3s4 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQt_aoki_FS()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3s4);
                              
                    PdfPCell celulaTemp5Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQp_aoki()), fontePadrao));
                    tabela3.addCell(celulaTemp5Tabela3);
                    PdfPCell celulaTemp6Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQl_aoki()), fontePadrao));
                    tabela3.addCell(celulaTemp6Tabela3);
                    PdfPCell celulaTemp7Tabela3 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQt_aoki()), fontePadrao));
                    tabela3.addCell(celulaTemp7Tabela3);
                   
                    PdfPCell celulaTemp4Tabela3sd1 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQp_decourt_reajustado()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3sd1);
                    PdfPCell celulaTemp4Tabela3sd2 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQl_decourt_reajustado()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3sd2);
                    PdfPCell celulaTemp4Tabela3sd4 = new PdfPCell(new Paragraph(UtilitariaDeCalculo.retornarTextoDeResultadoDouble(resultado.getQt_decourt_FS()), fontePadrao));
                    tabela3.addCell(celulaTemp4Tabela3sd4);
                
                }
            }
            doc.add(tabela3);
            if (resultados.size() < 19) {
                return;
            } else {
                List<ResultadoFundacaoProfunda> resultadosRestantes = resultados.subList(count - 1, resultados.size());

                ArrayList<ResultadoFundacaoProfunda> resultadosPreliminares = new ArrayList<>();
                for (ResultadoFundacaoProfunda resultadoTemp : resultadosRestantes) {
                    resultadosPreliminares.add(resultadoTemp);
                }
                escreverTabelaResultados(resultadosPreliminares, doc);
            }
        }
    }

    private Paragraph recuperaRodape() {
        Font fonteRodape = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);

        String rodape = "\n\n\u00a9 Todos os direitos reservados - Grupo MAEC - Universidade Federal do Pampa";
        return new Paragraph(rodape, fonteRodape);
    }

}
